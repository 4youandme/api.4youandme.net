# bad-tokens.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Thu Feb 27 16:30:30 2020.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Feb 25 11:51:56 2020.
curl --request POST \
     --header "Content-type: application/json" \
     --header "api-token: a-ridiculous-secret" \
     --data @bad-tokens.json \
     http://localhost:3000/internal/api/failed_tokens/oura
