# authenticate_user.rb: -*- Ruby -*- 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
#  Birthdate: Mon Sep 30 12:27:19 2019.
class AuthenticateUser
  prepend SimpleCommand

  def initialize(username, password)
    @username = username
    @password = password
  end

  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  attr_accessor :username, :password

  def user
    candidate = User.where(email: (username.downcase.strip rescue "")).first
    if candidate and candidate.authenticate(password)
      return candidate
    else
      errors.add :authentication, 'Invalid credentials'
    end
  end
end
