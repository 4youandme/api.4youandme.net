ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc { I18n.t("active_admin.dashboard") }

  content title: proc { I18n.t("active_admin.dashboard") } do
    columns do
      column do
        # non-superusers can only see participants who have given consent and who are in their site
        users = current_crc_user.superuser ? User : User.joins(:roster).where(gave_full_consent: true).where("rosters.site=?", current_crc_user.site)
        rosters = current_crc_user.superuser ? Roster : Roster.joins(:participant).where('users.gave_full_consent=true').where(site: current_crc_user.site)

        render 'admin/dashboard/participants', {rosters: rosters, users: users}


        panel "10 Most Recently Active Participants" do
          table_for users.order("last_fed_at desc NULLS LAST").limit(10) do
            column "Phone" do |u|
              if u.roster
                link_to u.phone.number, "/crc/rosters/" + u.roster.id
              else
                u.phone.number
              end
            end
            column "Site" do |u|
              u.site
            end
            column "Last Viewed Feed" do |u|
              u.last_fed_at.in_time_zone(u.timezone) rescue nil
            end
            column "Time Zone", :timezone
          end
        end
      end
    end
    
    #     section "Recently updated content" do
    #       table_for PaperTrail::Version.order(id: :desc).limit(20) do # Use PaperTrail::Version if this throws an error
    #         column ("Item") { |v| v.item }
    #         # This link to :crc doesn't work in production, and I don't know why.
    #         # column ("Item") { |v| link_to v.item, [:crc, v.item] } # Uncomment to display as link
    #         column ("Type") { |v| v.item_type.underscore.humanize }
    #         column ("Modified at") { |v| v.created_at.to_s :long }
    #         column ("Admin") do |v|
    #           u = CrcUser.where(id: v.whodunnit).first
    #           u ? u.email : "The Root User"
    #         end
    #       end
    #    end
  end
end

