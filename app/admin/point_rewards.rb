ActiveAdmin.register PointReward do
  filter :roster, label: "Phone", filters: [:ends_with], collection: -> {
    current_crc_user.superuser ? Roster.all : Roster.joins(:participant).where('users.gave_full_consent = true')
  }
  filter :awarded_at
  filter :points
  filter :trigger

  controller do
    # Only show Rosters where the user uploaded the consent document
    def scoped_collection
      if not current_crc_user.superuser
        PointReward.joins(user: :roster)
                   .where('users.gave_full_consent = true')
                   .where('rosters.site=?', current_crc_user.site)

      else
        super
      end
    end
  end

  index do
    column "Roster" do |pr|
      link_to pr.phone_number, "/crc/rosters/" + (pr.roster.id rescue "")
    end
    column :awarded_at
    column :points
    column :trigger
  end
end
