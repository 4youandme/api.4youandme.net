ActiveAdmin.register Roster do
  permit_params :phone,
                :site,
                :empatica_id,
                :ibdoc_id,
                :bodyport_id,
                :pooply_id,
                :after_phone_validation,
                :after_email_validation,
                :is_test_user,
                :study_end_date

  filter :participant_id_eq, label: "Part. ID"

  filter :phone, filters: [:ends_with]
  filter :empatica_id
  filter :ibdoc_id
  filter :bodyport_id
  filter :pooply_id
  filter :site
  filter :status
  filter :is_test_user

  after_update do
    resource.start_study if permitted_params[:commit].downcase.include?('start')
  end

  controller do
    # Only show Rosters where the user uploaded the consent document
    def scoped_collection
      # Calculate onboarding_state so we can sort by it
      roster_collection = Roster.select(%{
        rosters.*,
        rosters.id as id,
        rosters.created_at as created_at,
        CASE
          WHEN participant_id is null THEN after_phone_validation
          WHEN users.status is null THEN after_phone_validation
          ELSE users.status
        END as onboarding_state,
        active_storage_attachments.created_at as consent_date})
                              .left_outer_joins({ participant: :signed_consent_attachment })

      unless current_crc_user.superuser
        roster_collection = roster_collection.where('users.gave_full_consent = true').where('rosters.site=?', current_crc_user.site)
      end
      roster_collection
    end
  end

  order_by(:compliance) do |order_clause|
    [order_clause.to_sql, 'NULLS LAST'].join(' ')
  end

  order_by(:compliance_last_week) do |order_clause|
    [order_clause.to_sql, 'NULLS LAST'].join(' ')
  end

  index do
    table do
      tr do
        td "ONBOARDING STATES:"
        td "#{Roster.states.collect { |s| s.titleize }.to_sentence}"
      end
    end

    style do
      [
        "td.col-participant_id { min-width: 250px; text-align: center }",
        ".col-overall_compliance { text-align: center }",
        ".col-compliance_last_7_days { text-align: center }",
        ".col-study_day { text-align: center; }",
        ".col-site { text-align: center; }",
        "td.col-phone { min-width: 100px; }",
        "td.col-emp_id, td.col-ibd_id, td.col-bport_id, td.col-test_user, td.col-started { text-align: center; }"
      ].join(' ')
    end
    column "Phone" do |roster|
      link_to roster.phone, "/crc/rosters/" + roster.id
    end
    column "Name" do |r|
      r.user.name rescue ""
    end
    column "Email" do |r|
      r.user.email.address rescue ""
    end
    column("Test user", sortable: :is_test_user) { |r| r.is_test_user }
    column :site

    column "Overall Compliance", :compliance
    column "Compliance Last 7 Days", :compliance_last_week

    column "Last Opened" do |r|
      r.pretty_last_app_access
    end

    #    column "Participant ID" do |roster|
    #      roster.participant_id || "<not yet participating>"
    #    end
    column "Onboarding State", sortable: "onboarding_state" do |roster|
      roster.crc_status.titleize rescue nil
    end
    column "Consent", sortable: 'consent_date' do |r|
      r.consent_date&.in_time_zone(r.user ? r.user.timezone : nil)&.strftime("%F (%Z)")
    end
    column "Created", sortable: "created_at" do |r|
      r.created_at&.in_time_zone(r.user ? r.user.timezone : nil)&.strftime("%F (%Z)")
    end
    # column "Emp. ID", :empatica_id 
    # column "IBD ID", :ibdoc_id
    # column "BPort ID", :bodyport_id
    # column "Test User?", :is_test_user
    # column "Started?" do |r|
    #   status_tag r.study_started ? true : false
    #  end
    column "Start Date", sortable: "users.study_started_at" do |r|
      r.study_started ?
        r.study_started.in_time_zone(r.user ? r.user.timezone : nil).strftime("%F (%Z)") :
        "<i>study not yet started</i>".html_safe
    end
    column "Study Day" do |r|
      r.user.study_day rescue ""
    end
    column "DOB" do |r|
      r.user.birthdate rescue ""
    end

    column "" do |resource|
      links = ''.html_safe
      links += link_to I18n.t('active_admin.view'), resource_path(resource), class: "member_link show_link"
      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), class: "member_link edit_link"
      links
    end
  end

  show do
    style do
      [
        "form#superuser-tile-select { max-width: 500px; }"
      ].join(' ')
    end

    attributes_table do
      row "Phone" do |roster|
        roster.phone
      end
      row "Name" do |r|
        r.user.name rescue ""
      end
      row "Email" do |r|
        r.user.email.address rescue ""
      end
      row "Birthdate" do |r|
        r.user.birthdate rescue ""
      end
      row :site
      row "Timezone" do |r|
        r.user&.timezone
      end
      row "Participant ID" do |roster|
        roster.participant_id || "<not yet participating>"
      end
      row "Status" do |roster|
        roster.crc_status
      end
      row "API Token" do |roster|
        roster.api_token
      end

      row "App Last Opened" do |roster|
        roster.pretty_last_app_access
      end

      row "Consent Date" do |roster|
        roster.consent_date&.in_time_zone(roster.user ? roster.user.timezone : nil)&.strftime("%F (%Z)")
      end

      if current_crc_user.superuser
        row "Opt-In Relative Location" do
          roster
          roster.opt_in_relative_location_answer
        end
        row "Opt-In Sharing Options" do
          roster
          roster.opt_in_sharing_options_answer
        end
        row "Opt-In Future Reseach Contact" do
          roster
          roster.opt_in_future_research_contact_answer
        end
      end

      row "Empatica ID" do |r|
        r.empatica_id
      end
      row "Pooply ID" do |r|
        r.pooply_id
      end
      row "IBDoc ID" do |r|
        r.ibdoc_id
      end
      row "Bodyport ID" do |r|
        r.bodyport_id
      end

      row "Study Started" do |r|
        r.study_started
      end
      row "Study end date" do |r|
        r.participant&.study_ended_at
      end
      row "Current Study Month" do |r|
        r.participant.study_month rescue nil
      end
      row "Points Earned Current Month" do |r|
        r.points_of_study_month
      end

      row "Test User?" do |r|
        r.is_test_user
      end
    end

    render partial: 'tools_panel'
  end

  form do |f|
    input_html_hash = { disabled: true }
    input_html_hash = {} if current_crc_user && current_crc_user.superuser
    f.inputs do
      f.semantic_errors
      render partial: "phone_input"
      # f.input :site, as: :select, collection: ["MSSM", "Oxford"], requred: true
      f.input :site, as: :radio, collection: ["MSSM", "Oxford"], requred: true
      if !f.object.new_record?
        f.input :empatica_id, label: "Empatica ID", input_html: input_html_hash
        f.input :ibdoc_id, label: "IBDoc ID", input_html: input_html_hash
        f.input :bodyport_id, label: "Bodyport ID", input_html: input_html_hash
        f.input :pooply_id, label: "Pooply ID", input_html: input_html_hash
      end

      f.input :is_test_user, label: "Is this a test (i.e., non-study participant) user?"

      if !f.object.new_record?
        f.input :study_started, label: "Study Started", input_html: { disabled: true }
        if f.object.study_started
          f.input :study_end_date, label: "Study end date", as: :datepicker,
                  datepicker_options: {
                    min_date: f.object.study_started.strftime('%Y-%m-%d')
                  }
        end
      end

      f.actions do
        f.action :submit, wrapper_html: { onclick: "maybe_fix_phone()" }
        f.action :submit, label: "Verify & Start", wrapper_html: { onclick: "maybe_fix_phone()" } if !f.object.new_record? && !f.object.study_started
        f.action :cancel, label: "Cancel", wrapper_html: { class: 'cancel' }
      end
    end
  end

  controller do
    before_action :set_paper_trail_whodunnit
    before_action :maybe_do_superuser_action
    before_action :maybe_enable_schedule
    before_action :maybe_enable_feed
    after_action :do_audit_logging

    def user_for_paper_trail
      crc_user_signed_in? ? current_crc_user.try(:id) : "unknown user"
    end

    # def info_for_paper_trail
    #     debugger
    #     user = crc_user_signed_in? ? current_crc_user : nil

    #     if params[:id]
    #         if resource
    #             Rails.logger.debug("SEE RESOURCE")
    #             Rails.logger.debug(resource)
    #         end

    #         # @post_version = PaperTrail::Version.find_by_id(params[:id])
    #         # Rails.logger.debug("VERSION")
    #         # Rails.logger.debug(@post_version)

    #         if resource.user
    #             Rails.logger.debug("PAPER TRAIL INFO")
    #             AuditLog.create(who: resource.user.id, what: resource, when: Time.now, ip_address: request.ip)
    #         end
    #     end

    #     nil
    # end

    def is_superuser
      current_crc_user && current_crc_user.superuser
    end

    def maybe_do_superuser_action
      if is_superuser
        if params[:submit] == "send-tile"
          resource.user.send_tiles(params[:tile]) if resource.user
        end
      end
    end

    def maybe_enable_schedule
      @show_tile_schedule = true if params[:enable_schedule]
    end

    def maybe_enable_feed
      @show_feed = true if params[:enable_feed]
    end

    def do_audit_logging
      who = current_crc_user.email rescue 'Unknown CRC'
      which = "Roster(#{resource.id[0..5] rescue 'none'}...): <#{ resource.phone rescue '???'}>"
      what = action_name rescue "idunno"
      what += "+enable_feed" if params[:enable_feed]
      what += "+enable_schedule" if params[:enable_schedule]
      what += "+send_tile[#{params[:tile]}" if params[:submit] == "send-tile"

      AuditLog.create(who: who, what: what, which: which, when: Time.now, ip_address: (request.ip rescue 'Unknown IP'))
    end

  end
end
