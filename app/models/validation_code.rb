# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class ValidationCode < ApplicationRecord
  belongs_to :user

  TWILIO_SID = ENV['TWILIO_SID']
  TWILIO_AUTH= ENV['TWILIO_AUTH']
  TWILIO_NUM = ENV['TWILIO_NUM']
  PUBLIC_APP_NAME = ENV['PUBLIC_APP_NAME']

  def failure(message)
    Rails.logger.warn("validation_code.rb: failure: #{message}")
  end
  
  def send_code
    self.make_code and save if not self.code
    client = Twilio::REST::Client.new rescue (failure("Can't create client - bad ENV?") and return)
    client.messages.create(
      from: TWILIO_NUM, to: self.user.phone.number,
      body: "#{PUBLIC_APP_NAME} has sent you the following validation code: #{self.code}" 
    ) rescue (failure("Can't create message - bad number?") and return)
  end
  
  def make_code
    100.times do
      candidate = (rand(9) + 1).to_s
      candidate += 4.times.map{rand(10)}.join
      if not self.class.where(code: candidate).first
        self.code = candidate
        break
      end
    end
  end
end
