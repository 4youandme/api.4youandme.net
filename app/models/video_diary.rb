# video_diary.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Jan 31 09:43:38 2020.
require 'aws-sdk-s3'

class VideoDiary < ApplicationRecord
  belongs_to :user

  def upload_url(for_date=nil)
    for_date ||= Date.current
    for_date = for_date.to_s
    filename ||= "video-diary-#{user.id}-#{for_date}.mov"
    bucket = ENV['AWS_BUCKET']
    region = ENV['AWS_REGION']
    s3 = Aws::S3::Resource.new(access_key_id: ENV['AWS_KEY'], secret_access_key: ENV['AWS_SECRET'], region: region)
    obj = s3.bucket(bucket).object(filename)
    url = URI.parse(obj.presigned_url(:put))
    save if changed?
    url
  end
    
end
