class AppSection < ApplicationRecord
  @detailClassname = "AppSectionInfo"
  @detailClass = @detailClassname.constantize
  
  has_many :section_details, class_name: @detailClassname
  has_many :screens, through: :section_details

  def self.from_file(file)
    json = JSON.parse(File.read(file))
    self.from_json(json)
  end
  
  def self.from_json(array_of_sections)

    array_of_sections.each do |raw|
      json = ActiveSupport::HashWithIndifferentAccess.new(raw)
      cols = self.column_names
      cols.delete("id")
      section = self.new(json.slice(*cols))
      details = json[:section_details]

      if details
        details.each_with_index do |detail, pos|
          info = @detailClass.from_hash(detail, pos)
          section.section_details << info
        end
      end

      section.save or raise "COULDN'T SAVE SECTION: " + section.errors.full_messages.to_sentence
    end
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:title] = res[:name]
    res[:section_details] = self.section_details.as_json(options) if options[:legacy] && self.section_details.length > 0
    res
  end

  # Return the current versions of the app sections
  def self.current_app_sections
    self.all.select('distinct on (app_sections.name) app_sections.*').order('app_sections.name, app_sections.created_at DESC')
  end
end
