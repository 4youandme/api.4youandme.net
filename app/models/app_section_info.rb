class AppSectionInfo < ApplicationRecord
  belongs_to :app_section, required: false
  has_many :screens, as: :screenable
  has_many :properties, as: :owner
  has_many :contents, as: :contentable

  def self.from_hash(hash, pos=0)
    cols = self.column_names
    cols.delete("id")
    info = self.new(hash.slice(*cols))
    info.section_type ||= hash[:type]
    info.position ||= pos + 1
    c = info.contents.new(hash.slice(:content, :content_type, :style_class)) if hash.has_key?(:content)
    info.properties.new(name: "require_number_correct", value: hash[:require_number_correct]) if hash.has_key?(:require_number_correct)

    if hash[:screens]
      hash[:screens].each_with_index do |s, pos|
        screen = Screen.from_hash(s, pos)
        info.screens << screen
      end
    end

    info
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:type] = res.delete(:section_type)
    content = self.contents.first
    if content
      res[:content] = content.content
      res[:content_type] = content.content_type
      res[:style_class] = content.style_class
    end
    res[:screens] = self.screens.as_json(options) if self.screens.first

    res
  end

end
