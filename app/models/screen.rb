# screen.rb: -*- Ruby -*-  Generic model for a screen.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sat Dec 14 06:43:02 2019.
class Screen < ApplicationRecord
  belongs_to :screenable, polymorphic: true
  has_many :properties, as: :owner
  has_many :contents, as: :contentable
  has_many :content_switches, as: :switchable
  has_many :details, class_name: "ScreenDetail"
  default_scope { order(position: :asc, created_at: :asc) }

  def maybe_add_property(hash, sym)
    self.properties.new(name: sym.to_s, value: hash[sym]) if hash.has_key?(sym)
  end

  def self.from_hash(hash, pos=0)
    cols = self.column_names
    cols.delete("id")
    screen = self.new(hash.slice(*cols))
    screen.position ||= pos + 1

    # Oh, for God's sake.
    screen.maybe_add_property(hash, :counter)
    screen.maybe_add_property(hash, :custom_url)

    if hash.has_key?(:details)
      hash[:details].each_with_index do |hash_detail, pos|
        detail = ScreenDetail.from_hash(hash_detail, pos)
        screen.details << detail
      end
    end
    
    if hash.has_key?(:installed)
      installed = ContentSwitch.from_hash(hash[:installed].merge({tag: "installed"}))
      screen.content_switches << installed
    end

    screen.contents << Content.from_hash(hash) if hash.has_key?(:content)
    screen.contents << Content.from_hash(hash[:learn_more].merge({tag: "learn_more"})) if hash.has_key?(:learn_more)

    screen
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))

    res.delete(:screenable_id)
    res.delete(:screenable_type)
    propval = self.properties.where(name: "counter").first.value rescue nil
    res[:counter] = propval if propval

    propval = self.properties.where(name: "custom_url").first.value rescue nil
    res[:custom_url] = propval if propval

    switches = self.content_switches
    if switches.length > 0
      switches.each do |switch|
        tmp = switch.as_json(options)
        res[switch.tag] = tmp[switch.tag]
      end
    end

    c = self.contents.where(tag: nil).first
    if c
      res[:content] = c.content
      res[:content_type] = c.content_type
      res[:style_class] = c.style_class
    end

    self.contents.where.not(tag: nil).each do |c|
      res[c.tag] = c.as_json(options)
      [:contentable_type, :contentable_id, :tag].each {|x| res[c.tag].delete(x)}
    end

    deets = self.details
    if deets.length > 0
      res[:details] = []

      deets.each do |deet|
        res[:details].push(deet.as_json(options))
      end
    end

    res
  end
end
