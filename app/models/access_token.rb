# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Thu Dec 5 10:03:22 2019
require 'net/http'

class AccessToken < ApplicationRecord
  belongs_to :user
  belongs_to :oauth_provider
  after_save :maybe_update_pooply

  def refresh
    self.oauth_provider.refresh(self.refresh_token)
  end

  def self.refresh(options={})
    within = options[:within]
    within ||= 4.hours

    tokens = self.where.not(refresh_token: nil)
    tokens = tokens.where.not(expires_at: nil).where(expires_at: (Time.zone.now - 23.hours)..(Time.zone.now + within)) if not options[:force]
    tokens.each {|a| a.refresh}
  end

  # There's nothing I can do to get away from this provider specific insanity.
  # Note to self: design and implement a plugin architecture so that you can
  # write study-specific hooks and behaviors and store them in a gem that you
  # can integrate.
  def maybe_update_pooply
    oauth_name = self.oauth_provider.as_identifier
    if oauth_name == "pooply"
      roster = user.roster rescue nil
      if roster
        uri = URI("https://pooply.eu.api.healthmode.com/get-current-user/")
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true if uri.port == 443
        http.set_debug_output(Logger.new("/tmp/api.4youandme.net-oauth-#{oauth_name}-debug.log")) if self.oauth_provider.oauth_debug
        req = Net::HTTP::Get.new(uri.path)
        req["Accept"] = "*/*"
        req["Content-Type"] = "application/json"
        req["Authorization"] = "Bearer #{self.token}"
        res = http.request(req)
        pooply_id = JSON.parse(res.body)["id"] rescue nil
        (roster.pooply_id = pooply_id and roster.save) if pooply_id and pooply_id != roster.pooply_id
      end
    end
  end
end
