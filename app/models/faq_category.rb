# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class FaqCategory < ApplicationRecord
  has_many :questions, class_name: "FaqQuestion"
  before_save :ensure_order

  def ensure_order
    last = self.class.pluck(:position).max || 0
    self.position ||= last + 1
  end
  
  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:questions] = self.questions
    res
  end
end
