# content.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sat Dec 21 14:02:28 2019.
class Content < ApplicationRecord
  belongs_to :contentable, polymorphic: true
  has_many :buttons, as: :buttonable

  COLS = [:content, :content_type, :style_class, :tag]

  def self.from_hash(hash, pos=0)
    hash = ActiveSupport::HashWithIndifferentAccess.new(hash) if hash.class == Hash
    c = self.new(hash.slice(*(COLS + [:content_style])))
    c.buttons << Button.from_hash(hash[:button]) if hash.has_key?(:button)

    # FIXME: This is bad, and shouldn't be like this.  I'm a slave to the current schema.
    # There should never be a tag :button_content.  It should just be a button.
    if hash.has_key?(:button_content)
      bhash = ActiveSupport::HashWithIndifferentAccess.new({label: hash[:button_content], tag: "button_content"})
      # Add a button so that this could be produced correctly in the future.
      c.buttons << Button.from_hash(bhash, 0)
    end

    if hash.has_key?(:buttons)
      bs = hash[:buttons]
      bs.each { |b| c.buttons << Button.from_hash(b, 0) } if bs.is_a?(Array)
    end
    
    c
  end

  def content_style=(val)
    self.style_class = val
  end

  def add_button(hash)
    self.buttons.new(hash.slice(:label, :target, :tag)) if hash
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    [:contentable_type, :contentable_id, :created_at, :updated_at].each {|x| res.delete(x)} if options[:legacy]

    res = res.merge(Button.json_helper(self, options))

    if options[:legacy]
      res.delete(:contentable_type)
      res.delete(:contentable_id)
      res.delete(:created_at)
      res.delete(:updated_at)
    end

    res
  end
end
