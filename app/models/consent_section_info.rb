# consent_section_info.rb -*- Ruby -*-
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Author: Brian J. Fox (bfox@opuslogica.com)
# Birthdate: Tue Sep 17 07:43:18 2019
class ConsentSectionInfo < ApplicationRecord
  belongs_to :consent_section, required: false
  has_many :questions, as: :questionable, dependent: :destroy
  has_many :contents, as: :contentable, dependent: :destroy
  has_many :properties, as: :owner, dependent: :destroy
  has_many :screens, as: :screenable, dependent: :destroy
  default_scope { order(position: :asc, created_at: :asc) }
  
  COLS = [:name, :title, :position, :format, :type]

  def self.from_hash(hash, pos=0)
    info = self.new(hash.slice(*COLS))
    info.section_type = hash[:type]
    info.position ||= pos + 1
    info.contents << Content.from_hash(hash) if hash.has_key?(:content)
    info.properties.new(name: "require_number_correct", value: hash[:require_number_correct]) if hash.has_key?(:require_number_correct)

    hash[:questions].each {|q| info.questions << Question.from_hash(q) } if hash[:questions]

    # Sometimes, the schema includes a :details element.  This element is not an array
    # where each element is a detail.  It is an object where each key is the tag for some
    # content.
    details = hash[:details]
    if details
      tags = details.keys
      tags.each {|tag| info.contents << Content.from_hash(details[tag].merge(tag: tag)) }
    end
    
    if hash[:summaries]
      hash[:summaries].each_with_index {|summary, pos| info.screens << Screen.from_hash(summary, pos) }
    end

    info
  end

  def type
    self.section_type
  end

  def type=(val)
    self.section_type=val
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:type] = res.delete(:section_type)
    prop = self.properties.where(name: "require_number_correct").first
    res[:require_number_correct] = prop.value.to_i if prop rescue nil
    qs_json = self.questions.order(position: :asc, created_at: :asc).as_json(options)
    res[:questions] = qs_json if qs_json.length > 0
    content = self.contents.where(tag: nil).first
    if content
      res[:content] = content.content
      res[:content_type] = content.content_type
      res[:style_class] = content.style_class
    end

    details = {}
    self.contents.where.not(tag: nil).each do |c|
      details[c.tag] = c.as_json(options)
      details[c.tag].delete("tag")
    end
    res[:details] = details if details.length > 0

    res[:summaries] = self.screens.as_json(options) if self.screens.length > 0

    res
  end
end
