# oauth_provider.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Dec 20 12:50:46 2019.
require 'net/http'

class OauthProvider < ApplicationRecord
  has_many :properties, as: :owner
  has_many :access_tokens
  has_many :users, through: :access_tokens
  
  def client_id
    var = self.as_identifier.upcase + "_CLIENT_ID"
    ENV[var]
  end

  def client_secret
    var = self.as_identifier.upcase + "_CLIENT_SECRET"
    ENV[var]
  end

  def authorize_uri
    uri = self.properties.where(name: "authorize-uri").first.value rescue nil
    uri ||= self.base_uri + "/oauth/authorize"
    uri
  end

  def token_uri
    uri = get_property("token-uri")
    uri ||= self.base_uri + "/oauth/token"
    uri
  end

  def token_uri=(val)
    set_property("token-uri", val)
  end

  def token_extras
    propval = get_property("token-extras")
    JSON.parse(propval) rescue nil
  end

  def token_extras=(obj)
    prop = self.properties.where(name: "token-extras").first_or_create
    prop.value = obj.to_json
    prop.save
    prop.value
  end
  
  def use_form_post
    get_boolean_property("use-form-post")
  end

  def use_form_post=(val)
    set_boolean_property("use-form-post", val)
  end

  def use_basic_auth
    get_boolean_property("use-basic-auth")
  end

  def use_basic_auth=(val)
    set_boolean_property("use-basic-auth", val)
  end

  def oauth_debug
    get_boolean_property("oauth-debug")
  end

  def oauth_debug=(val)
    set_boolean_property("oauth-debug", val)
  end

  def get_boolean_property(name)
    prop = properties.where(name: name).first
    prop and prop.value == "true"
  end

  def set_boolean_property(name, val)
    if val != "true" and val != true
      properties.where(name: name).destroy_all
    else
      properties.where(name: name).first_or_create(value: "true")
    end
    get_boolean_property(name)
  end

  def get_property(name)
    prop = properties.where(name: name).first
    prop and prop.value
  end
  
  def set_property(name, val)
    prop = self.properties.where(name: name).first_or_create
    prop.value = val.to_s
    prop.save
    val
  end

  def token_post(options={})
    options = ActiveSupport::HashWithIndifferentAccess.new(options)
    uri = URI(self.token_uri)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true if uri.port == 443
    req = Net::HTTP::Post.new(uri.path)
    req["Accept"] = "*/*"

    body = { client_id: self.client_id, client_secret: self.client_secret }
    extras = ActiveSupport::HashWithIndifferentAccess.new(self.token_extras || {})
    extras = extras.merge(code: options[:code]) if options.has_key?(:code)
    extras = extras.merge(grant_type: options[:grant_type]) if options.has_key?(:grant_type)
    extras = extras.merge(refresh_token: options[:refresh_token]) if options.has_key?(:refresh_token)
    extras = extras.merge(redirect_uri: options[:redirect_uri]) if options.has_key?(:redirect_uri)
    body = body.merge(extras)

    # Make the appropriate type of request -- XHR or x-www-form-urlencoded
    if self.use_form_post
      req.set_form_data(body)
    else
      req["Content-Type"] = "application/json"
      req.body = body.to_json
    end

    if self.oauth_debug
      controller_name = self.class.to_s
      Rails.logger.error("#{controller_name}->token_post(): Provider: #{self.name}")
      Rails.logger.error("#{controller_name}->token_post(): Token URI: #{self.token_uri}")
      Rails.logger.error("#{controller_name}->token_post(): Token Extras: #{self.token_extras}")
      Rails.logger.error("#{controller_name}->token_post(): Use Basic Auth: #{self.use_basic_auth}")
      Rails.logger.error("#{controller_name}->token_post(): Use Form Post: #{self.use_form_post}")
      require "logger"
    end

    http.set_debug_output(Logger.new("/tmp/api.4youandme.net-oauth-#{self.as_identifier.downcase}-debug.log")) if self.oauth_debug

    res = http.request(req)
    data = JSON.parse(res.body) rescue nil
    access_token = AccessToken.where(refresh_token: options[:refresh_token], oauth_provider: self).first if options[:refresh_token]
    access_token ||= AccessToken.where(user: options[:user], oauth_provider: self).first_or_create if data.has_key?('access_token')

    if access_token
      access_token.token = data['access_token'] if not data['access_token'].blank?
      access_token.refresh_token = data['refresh_token'] if data.has_key?('refresh_token')
      access_token.expires_at = Time.zone.now + data['expires_in'].to_i if not data['expires_in'].blank?
      access_token.save
    end

    access_token
  end
  
  def refresh(refresh_token)
    user = AccessToken.where(refresh_token: refresh_token).first.user rescue nil
    token_post(grant_type: "refresh_token", refresh_token: refresh_token, user: user) if user
  end
  
  def self.lookup(name)
    self.where("REPLACE(LTRIM(RTRIM(LOWER(name))), ' ', '') = ?", self.as_identifier(name)).first
  end

  def self.as_identifier(s)
    s.gsub(/(\s|[-_])/, "").downcase
  end
  
  def as_identifier
    self.class.as_identifier(self.name)
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:oauth_url] = "https://api.4youandme.net/app/api/v1/authorize/#{self.as_identifier}"
    if options[:for_user]
      user = options[:for_user]
      token = AccessToken.where(user: user, oauth_provider: self).
                where("(expires_at > :now) or (expires_at is null)", now: Time.zone.now).first
      res[:connected] = token ? true : false
      res[:expires_at] = token.expires_at if token
    end
    if options[:redirect_detail]
      res[:title] = res.delete(:name)
      res.delete(:base_uri)
      res.delete(:scope)
      res[:redirect_detail] = {
        success_url: "foryouandmesic://#{self.as_identifier}_auth?status=success",
        failure_url: "foryouandmesic://#{self.as_identifier}_auth?status=failure",
        failure_msg: "Please try to connect again or skip to the next page"
      }
    end

    res
  end
end
