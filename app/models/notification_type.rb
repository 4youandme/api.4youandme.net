class NotificationType
  ALL =   0b1000

  PUSH =  0b1
  EMAIL = 0b10
  SMS =   0b100

  PUSH_OR_ALL = PUSH | ALL
  EMAIL_OR_ALL = EMAIL | ALL
  SMS_OR_ALL = SMS | ALL

  # Returns true if the type is PUSH
  def self.is_push?(type)
    !!type and type & PUSH_OR_ALL > 0
  end

  # Returns true if the type is EMAIL
  def self.is_email?(type)
    !!type and type & EMAIL_OR_ALL > 0
  end

  # Returns true if the type is SMS
  def self.is_sms?(type)
    !!type and type & SMS_OR_ALL > 0
  end

end
