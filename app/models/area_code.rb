class AreaCode < ApplicationRecord

  STATES = { "AK": "Alaska", "Alaska": "AK", 
             "AL": "Alabama", "Alabama": "AL", 
             "AR": "Arkansas", "Arkansas": "AR", 
             "AS": "American Samoa", "American Samoa": "AS", 
             "AZ": "Arizona", "Arizona": "AZ", 
             "CA": "California", "California": "CA", 
             "CO": "Colorado", "Colorado": "CO", 
             "CT": "Connecticut", "Connecticut": "CT", 
             "DC": "District of Columbia", "District of Columbia": "DC", 
             "DE": "Delaware", "Delaware": "DE", 
             "FL": "Florida", "Florida": "FL", 
             "GA": "Georgia", "Georgia": "GA", 
             "GU": "Guam", "Guam": "GU", 
             "HI": "Hawaii", "Hawaii": "HI", 
             "IA": "Iowa", "Iowa": "IA", 
             "ID": "Idaho", "Idaho": "ID", 
             "IL": "Illinois", "Illinois": "IL", 
             "IN": "Indiana", "Indiana": "IN", 
             "KS": "Kansas", "Kansas": "KS", 
             "KY": "Kentucky", "Kentucky": "KY", 
             "LA": "Louisiana", "Louisiana": "LA", 
             "MA": "Massachusetts", "Massachusetts": "MA", 
             "MD": "Maryland", "Maryland": "MD", 
             "ME": "Maine", "Maine": "ME", 
             "MI": "Michigan", "Michigan": "MI", 
             "MN": "Minnesota", "Minnesota": "MN", 
             "MO": "Missouri", "Missouri": "MO", 
             "MS": "Mississippi", "Mississippi": "MS", 
             "MT": "Montana", "Montana": "MT", 
             "NC": "North Carolina", "North Carolina": "NC", 
             "ND": "North Dakota", "North Dakota": "ND", 
             "NE": "Nebraska", "Nebraska": "NE", 
             "NH": "New Hampshire", "New Hampshire": "NH", 
             "NJ": "New Jersey", "New Jersey": "NJ", 
             "NM": "New Mexico", "New Mexico": "NM", 
             "NV": "Nevada", "Nevada": "NV", 
             "NY": "New York", "New York": "NY", 
             "OH": "Ohio", "Ohio": "OH", 
             "OK": "Oklahoma", "Oklahoma": "OK", 
             "OR": "Oregon", "Oregon": "OR", 
             "PA": "Pennsylvania", "Pennsylvania": "PA", 
             "PR": "Puerto Rico", "Puerto Rico": "PR", 
             "RI": "Rhode Island", "Rhode Island": "RI", 
             "SC": "South Carolina", "South Carolina": "SC", 
             "SD": "South Dakota", "South Dakota": "SD", 
             "TN": "Tennessee", "Tennessee": "TN", 
             "TX": "Texas", "Texas": "TX", 
             "UT": "Utah", "Utah": "UT", 
             "VA": "Virginia", "Virginia": "VA", 
             "VI": "Virgin Islands", "Virgin Islands": "VI", 
             "VT": "Vermont", "Vermont": "VT", 
             "WA": "Washington", "Washington": "WA", 
             "WI": "Wisconsin", "Wisconsin": "WI", 
             "WV": "West Virginia", "West Virginia": "WV", 
             "WY": "Wyoming", "Wyoming": "WY"
           }

  def self.import(filename)
    CSV.foreach(filename, headers: true) do |row|
      hash = ActiveSupport::HashWithIndifferentAccess.new(row.to_h)
      code = hash[:NPA_ID]
      zone = hash[:TIME_ZONE]
      state = STATES[hash[:LOCATION].to_sym] rescue nil
      assigned_at = hash[:ASSIGNMENT_DT]
      next if not state or not zone or zone[0] == "(" or zone[0] == "U"
      zone = "America/New_York" if zone[0] == "E"
      zone = "America/Chicago" if zone[0] == "C"
      zone = "America/Denver" if zone[0] == "M"
      zone = "America/Los_Angeles" if zone == "P"

      self.where(code: code, timezone: zone, location: state,  assigned_at: assigned_at).first_or_create
    end
  end
end
