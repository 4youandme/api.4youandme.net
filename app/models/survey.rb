class Survey < ApplicationRecord
  belongs_to :surveyable, polymorphic: true, required: false
  has_many :questions, as: :questionable, dependent: :destroy
  has_many :properties, as: :owner, dependent: :destroy


  def self.survey_tile_schedules_for_user(user)
    user
    .tile_schedules
    .joins(:tile)
    .where({ tiles: { referenced_type: "survey" }})
  end

  def self.get(name)
    self.where(name: name).first
  end
  
  def self.from_hash(hash, pos=0)
    survey = self.get(hash[:survey_ref]) if not hash[:survey_ref].blank?

    if not survey
      cols = self.column_names
      cols.delete("id")
      survey = self.new(hash.slice(*cols))
      survey.name ||= hash[:survey_name] if hash.has_key?(:survey_name)
      hash[:questions].each_with_index {|q, i| survey.questions << Question.from_hash(q, i)} if hash.has_key?(:questions)
    end

    survey
  end

  def self.for_study_day(day)
    surveys = []
    surveys  = self.where(delivery: "daily")
    surveys += self.where(delivery: "even") if day.even?
    surveys += self.where(delivery: "odd") if day.odd?
    surveys += self.where(delivery: "weekly") if (day % 7) == 0
    surveys += self.where(delivery: "biweekly") if (day % 14) == 0

    # Do the right thing for month.  This probably isn't it.
    surveys += self.where(delivery: "monthly") if (day % 30) == 0

    surveys
  end

  def task
    Task.where(name: self.name).first
  end
  
  def rename_to(new_name)
    self.name = new_name
    self.save
  end
  
  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:questions] = self.questions.order(position: :asc, created_at: :asc).as_json(options)
    self.properties.each { |p| res[p.name] = ((JSON.parse(p.value).as_json(options)) rescue p.value) }
    
    res
  end
end
