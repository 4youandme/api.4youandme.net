class FeedSection
  include ActiveModel::Model
  attr_accessor :position, :title, :tiles

  def self.tiles_for_study_day(day)
    tiles = Tile.for_study_day(day)

    # tasks = Task.for_study_day(day)
    # surveys = Survey.for_study_day(day)

    tiles
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:tiles] = self.tiles.as_json(options) if options[:legacy] && self.tiles.length > 0
    res
  end

end
