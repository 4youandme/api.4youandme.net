# question_option.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Jan  3 17:53:18 2020.
class QuestionOption < ApplicationRecord
  belongs_to :question
  has_many :targets, as: :target_source, dependent: :destroy
  has_many :properties, as: :owner
  has_many :contents, as: :contentable
  
  def self.from_hash(hash, pos=0)
    cols = self.column_names
    cols.delete("id")
    option = self.new(hash.slice(*cols))
    option.position ||= pos + 1

    option.contents.new(hash.slice(*Content::COLS))

    # BEG: How to make TECH-DEBT for fun and profit
    option.properties << Property.new(name: "details", value: hash[:details].to_json) if hash.has_key?(:details)
    # FIN: How to make TECH-DEBT for fun and profit

    hash[:targets].each_with_index {|t, i| option.targets << Target.from_hash(t, i)} if hash.has_key?(:targets)

    option
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    content = self.contents.first
    if content
      res[:content] = content.content
      res[:content_type] = content.content_type
      res[:style_class] = content.style_class
    end

    # BEG: How to make TECH-DEBT for fun and profit
    debt_details = self.properties.where(name: "details").first.value rescue nil
    res[:details] = JSON.parse(debt_details) if debt_details
    # FIN: How to make TECH-DEBT for fun and profit

    if self.targets.length > 0
      res[:targets] = []
      self.targets.each {|t| res[:targets].push(t.as_json(options))}
    end

    if options[:legacy]
      res.delete(:created_at)
      res.delete(:updated_at)
    end

    res
  end
end
