# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class Device < ApplicationRecord
  belongs_to :user
  # Should really be device_events, right?
  has_many :phone_events, dependent: :destroy
  before_save :maybe_make_api_token
  default_scope { order(updated_at: :desc) }
  
  def self.from_hash(hash)
    hash[:api_token] ||= hash[:access_token]
    hash.delete(:access_token)
    hash[:user_id] ||= hash[:participant_id]
    hash.delete(:participant_id)
    hash[:id] ||= hash[:device_id]
    hash.delete(:device_id)

    cols = self.column_names
    device = self.where(id: hash[:id]).first
    device ||= self.new(hash.slice(*cols))
    device
  end
  
  def send_notification(options={alert: "Sample Message"})
    options = { alert: options } if options.is_a?(String)
    if self.os == "ios"
      # APNS.send_notification(self.token, options)
    elsif self.os == "android"
      data = { notification: { body: options[:alert], title: options[:title] } }
       GCM.send_notification(self.token, data)
    end
  end

  def make_api_token
    self.api_token = SecureRandom.uuid.delete('-')
  end

  def maybe_make_api_token
    self.make_api_token if not self.api_token
  end
  
  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:device_id] = self.id
    res[:participant_id] = self.user.id if self.user
    res[:access_token] = self.api_token
    res
  end

end
