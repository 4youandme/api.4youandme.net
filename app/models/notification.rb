class Notification < ApplicationRecord
  belongs_to :user
  after_create :set_delivery_date
  after_create :maybe_deliver

  def increment_badge
    user.notification_badge_count = (user.notification_badge_count || 0) + 1
    user.save
  end

  def self.deliverable
    self.where(delivered_at: nil).where.not(deliver_at: nil).where(':date >= deliver_at', date: Time.zone.now)
  end

  def self.deliver_pending
    self.deliverable.each { |n| n.deliver }
  end

  # Sends a "Click to view today's tasks" reminder to users at 6pm in their timezone.
  # To be run every hour, shortly after the hour (e.g. ~5 minutes after the hour)
  def self.send_reminders_cron_task
    User.all.each do |user|
      if Time.now.in_time_zone(user.timezone).hour == 18
        Notification.create({
          user: user,
          deliver_at: DateTime.now - 15.minutes, # see #maybe_deliver, also made to work with cron tasks
          title: "Daily Reminder",
          message: "Click to view today's tasks."
        })
      end
    end
  end

  def deliver
    should_send = true
    if self.send_condition
      m = self.send_condition.to_sym
      should_send = self.send(m) if self.respond_to?(m)
    end

    if should_send
      pref = user.delivery_preference

      # When it exists, self.notification_type overrides user.delivery_preference
      if self.notification_type ? NotificationType.is_sms?(self.notification_type) : NotificationType.is_sms?(pref)
        self.deliver_sms
      end

      if self.notification_type ? NotificationType.is_push?(self.notification_type) : NotificationType.is_push?(pref)
        self.deliver_pn
      end

      if self.notification_type ? NotificationType.is_email?(self.notification_type) : NotificationType.is_email?(pref)
        self.deliver_email
      end

      self.delivered_at = Time.zone.now
      save
    end
  end

  def deliver_sms
    client = Twilio::REST::Client.new rescue (Rails.logger.error("Can't create client - bad ENV?") and return)
    client.messages.create(
        from: TWILIO_NUM, to: self.user.phone.number,
        body: "#{self.title} #{self.message}"
    ) rescue (Rails.logger.error("Can't create message - bad number?") and return)
  end

  # We will try a couple of Firebase server keys in order to be able to
  # test notifications seamlessly across store and local builds.
  def deliver_pn
    increment_badge
    success = false
    server_keys = {production: ENV['FCM_PRODUCTION_KEY'],
                   development: ENV['FCM_DEVELOPMENT_KEY'],
                   localnet: ENV['FCM_LOCALNET_KEY']}
    options = {notification: {title: self.title, body: self.message}, data: (payload || {})}
    registration_ids = user.devices.where.not(push_token: nil).pluck(:push_token).uniq

    server_keys.each do |environment, server_key|
      next if server_key.blank?
      fcm = FCM.new(server_key)
      registration_ids.each_slice(20) do |batch|
        response = fcm.send(batch, options)
        rbody = JSON.parse(response[:body]) rescue nil
        success = true if rbody and rbody["success"] == 1
        Rails.logger.info("PN Failed because #{rbody['results'][0]['error']} for #{environment}") if not success and rbody
        break if not success
      end
    end
    success
  end

  def deliver_email
    if user.email
      UserMailer.notification(user: user, email_object: user.email, notification: self).deliver_later
    end
  end

  def maybe_deliver
    self.deliver if not self.delivered_at and self.deliver_at and self.deliver_at <= Time.zone.now
  end

  def payload
    JSON.parse(self.payload_json) rescue nil
  end

  def payload=(obj)
    self.payload_json = (obj and obj.to_json) || nil
  end

  def set_delivery_date
    self.deliver_at ||= Time.zone.now
    self.save
  end


  # Returns true if the user is validated but has not yet reached the dashboard
  def validated_user_not_on_dashboard
    self.user.validated and self.user.status != "dashboard"
  end
end
