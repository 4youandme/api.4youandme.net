# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class Roster < ApplicationRecord
  before_save :canonicalize_phone
  before_save :unique_codename
  before_save :create_crc_codes
  before_save :maybe_set_after_validation
  belongs_to  :participant, class_name: "User", required: false
  has_paper_trail
  before_save :maybe_update_participant
  after_save :update_organization
  after_save do
    # keep Roster.is_test_user and User.is_test_user in sync. Probably we want to factor out User#is_test_user at some point
    unless participant.nil?
      participant.update(is_test_user: is_test_user)
    end
  end

  delegate :study_month, to: :participant, allow_nil: true
  
  validates :phone, uniqueness: true
  
  def self.lookup(num)
    self.where(phone: Phone.canonicalize(num)).first
  end
  
  def self.generate_codename
    self.new.generate_codename
  end
  
  def self.from_hash(hash)
    roster = self.where(id: hash["id"]).first if hash["id"]
    roster ||= self.where(phone: Phone.canonicalize(hash["phone"])).first if hash["phone"]
    if not roster
      cols = self.column_names
      roster = self.new(hash.slice(*cols))
    end
    roster
  end
  
  def generate_codename
    /\w{4,6}+/.gen.capitalize + /\w{4,6}+/.gen.capitalize
  end
  
  def unique_codename
    if not self.codename
      candidate = self.generate_codename
      while self.class.where(codename: candidate).first != nil
        candidate = self.generate_codename
      end
      self.codename = candidate
    end
  end

  def regen_codename
    self.codename = nil
    self.save
  end

  def canonicalize_phone
    self.phone = Phone.canonicalize(self.phone) if self.phone
  end

  def make_digits(num, for_field)
    candidate = ""
    100.times do
      candidate = (rand(9) + 1).to_s
      candidate += (num - 1).times.map{rand(10)}.join
      hash = {}
      hash[for_field] = candidate
      if not self.class.where(hash).first
        break
      end
    end
    candidate
  end

  def user
    User.lookup(self.phone)
  end

  def update_participant
    self.participant = user if not @skip_update_participant
  end

  def maybe_update_participant
    update_participant if not self.participant_id
  end

  def user_status
    self.participant.status if self.participant
  end

  def status
    user_status
  end

  def status=(new_status)
    u = self.participant
    (u.status = new_status && save) if u
  end
      
  def crc_status
    self.user_status || self.after_phone_validation
  end
  
  def create_crc_codes
    self.empatica_id ||= make_digits(3, :empatica_id)
    self.ibdoc_id ||= make_digits(4, :ibdoc_id)
    self.bodyport_id ||= make_digits(4, :bodyport_id)
  end

  def to_s
    "Roster:#{self.phone}"
  end

  def show_phone_number
    self.phone
  end

  def api_token
    self.user.devices.where.not(api_token: nil).first.api_token rescue nil
  end
  
  def start_study(make_tile_schedule=true)
    u = self.user
    u ||= User.lookup(self.phone)
    u ||= User.create(phone: self.phone)
    self.participant_id = u.id
    self.save and self.reload
    self.participant.start_study
    self.participant.make_tile_schedule if make_tile_schedule
  end

  def study_started
    self.participant&.study_started_at
  end

  # Gets the end Date of the study for the participant.
  def study_end_date
    # Note that even though the property is named "study_ended_at," it is used to indicate when the study _will_ end
    participant = self.participant
    if participant
      participant.study_ended_at&.in_time_zone(participant.timezone)&.to_date
    end
  end

  # Set when the study will end for the user
  def study_end_date=(study_end_date)
    participant = self.participant
    date = study_end_date.to_date
    if participant && date
      participant.study_ended_at = date.in_time_zone(participant.timezone)
      participant.save
    end
  end

  def study_started=(val)
    if val
      self.start_study
    else
      if self.user
        self.user.study_started_at = nil
        self.user.save
      end
    end
    self.study_started
  end
  
  def opt_in_info
    self.participant.opt_in_answers rescue nil
  end

  def opt_in_relative_location_answer
    info = opt_in_info.select { |x| x[:question_text] == "Relative location" }.first rescue nil
    info[:answer_text] if info
  end
  
  def opt_in_sharing_options_answer
    info = opt_in_info.select { |x| x[:question_text] == "Sharing options"}.first rescue nil
    info[:answer_text] if info
  end
  
  def opt_in_future_research_contact_answer
    info = opt_in_info.select { |x| x[:question_text] == "Future research contact"}.first rescue nil
    info[:answer_text] if info
  end
  
  def last_app_access
    event = participant.phone_events.order(created_at: :desc).first rescue nil
    event.created_at if event
  end
  
  def pretty_last_app_access
    access = last_app_access
    access ? access.in_time_zone(participant.timezone).strftime("%D %T") : "Not yet opened"
  end

  def points_of_study_month(month=nil)
    participant.points_of_study_month(month) rescue 0
  end
  
  def reset
    u = self.participant
    self.participant_id = nil
    @skip_update_participant = true
    self.save
    @skip_update_participant = nil
    
    if u
      u.reset(make_schedule: false)
      u.devices.destroy_all
      u.destroy
    end
    self.reload
    self
  end

  # Here's some more tech debt.  I'll fix this someday.
  #
  #   registration
  #   studyVideo
  #   screeningQuestions
  #   consentSummaryScreens
  #   reviewConsent
  #   optInQuestions
  #   verifyEmail
  #   conclusion
  #   appsConnection
  #   dashboard
  #
  def self.states
    %w(registration studyVideo screeningQuestions consentSummaryScreens reviewConsent optInQuestions
       verifyEmail conclusion appsConnection dashboard)
  end

  # Ugh.
  def maybe_set_after_validation
    if not self.user_status
      # All users consent in the app now.
      # TODO: This shouldn't be hard-coded; it should be configurable per site
      self.after_phone_validation = "consentSummaryScreens"
    end
  end

  def update_organization
    if self.site
      org = Organization.lookup(self.site)
      if not org
        org = Organization.create(name: self.site.titleize)
        nick = self.site.downcase
        org.nicknames.where(nickname: nick).find_or_create
      end
    end
  end

  def lifetime_compliance
    compliance_since(study_started)
  end

  def compliance_since(beg=nil, fin=nil)
    beg ||= Time.zone.now - 7.days
    fin ||= Time.zone.now
    res = self.user.compliance_since(beg, fin).to_s + "%" if self.user
    res ||= "N/A"
    res
  end

  def as_json(options={})
    res = super(options)
    res[:study_started_at] = self.study_started
    res.delete(:participant_id) if options[:dump]
    res
  end
end
