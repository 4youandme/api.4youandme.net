# person.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Sep 10 10:39:30 2019.
require_dependency Openopus::Core::People::Engine.root.join('app', 'models', 'person').to_s
class Person < ApplicationRecord

  def send_notification(options={ alert: "A random alert", sound: "default", badge: 42 })
    devices.each do |device|
      device.send_notification(options)
    end
  end

  def self.from_hash(hash)
    cols = self.column_names
    person = self.where(id: hash[:id]).first
    person ||= self.new(hash.slice(*cols))
    emails = hash[:emails]
    phones = hash[:phones]
    addresses = hash[:addresses]
    nicknames = hash[:nicknames]

    emails.each {|eh| e = Email.from_hash(eh); e.save(validate: false); person.emails << e } if emails
    addresses.each {|ah| a = Address.from_hash(ah); a.save(validate: false); person.addresses << a } if addresses
    nicknames.each {|nh| n = Nickname.from_hash(nh); n.save(validate: false); person.nicknames << n } if nicknames
    phones.each {|ph| p = Phone.from_hash(ph); p.save(validate: false); person.phones << p } if phones
    person
  end

  # Sets the birthdate. Accepts either strings or Date objects
  def birthdate=(date)
    if date.class == Date
      date = date.strftime('%Y/%m/%d')
    end
    super(date)
  end
                      
  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:name] = self.name
    res[:phones] = self.phones.as_json
    res[:emails] = self.emails.as_json
    res
  end

  def show_phone_number
    Phone.obfuscate(self.phone.number) rescue ""
  end
end
