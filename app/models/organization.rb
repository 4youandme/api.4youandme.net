require_dependency Openopus::Core::People::Engine.root.join('app', 'models', 'organization').to_s
class Organization < ApplicationRecord
  has_many :consent_sections, as: :sectionable
  has_many :phones, as: :phoneable
  before_save :maybe_guess_timezone

  # Gets the most recent version of the consent sections
  def current_consent_sections
    self.consent_sections.select('distinct on (consent_sections.name) consent_sections.*').reorder('consent_sections.name, consent_sections.created_at DESC')
  end

  def phone
    self.phones.first
  end
  
  def phone=(number)
    if number.is_a?(Phone)
      self.phones << number unless self.phones.include?(number)
      return
    end

    if not number.blank?
      p = self.phones.first rescue nil
      p ||= Phone.create(phoneable_type: self.class.name, label: Label.get("Work"))
      self.phones << p unless self.phones.include?(p)
      p.number = number
      p.save
    end
  end

  def maybe_guess_timezone
    if not self.timezone
      ac = self.phone.areacode rescue nil
      self.timezone = AreaCode.where(code: ac.to_i).first.timezone if ac rescue nil

      if not self.timezone and self.phone
        self.timezone = "Europe/London" if self.phone.number.starts_with?("+44")
        self.timezone = "Europe/Rome" if self.phone.number.starts_with?("+39")
      end
      self.timezone ||= "Europe/London"
    end
  end

  def self.lookup(thing)
    org   = self.where(name: thing.to_s.titleize).first
    org ||= self.includes(:nicknames).where(nicknames: { nickname: thing }).first
    org ||= self.includes(:nicknames).where(nicknames: { nickname: thing.to_s.downcase }).first
    org
  end
end
