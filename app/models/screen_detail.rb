class ScreenDetail < ApplicationRecord
  belongs_to :screen
  has_many :properties, as: :owner
  has_many :contents, as: :contentable
  has_one :redirect_detail
  has_many :buttons, as: :buttonable

  def maybe_add_property(hash, sym)
    self.properties.new(name: sym.to_s, value: hash[sym]) if hash.has_key?(sym)
  end

  def get_property(name)
    candidate = self.properties.where(name: name.to_s).first
    candidate.value if candidate
  end
  
  def self.from_hash(hash, pos=0)
    cols = self.column_names
    cols.delete("id")
    detail = self.new(hash.slice(*cols))
    detail.detail_type = hash[:type] if hash.has_key?(:type)
    detail.position ||= pos + 1
    detail.contents << Content.from_hash(hash.slice(:content, :content_type, :style_class))
    detail.maybe_add_property(hash, :oauth_url)
    detail.redirect_detail = RedirectDetail.new(hash[:redirect_details]) if hash.has_key?(:redirect_details)
    detail.buttons.new(hash[:button].slice(:label, :target)) if hash.has_key?(:button)

    detail
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:type] = res.delete(:detail_type)
    
    if self.get_property(:oauth_url)
      res[:redirect_detail] = self.redirect_detail.as_json(options)
      res[:oauth_url] = self.get_property(:oauth_url)
    end

    content = self.contents.first
    if content
      res[:content] = content.content
      res[:content_type] = content.content_type
      res[:style_class] = content.style_class
    end

    res = res.merge(Button.json_helper(self, options))

    res
  end
end
