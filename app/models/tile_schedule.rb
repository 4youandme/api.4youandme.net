require 'action_view'
include ActionView::Helpers::DateHelper

class ActiveRecord::Relation
  def format
    self.each.collect {|x| x.format}
  end
end

class TileSchedule < ApplicationRecord
  belongs_to :user
  belongs_to :tile
  # default_scope { order(active_at: :asc) }
  scope :find_named, -> (name) { includes(:tile).eager_load(:tile).where(tiles: { name: name }).order(study_day: :asc) }
  scope :cognitive_tasks, -> () { includes(:tile).where(tiles: { referenced_type: "cognitive-task" }).order(study_day: :asc) }
  scope :active_tasks, -> () { includes(:tile).where(tiles: { referenced_type: "active-task" }).order(study_day: :asc) }
  scope :quick_activity, -> () { includes(:tile).where(tiles: { referenced_type: "ema" }).order(study_day: :asc) }

  scope :currently_active, -> () {
    order(study_day: :asc).where(':time between active_at and expires_at', time: Time.zone.now)
  }

  # Notifications can sent these time periods before the tiles expire
  BEFORE_EXPIRATION_NOTIFICATION_TIME = 30.minutes
  TWELVE_HOUR_BEFORE_EXPIRATION_NOTIFICATION_TIME = 11.hours + 30.minutes
  THIRTYSIX_HOUR_BEFORE_EXPIRATION_NOTIFICATION_TIME = 24.hours + 11.hours + 30.minutes


  delegate :name, to: :tile

  def self.incomplete
    self.where(completed_at: nil)
  end
  
  def self.noninfo
    self.includes(:tile).where.not(tiles: { tile_type: "info" })
  end
  
  def self.info
    self.includes(:tile).where(tiles: { tile_type: "info" })
  end
  
  def self.expiring_within(duration)
    earlier = Time.zone.now.change(usec: 0)
    later = (earlier + duration).change(usec: 0)
    self.where(expires_at: earlier...later).order(active_at: :asc)
  end

  def self.expiring_in_around(duration, fudge=2.5.minutes)
    earlier = (Time.zone.now.change(usec: 0) + (duration - fudge)).change(usec: 0)
    later = (earlier + (2 * fudge)).change(usec: 0)
    expiring = self.order(active_at: :asc)
    expiring = expiring.where(expires_at: earlier..later)
  end

  def self.expiring_in(duration)
    self.expiring_in_around(duration, 30.seconds)
  end

  def self.recently_expired(distance=30.minutes)
    self.where(expires_at: (Time.zone.now - distance)..(Time.zone.now))
  end
  
  def self.active_in(duration)
    earlier = Time.zone.now.change(usec: 0)
    later = (earlier + duration).change(usec: 0)
    query = self.order(study_day: :asc, active_at: :asc)
    query = query.where(":time < active_at", time: earlier)
    query = query.where(':time between active_at and expires_at', time: later)
    query
  end

  def self.just_started(duration=5.minutes)
    beg = Time.zone.now.change(usec: 0)
    query = self.order(study_day: :asc, active_at: :asc)
    query = query.where(":beg between active_at and (active_at + interval ':duration' second)",
                        beg: beg, duration: duration)
    query
  end

  def just_started(duration=5.minutes)
    now = Time.zone.now.change(usec: 0)
    now > active_at && now < active_at + duration
  end

  # Checks if the tile is active and will expire within the next +within+ Duration
  #
  # +within+ The duration to check
  # +:fudge+ Add a fudge factor to the beginning and end of the time range checked
  def expiring_soon(within=30.minutes, options={})
    fudge = options[:fudge] || 0.minutes
    now = Time.zone.now.change(usec: 0)
    some_time_before_expiration = expires_at - within

    active_at < now + fudge && now > some_time_before_expiration - fudge && now < expires_at + fudge
  end

  def reminding(within=60.minutes)
    result = false
    if reminded_at
      now = Time.zone.now.change(usec: 0)
      result = (active_at < now && expires_at > now && now < reminded_at + within)
    end
    result
  end

  # Returns the item scheduled; either a Survey or a Task
  def scheduled
    result = nil
    result = Survey.get(self.tile.referenced) if self.tile.tile_type == "survey-reference"
    result = Task.get(self.tile.referenced) if self.tile.tile_type == "task-reference"
    if self.tile.tile_type == "task"
      result   = self.tile if self.tile.name == "ema"
      result ||= Task.get(self.tile.name)
    end
    result
  end

  # Updates the surveys to be scheduled for the entire day.
  # Used by rake task; maybe delete after task is no longer needed
  def update_survey_schedule
    return unless self.tile.referenced_type == "survey"
    self.active_at = self.active_at.in_time_zone(self.user.timezone).beginning_of_day.utc
    self.expires_at = self.expires_at.in_time_zone(self.user.timezone).end_of_day.utc
    self.save 
  end

  def format
    res = "<TS"
    res += ": #{self.id[0..5]...} - #{self.user ? self.user.phone.number : '+0 (000) 000-0000'} "
    res += "#{self.tile.tile_type}:#{self.tile.name}:"
    if self.completed_at
      res += "[completed:#{self.completed_at.in_time_zone(self.user.timezone)}]"
    else
      res += "[incomplete]"
    end

    res += "[active:#{self.active_at.in_time_zone(self.user.timezone)} for #{distance_of_time_in_words(self.expires_at - self.active_at)}]"
    res += ">"
    res
  end

  def self.canonicalize_task_name(item, options={})
    return item if not item
    item = item.gsub(/[-](copy|task).*$/, "") if options[:remove_copy_task]
    # remove timestamp
    item = item.gsub(/\s\d{4}\s\d{2}\s\d{2}.*$/, '') if options[:remove_copy_task]
    result = item.underscore.titleize
    result = "Quick Activity" if item.downcase == "ema"
    result = "Quick Activity Follow Up" if item.downcase == "ema-add-on"
    result = result.gsub("Ibd", "IBD").gsub("Oci", "OCI").gsub("Task Task", "Task")
  end
  
  def send_notification(options={})
    # Don't send any task reminder notifications. We just send a single reminder once a day at 6pm
    return

    # TODO: Once we're sure we don't want notifications, just delete this stuff

    now = Time.zone.now
    freq = options[:frequency] || 10.minutes
    force = options[:force]
    return if not force and not self.last_notified_at.blank? and (now - self.last_notified_at) < freq
    self.reload if not self.tile
    item = self.tile.referenced_type.titleize rescue nil
    item ||= self.tile.name
    item ||= "item"
    item = item.gsub(/[-](copy|task).*$/, "")
    item_type = item

    #
    # mwildman sez: "There should not be a push notification for 'quick activity'"
    # item = "Quick Activity" if item == "Ema"
    # +item+ will be capitalized because of the .titleize call above
    return if item == "Ema"
    
    item = "#{self.class.canonicalize_task_name(self.tile.name)} Survey" if item == "Survey"
    item = "#{self.class.canonicalize_task_name(self.tile.name)} Task" if item == "Active Task" or item == "Cognitive Task"

    task   = Task.ema if self.tile.name == "ema"
    task ||= Task.get(self.tile.referenced) if self.tile.tile_type == "task-reference"
    task ||= self.tile.task if self.tile.tile_type == "task"
    item = self.class.canonicalize_task_name(item) if item

    # Refactor task notifications and reminders to adhere to the document.
    #
    title = nil
    message = nil

    if item_type == "Survey"
      # No notifications
    elsif item_type == "Cognitive Task"
      if just_started
        message = "Your daily task is ready now"
      elsif reminding
        message = "Don't forget your daily task!"
      elsif expiring_soon(BEFORE_EXPIRATION_NOTIFICATION_TIME)
        message = "Only 30 minutes left to do your daily task and get up to 15 points"
      end
    elsif item_type == "Active Task"
      if just_started
        message = "It's time to record your video diary"  if self.tile.is_video_diary?
        message = "It's time to do your walk test" if self.tile.is_walk_test?
      elsif expiring_soon(BEFORE_EXPIRATION_NOTIFICATION_TIME)
        # If this is an active task, we've bothered them enough, and now there's only 30 minutes left to complete it.
        # Do NOT send a notification of any type.
        return if not message or not force
      end
    else
      title = "You've got something to do!"
      # The following is the generic way to notify about anything, at any time.
      if now < active_at
        dist = "will start in about " + distance_of_time_in_words(active_at - now)
      elsif now < expires_at
        if (expires_at - now) < 1.day
          dist = "will end in about " + distance_of_time_in_words(expires_at - now)
        else
          dist = "should be completed"
        end
      else
        dist = "ended about " + distance_of_time_in_words(expires_at - now) + " ago"
      end

      a = "a"
      a = "an" if %w(a e i o u).include?(item[0].downcase) rescue "a"
      message = "You have #{a} #{item} that #{dist}."
    end

    if title or message
      n = user.send_notification(title: title, message: message, task: task, force: force)
      self.last_notified_at = Time.zone.now
      self.save
      n
    end
  end
  
  def self.notify_of_readiness(duration=3.minutes, options={})
    recently_ready = self.just_started(duration).incomplete.noninfo
    recently_ready.sort_by { |ts| ts.user_id }
    recently_ready.each {|ts| ts.send_notification(options)}
  end

  def self.notify_of_pending_expiration(duration=30.minutes, options={})
    expiring_soon = self.expiring_in_around(duration, 2.minutes).incomplete.noninfo
    expiring_soon.sort_by { |ts| ts.user_id }
    expiring_soon.each {|ts| ts.send_notification(options) if ts.tile.tile_type  }
  end

  def self.notify_of_tasks(from_now=10.minutes, options={})
    upcoming = self.active_in(from_now).incomplete.noninfo
    upcoming.sort_by { |ts| ts.user_id }
    dist = distance_of_time_in_words((Time.zone.now + from_now) - Time.zone.now)
    Rails.logger.info("TILE_SCHEDULE::NOTIFY_OF_UPCOMING_TASKS(#{dist}) -- #{upcoming.count} Notifications")

    upcoming.each {|ts| ts.send_notification }
  end

  def self.periodic
    self.notify_of_readiness
    self.notify_of_pending_expiration(BEFORE_EXPIRATION_NOTIFICATION_TIME)
    self.notify_of_pending_expiration(TWELVE_HOUR_BEFORE_EXPIRATION_NOTIFICATION_TIME)
    self.notify_of_pending_expiration(THIRTYSIX_HOUR_BEFORE_EXPIRATION_NOTIFICATION_TIME)
  end
end
