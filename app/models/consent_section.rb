# consent_section.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Jan  3 17:54:47 2020.
class ConsentSection < ApplicationRecord
  @detailClass = "ConsentSectionInfo"
  belongs_to :sectionable, polymorphic: true, required: false

  has_many :section_details, class_name: @detailClass, dependent: :destroy
  has_many :questions, through: :section_details
  default_scope { order(position: :asc, created_at: :asc) }
  
  def self.from_hash(hash, org=nil)
    org ||= Organization.lookup("Default")
    json = ActiveSupport::HashWithIndifferentAccess.new(hash)
    section = self.new(json.slice(:name, :position, :gatekeeper))
    details = json[:section_details]

    if details
      details.each_with_index do |detail, pos|
        info = (@detailClass.constantize).from_hash(detail, pos)
        section.section_details << info
      end
    end

    org.consent_sections << section if org
    
    section
  end

  def self.from_file(file, org=nil)
    org ||= Organization.lookup("Default")
    json = JSON.parse(File.read(file))
    result = self.from_json_array(json, org) if json.is_a?(Array)
    result = self.from_hash(json, org) if json.is_a?(Hash)
    result.save if result.respond_to?(:save)
    result
  end

  def self.from_json_array(array_of_sections, org=nil)
    array_of_sections.each do |raw|
      section = self.from_hash(raw, org)
      section.save or raise "COULDN'T SAVE SECTION: " + section.errors.full_messages.to_sentence
    end
    org.consent_sections if org
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:section_details] = self.section_details.as_json(options) if options[:legacy] && self.section_details.length > 0

    res
  end

end
