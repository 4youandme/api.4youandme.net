# coding: utf-8
# user.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Sep 10 10:16:31 2019.
require_dependency Openopus::Core::People::Engine.root.join('app', 'models', 'user').to_s
require 'action_view'
include ActionView::Helpers::NumberHelper

class User < ApplicationRecord
  before_save :maybe_guess_timezone
  before_save :adjust_for_changed_zone
  after_commit :update_organization

  # Disabled for now (or maybe forever)
  # after_create :schedule_signup_sms_notifications

  has_many :point_rewards
  has_many :measurements
  has_many :answers
  has_many :devices
  has_many :phone_events, through: :devices
  has_one :validation_code
  has_one_attached :signed_consent
  has_many :access_tokens
  has_many :oauth_providers, through: :access_tokens
  has_many :completed_tasks
  has_many :notifications
  has_many :video_diaries, dependent: :destroy
  has_many :tile_schedules
  has_many :properties, as: :owner
  has_one :roster, foreign_key: "participant_id", required: false
  has_many :failed_tokens

  accepts_nested_attributes_for :devices

  def self.from_hash(hash)
    hash = ActiveSupport::HashWithIndifferentAccess.new(hash)
    cols = self.column_names
    user = self.where(id: hash[:id]).first
    user ||= self.new(hash.slice(*cols))
    person_hash = hash[:person]
    person = Person.from_hash(person_hash); person.save(validate: false); user.person = person
    user.save(validate: false)
    devices_array = hash[:devices]
    devices_array.each { |dh| d = Device.from_hash(dh); d.save(validate: false); user.devices << d; } if devices_array
    user
  end

  def roster_by_number
    numbers = self.phones.pluck(:number)
    Roster.where(phone: numbers).first
  end

  def start_study(at_specified_time = nil)
    at_specified_time ||= Time.zone.now.in_time_zone(self.timezone)
    self.study_started_at = at_specified_time.beginning_of_day
    self.save
    self.make_base_measurements
    self.register_camcog
  end

  def formatted_roster
    if roster
      "<a href='/admin/roster/#{self.roster.id rescue nil}'>Roster&lt;#{self.roster.id[0..5]}&gt;</a>".html_safe
    else
      "This cannot happen"
    end
  end

  def formatted_email
    e = self.email
    if e
      e.address
    else
      "<no email, yet>"
    end
  end

  def consent_link
    self.signed_consent.attachment && self.signed_consent.attachment.service_url
  end

  def site
    roster.site if roster
  end

  def device
    self.devices.order(updated_at: :desc).first
  end

  def device=(dev)
    self.devices << dev if not self.devices.include?(dev)
  end

  def load_measurements_from_file(file)
    json = ActiveSupport::HashWithIndifferentAccess.new(JSON.parse(File.read(file)))
    json.keys.each do |datatype|
      load_measurements_from_json_data(datatype.to_s, json[datatype][:data])
    end
  end

  def load_measurements_from_json_data(datatype, points)
    points.each do |point|
      gathered_at = point[:gathered_at].to_i
      gathered_at = (gathered_at.to_s[0, 10]).to_i if gathered_at.to_s.length == 13
      value = Measurement.text_as_value(point[:value] || point[:mood] || point[:energy])
      m = self.measurements.where(datatype: datatype, gathered_at: Time.at(gathered_at), value: value).first_or_create
    end
  end

  def make_fake_measurements(beg, fin, datatype)
    while beg < fin
      beg.beginning_of_day + rand(12).hours + rand(60).minutes
      self.measurements << Measurement.make_fake_datapoint(beg, datatype)
      beg += 24.hours
    end
  end

  def make_more_measurements(beg = nil, fin = nil)
    [:weight, :steps, :mood, :energy].each do |datatype|
      datatype_beg = beg
      datatype_beg ||= (self.measurements.order(ts: :desc).first.gathered_at + 1.day) rescue nil
      datatype_beg ||= Time.zone.now.in_time_zone(self.timezone)
      datatype_fin = fin
      datatype_fin ||= datatype_beg + 4.weeks
      make_fake_measurements(datatype_beg, datatype_fin, datatype)
    end
  end

  def xdata_for_measurements(datatype)
    datatype = datatype.to_sym if datatype.is_a?(String)
    xdata = {
      weight: { title: "Your Weight" },
      steps: { title: "Your Daily Steps" },
      mood: { title: "Your Self-Assessed Mood", categories: Measurement::MOOD_TYPES },
      energy: { title: "Your Self-Assessed Energy", categories: Measurement::ENERGY_TYPES }
    }
    xdata[datatype]
  end

  def get_measurements(options = { beg: nil, fin: nil, datatype: [:weight, :steps, :mood, :energy], freq: 1, buckets: nil })
    beg = options[:beg]
    fin = options[:fin]
    beg = Time.zone.parse(beg) if beg.is_a?(String)
    fin = Time.zone.parse(fin) if fin.is_a?(String)
    freq = 1 if freq == 0

    datatype = options[:datatype] || [:weight, :steps, :mood, :energy]
    datatype = [datatype] if datatype.is_a?(String)
    freq = options[:freq].to_f || 1
    freq = 1 if freq < 1

    beg ||= (Date.current.in_time_zone(self.timezone) - 15.days).beginning_of_month;
    fin ||= beg.end_of_month
    beg = beg.beginning_of_day + 12.hours
    fin = fin.beginning_of_day + 12.hours

    freq = ((fin.to_date - beg.to_date) / options[:buckets].to_i) if options[:buckets]
    skip = freq.days

    res = { beg: beg, fin: fin, freq: freq, stream: datatype }

    if datatype
      qstart = beg - (skip / 2.0)
      while (qstart < fin)
        range_start = qstart.beginning_of_day
        range_end = (qstart + skip).beginning_of_day
        datatype.each do |dt|
          sdt = dt.to_s.downcase
          res[sdt] ||= { data: [] }.merge(xdata_for_measurements(dt))
          value = self.measurements.where(datatype: sdt)
                      .where("gathered_at >= ? AND gathered_at < ?", range_start, range_end)
                      .average(:value)
          res[sdt][:data].push(value.to_f)
        end
        qstart += skip
      end
    end

    return res
  end

  def save_consent_document(filedata)
    input_filename = Rails.root.join("tmp/Consent-#{self.id}-before-mark.pdf")
    output_filename = Rails.root.join("tmp/Consent-#{self.id}-signed.pdf")
    nick = self.organization.nicknames.first.nickname rescue self.organization.nicknames.first.name
    mark = "#{Rails.root}/app/assets/images/#{nick.downcase}-consent-mark.png"
    mark = "#{Rails.root}/app/assets/images/consent-mark.png" if not File.exists?(mark)
    File.open(input_filename, "wb") { |f| f.write(filedata) }
    convert = "/usr/local/bin/convert" if File.exists?("/usr/local/bin/convert")
    convert ||= "/usr/bin/convert" if File.exists?("/usr/bin/convert")
    convert ||= "/opt/homebrew/bin/convert" if File.exists?("/opt/homebrew/bin/convert")

    command = "#{convert} -density 144 #{input_filename}[0] #{mark} -gravity NorthEast -geometry +40+86 -compose multiply -composite #{input_filename} -delete 1 #{output_filename}"
    system(command)

    self.gave_full_consent = true
    self.signed_consent.attach(io: File.open(output_filename, "rb"), filename: File.basename(output_filename), content_type: "application/pdf")
    result = self.save

    File.delete(input_filename)
    File.delete(output_filename)

    # Once the user has given full consent, we schedule push notifications to remind the user to get started.
    schedule_signup_notifications(
      "Thanks for your interest in Stress in Crohn's!",
      "Click to continue signing up",
      NotificationType::PUSH
    )

    result
  end

  def load_consent_from_file(filename)
    self.save_consent_document(File.read(filename))
  end

  # Returns the study day (1, 2, etc.). Returns "0" if the study has not started yet
  def study_day(of_date = nil)
    return 0 unless self.study_started_at
    of_date ||= Time.zone.now.in_time_zone(self.timezone)
    started = study_started_at.in_time_zone(self.timezone)
    return ((of_date.beginning_of_day.to_date - started.beginning_of_day.to_date).to_i + 1)
  end

  def date_of_study_day(day)
    self.study_started_at.in_time_zone + (day - 1).days
  end

  # Returns the study month (1, 2, etc.). Returns "0" if the study has not started yet
  def study_month(for_now = nil)
    return 0 unless self.study_started_at
    now = for_now
    now ||= Time.zone.now.in_time_zone(self.timezone).to_datetime
    beg = self.study_started_at.in_time_zone(self.timezone).to_datetime
    month = (beg..now).map { |d| [d.month, d.year] }.uniq.length
    month -= 1 if (beg + month.months).day > now.day
    month = 1 if month == 0
    month
  end

  # Returns the study month and the day in the study month
  def study_monthday(time = Time.zone.now)
    time = self.study_started_at + (time - 1).days if time.is_a?(Integer)
    beg = self.study_started_at.in_time_zone(self.timezone)
    fin = time.in_time_zone(self.timezone).end_of_day
    month = 1
    month += 1 while (beg + month.months) <= fin
    beg = beg + (month - 1).months
    day = (fin.to_date - beg.to_date).to_i + 1
    return month, day
  end

  def first_day_of_study_month(month = nil)
    month ||= study_month
    self.study_started_at + (month - 1).months
  end

  def last_day_of_study_month(month = nil)
    month ||= study_month
    (first_day_of_study_month(month) + 1.month) - 1.day
  end

  def points_of_study_month(month = nil)
    month ||= study_month
    study_points(first_day_of_study_month(month), last_day_of_study_month(month))
  end

  # @param +completed_task+ either a CompletedTask or a TileSchedule
  def add_point_reward_for_completed_task(completed_task)
    survey = completed_task.survey rescue nil
    points = nil
    trigger = nil
    right_now = Time.zone.now

    # Prep to make an info tile with reward information on it.
    body = "You've received POINTS points for completing the TASK"

    task_name = completed_task.task_type

    if completed_task.task_type == "ema"
      points = 5
      trigger = "completed ema"
      body = body.gsub(/TASK/, "quick activity")
    elsif completed_task.task_type == "survey"
      points = 5
      trigger = "completed survey"
    elsif %w(trail-making reaction-time).include?(completed_task.task_type)
      points = 10
      trigger = "completed #{completed_task.task_type}"
    elsif %w(video-diary walk-test).include?(completed_task.task_type)
      points = 30
      trigger = "completed #{completed_task.task_type}"
    elsif completed_task.task_type == 'web-task'
      # Currently there is only one web task; if there are more, we will need to put some logic here to assign points
      points = 30
      task_name = completed_task.task.human_name
      trigger = "completed #{completed_task.task_type}"
    end

    # Check if there is a duplicate completed task, and don't award points if there is.
    # There shouldn't be duplicates, but unfortunately sometimes there are because the client pushes the tasks more than once.
    duplicates = CompletedTask
                   .where(task_type: completed_task.task_type, started_at: completed_task.started_at)
                   .where.not(id: completed_task.id)
    return if duplicates.size > 0

    if survey and not survey.name.blank?
      body = body.gsub(/TASK/, "#{survey.title} survey")
    else
      body = body.gsub(/TASK/, "#{TileSchedule.canonicalize_task_name(task_name)} task")
    end

    if points
      body = body.gsub(/POINTS/, points.to_s)
      self.point_rewards.create(trigger: trigger, points: points, awarded_at: right_now)
      Tile.for_point_award(self, body, points, study_day(completed_task.completed_at))
    end
  end

  # Assign points for device compliance for the given day
  # Also updates points for the previous two days if they are missing (perhaps because the device hadn't been synced) or require a "bump" (because participants get bonuses for using your device 3+ days in a row)
  def award_device_compliance_points_for_study_day(study_day)

    # We should gracefully and purposefully handle the case of the device not having
    # been synced for 10 days, but having been worn for all those days.  Then the user
    # syncs the device, and now we EITHER:
    #  1. award for all previous days as if the device had been synced every day
    #  2. award for today, with a bump for wearing it the last 3 days
    #  3. award for today, with a bump for wearing AND syncing it the last 3 days
    #  4. award for today
    option = 1

    if option == 1
      award_device_points_for_measurements(study_day) if option == 1
    else
      measurements = Measurement.for_user_study_days(self, study_day, study_day).where(datasource: Measurement::WEARABLE_DATASOURCES)
      award_for_today(measurements, trigger: trigger, bump: :worn, today: today) if option == 2
      award_for_today(measurements, trigger: trigger, bump: :sync, today: today) if option == 3
      award_for_today(measurements, trigger: trigger, today: today) if option == 4
    end
  end

  def award_for_today(data, options)
    raise "Dead code path hasn't been updated. sorry :("
    #TODO: needs work
    m = data
    b = m.gathered_at.beginning_of_day
    e = b.end_of_day
    points = 5
    body = "You've received POINTS points for using your #{m.datasource.capitalize}"
    trigger = options[:trigger]
    trigger ||= "device compliance: #{m.datasource}"
    today = options[:today]
    today ||= Time.zone.now

    # If you already received points for compliance on this day, you don't get additional points.
    if self.point_rewards.where(trigger: trigger).where(awarded_at: b..e).first
      points = nil
    elsif options[:bump] == :sync
      # You get a bump if you've been awarded points for it 3 days in a row.
      if self.point_rewards.where(awarded_at: (b - 2.days)..today).where(trigger: trigger).count >= 2
        points = points * 2
        body = "You got extra points (POINTS) for wearing and syncing your #{m.datasource.capitalize} 3 days in a row!"
      end
    elsif options[:bump] == :worn
      if self.measurements.where(datasource: m.datasource, datatype: m.datatype).where(gathered_at: (b - 2.days)..e).count >= 2
        points = points * 2
        body = "You got extra points (POINTS) for wearing your #{m.datasource.capitalize} 3 days in a row!"
      end
    end
    return points, body
  end

  # Awards points for device compliance for +study_day+ and the previous two days.
  #
  # Participants receive 5pts/day/device.
  # If a device was worn for three days, then the participant receives 10pts/day/device for all those days (so we have to go back and add points for previous days)
  # If empatica, oura, and bodyport were _all_ worn for the past 3 days, the participant receives 20pts/day/device for all of those days
  def award_device_points_for_measurements(study_day)
    measurements = Measurement.for_user_study_days(self, [1, study_day - 2].max, study_day)
                              .where(%{((datasource='bodyport' AND datatype='weight') OR
                   (datasource='empatica' AND datatype='Worn correctly') OR
                   (datasource='empatica' AND datatype='data_uploaded') OR
                   (datasource='empatica' AND datatype='Free points!') OR
                   (datasource='oura' AND datatype='steps' AND value > 0))
                })

    measurement_sums = {}
    measurements.each do |m|
      measurement_sums[m.datasource] ||= 0
      measurement_sums[m.datasource] += 1
    end

    # If there are measurements for all days for all devices, the participant gets a 4x bonus (4x5pts=20pts) per award
    if measurement_sums.values.sum == 9 # 3 measurements x 3 days
      # create/update all PointRewards with 20pts
      points = 20
      Measurement::WEARABLE_DATASOURCES.each do |datasource|
        (study_day - 2..study_day).each do |sd|
          PointReward.create_or_increase_points(sd,
                                                PointReward.device_compliance_trigger_name(datasource),
                                                points,
                                                self)
        end

        heading = "You're on a streak!"
        body = "You've received 60 points for using your Bodyport, Emaptica, and Oura devices for three or more days. You will earn 4x the points for each day in this streak"

        # Create a single tile for all the datasources
        # Show the tile on the _next_ study day
        Tile.for_point_award(self, body, heading, points, study_day + 1)
      end
    else
      # Participant does not have a measurement for every device on all of the past three days
      measurement_sums.each do |datasource, count|
        trigger = PointReward.device_compliance_trigger_name(datasource)
        # if there are measurements for all of the past three days, the participant gets a 2x bonus (2x5pts=10pts) per award
        # Otherwise, the user just gets five points
        if count == 3
          points = 10
          heading = "You're on a streak!"
          body = "You've received #{points} points for using your #{datasource.capitalize} for three or more days. You will earn 2x the points for each day in this streak"

          # Bump the previous days
          (study_day - 2..study_day).each do |sd|
            PointReward.create_or_increase_points(sd, trigger, points, self)
          end
          # Show the tile on the _next_ study day
          Tile.for_point_award(self, body, heading, points, study_day + 1)
        else
          # just reward points for each day
          points = 5
          heading = nil
          measurements.where(datasource: datasource).each do |m|
            body = "You've received #{points} points for using your #{datasource.capitalize}"
            measurement_study_day = m.study_day
            new_reward = PointReward.create_or_increase_points(measurement_study_day, trigger, points, self)
            if new_reward
              Tile.for_point_award(self, body, heading, points, measurement_study_day + 1)
            end
          end

        end

      end
    end

  end

  def study_points(beg = nil, fin = nil)
    # Default is from beginning of current study month.
    fin ||= Time.zone.now
    beg ||= self.first_day_of_study_month(self.study_month)
    self.point_rewards.where(awarded_at: (beg.to_datetime)..(fin.to_datetime)).sum(:points)
  end

  def lifetime_points
    study_points(study_started_at)
  end

  def compliance_since(beg = nil, fin = nil)
    beg ||= Time.zone.now - 7.days
    fin ||= Time.zone.now
    points_accrued = study_points(beg, fin)
    days = (fin - beg) / 1.day
    points_expected = 59.28 * days
    ((points_accrued.to_f / points_expected.to_f) * 100.0).round(2)
  end

  def compute_perfect_points_for_study_day(day)
    day * 59.28
  end

  def compute_compliance
    # Given study day X you should be at Y points.  The percentage of points that
    # you have vs the "perfect" number of points says what kind of "status" you have.
    perfect_points = compute_perfect_points_for_study_day(self.study_day).to_f
    my_points = study_points(self.study_started_at.to_date).to_f
    percent_of_perfect = (100 * (my_points / perfect_points))
    percent_of_perfect
  end

  def compute_roster_compliance
    r = self.roster
    if r and r.respond_to?(:compliance)
      r.compliance = compliance_since(study_started_at)
      r.compliance_last_week = compliance_since(Time.zone.now - 7.days)
      r.compliance_computed_at = Time.zone.now
      r.save
    end
  end

  def points_compliance_mapping(points)
    result = { title: "Beginner", stars: -1 }

    if points >= 18000
      result = { title: "Groundbreaking Contributor", stars: 5 }
    elsif points >= 12000
      result = { title: "Super Contributor", stars: 4 }
    elsif points >= 7000
      result = { title: "Major Contributor", stars: 4 }
    elsif points >= 3000
      result = { title: "Growing Contributor", stars: 4 }
    else
      result = { title: "Beginning Contributor", stars: 1 }
    end

    result
  end

  def study_progress_status
    points_compliance_mapping(lifetime_points)[:title]
  end

  def study_progress
    if not self.study_started_at
      return {
        heading: "Thank you for joining the study!",
        body: "You'll see the number of points you've earned here"
      }
    end
    # It's completely ridiculous that we have to return a one-off object called "summary_tile"
    # instead of just creating a summary tile.
    summary_tile = {}
    metadata = { days_complete: self.study_day,
                 status: self.study_progress_status,
                 avg_compliance: compute_compliance.round(2),
                 lifetime_points: lifetime_points,
                 num_stars: (compute_compliance / 20.0).round(2)
    }
    summary_tile[:metadata] = metadata
    heading = "You've been participating in this study for #{ActionController::Base.helpers.pluralize(study_day, 'day')} so far!"
    metadata[:num_stars] = points_compliance_mapping(lifetime_points)[:stars]
    body = "You have earned #{number_with_delimiter(lifetime_points)} lifetime points which "
    body += "makes you a #{points_compliance_mapping(lifetime_points)[:title].upcase} to science. Keep up the good work!"

    summary_tile[:heading] = heading
    summary_tile[:body] = body
    summary_tile[:content] = "<h1>#{heading}</h1> <p>#{body}</p>"
    summary_tile[:content_type] = "text/html"

    summary_tile
  end

  def pick_random_time(day, beg, fin)
    hour = rand(beg - fin) + beg
    minute = rand(60)
    day.in_time_zone(self.timezone) + hour.hours + minute.minutes
  end

  def dump_schedule(starting_study_day = nil, ending_study_day = nil)
    starting_study_day ||= 1
    ending_study_day ||= starting_study_day + 30
    (starting_study_day..ending_study_day).each do |day|
      scheds = self.tile_schedules.order(study_day: :asc).where(study_day: day)
      puts "STUDY-DAY: #{day} (#{(self.study_started_at.to_date + (day -1).days).strftime('%A')}) >> #{scheds.collect { |t| t.tile.name }}\n"
    end
  end

  # Takes a datetime and returns a time in the same time but in UTC.
  # E.g., if +datetime+ is 3pm PST, this function will return 3PM UTC.
  # For stupid things that can't adjust times for timezones themselves
  def datetime_tz_adjusted datetime
    in_tz = datetime.in_time_zone(self.timezone)
    DateTime.new(in_tz.year, in_tz.month, in_tz.day, in_tz.hour, in_tz.min, in_tz.sec)
  end

  # Get objects needed to populate FullCalendar JavaScript calendar.
  # +options[:strip_tz]+: Strip timezone from times, and adjust times to user's timezone.
  def schedule_as_calendar_events(options = {})
    beg = options[:beg] || self.study_started_at
    fin = options[:fin] || self.study_ended_at || beg + 3.months
    strip_tz = options[:strip_tz] || false
    include = options[:include]
    include ||= [:task, :survey, :info]
    include.map! { |x| x.to_s }
    include.push("task-reference") if include.include?("task")
    res = self.tile_schedules.where(active_at: beg..fin).collect do |ts|
      if not ts.tile.name.blank? and include.include?(ts.tile.tile_type)
        ends_at = ts.completed_at || ts.expires_at
        ends_at = beg + 3.hours if ts.tile.is_persistent
        bgcolor = "#31668e"
        bgcolor = "#1a898d" if ts.completed_at
        { type: ts.tile.tile_type,
          title: TileSchedule.canonicalize_task_name(ts.tile.name),
          start: strip_tz ? datetime_tz_adjusted(ts.active_at) : ts.active_at,
          end: strip_tz ? datetime_tz_adjusted(ends_at) : ends_at,
          persistent: ts.tile.is_persistent,
          study_day: ts.study_day,
          backgroundColor: bgcolor
        }
      end
    end.compact

    # Let's add a Study Day XXX event for every day on the calendar.
    date = beg.in_time_zone(self.timezone).beginning_of_day.utc
    while (date <= fin)
      day = study_day(date)
      res.unshift({ backgroundColor: "blue", type: "info", title: "[Study Day #{day}]", allDay: true, start: date })
      date += 1.day
    end

    # Maybe add "events" that show device compliance?
    date = beg.in_time_zone(self.timezone).beginning_of_day
    while (date <= fin)
      study_day = self.study_day(date)

      measurements = Measurement.for_user_study_days(self, study_day, study_day)

      empatica = measurements.where(datasource: "empatica", datatype: ["Worn correctly", "data_uploaded", "Free points!"]).first
      bodyport = measurements.where(datasource: "bodyport", datatype: "weight").first
      rescuetime = measurements.where(datasource: "rescuetime", datatype: "mobile").first
      pooply = measurements.where(datasource: "pooply", datatype: "pooply").first
      oura = measurements.where(datasource: "oura", datatype: "steps").where("value > 0").first

      compliance = []
      compliance.push("E") if empatica
      compliance.push("B") if bodyport
      compliance.push("R") if rescuetime
      compliance.push("P") if pooply
      compliance.push("O") if oura

      compliance_string = ("Devices: [" + compliance.join(",") + "]") if not compliance.blank?
      res.unshift({ backgroundColor: "lightgreen", textColor: "black", type: "info", allDay: true, title: compliance_string, start: date }) if not compliance.blank?
      date += 1.day
    end

    res
  end

  # (Re)Build a tile schedule for this user, tile by tile.
  def make_tile_schedule(options = {})
    # We are destroying the existing one, which also removes the information that a particular
    # tile has been completed.  We may need to do a mark/sweep over completed_tasks after we
    # make a new schedule.
    tile_schedules.destroy_all unless options[:study_day]

    # Iterate over the tiles, creating a schedule for them for the next 365 days.
    # Don't grab "tile copies"
    Tile.where.not("name like '%copy'").where.not("name like '%UTC'").where(tileable_id: nil).where.not(delivery: nil).each do |tile|
      schedule_tile(tile, options)
    end
  end

  # Schedule the particular tile
  def schedule_tile(tile, options={})
    # Setup some "constants"
    first_study_day = study_started_at.in_time_zone(self.timezone).to_date
    study_end = first_study_day + 1.year
    ema_tile = Tile.ema_tile

    # Job 1.  Find the iterative spacing, and the start day for that.
    day = first_study_day + (tile.first_study_day - 1).days
    weekday = day.strftime("%A").downcase
    delivery = tile.delivery.downcase

    # Maybe advance because we're on the wrong weekday.
    while delivery.include?("day") and not delivery.include?(weekday) do
      day = day + 1.day
      weekday = day.strftime("%A").downcase
    end

    # Now compute the skip amount for this tile.
    skip = 1.day
    skip = 1.weeks if delivery.include?("week")
    skip = 1.month if delivery.include?("month")
    skip = skip * 2 if delivery.include?("even") or delivery.include?("odd")
    skip = skip * 2 if delivery.include?("biweekly")
    skip = 20.years if delivery.include?("once")

    if delivery.include?("every")
      m = delivery.match(/every (([0-9]*))/)
      multiplier = m[1].to_f if m and not m[1].blank?
      if not multiplier
        everys = %w[first second third fourth fifth six seventh eighth ninth tenth]
        m = delivery.match(/every (first|second|third|fourth|fifth|six|seventh|eighth|ninth|tenth)/)
        multiplier = (everys.index(m[1]) + 1) if m
      end
      mmultiplier = 1 if not multiplier or multiplier < 1
      skip = skip * multiplier
    end

    # DAY is the first day to schedule this tile.  Unless delivery is "once",
    # we will iterate over the remaining possible days skipping days according
    # to our strict rules.
    while day < study_end do
      ts_study_day = ((day - self.study_started_at.in_time_zone(self.timezone).to_date).to_i + 1)
      ts_study_week = (ts_study_day / 7.0).ceil
      ts_study_month = self.study_month(self.study_started_at.in_time_zone(self.timezone).to_date + (ts_study_day - 1).days)

      # If this tile should only appear on "odd" days, we should skip if the day isn't "odd".
      # Note that we chose to use the study_day, and not the day of the month.  Easy change
      # if desired.  Ditto for "even".
      if delivery.include?("daily")
        (day += skip and next) if delivery.include?("odd") and not ts_study_day.odd?
        (day += skip and next) if delivery.include?("even") and not ts_study_day.even?
      end

      # If this tile should only appear on "odd" weeks, we should skip if the week isn't "odd".
      if delivery.include?("weekly")
        (day += (skip / 2) and next) if delivery.include?("odd") and not ts_study_week.odd?
        (day += (skip / 2) and next) if delivery.include?("even") and not ts_study_week.even?
      end

      # If this tile should only appear on "odd" months, we should skip if the month isn't "odd".
      if delivery.include?("monthly")
        (day += (skip / 2) and next) if delivery.include?("odd") and not ts_study_month.odd?
        (day += (skip / 2) and next) if delivery.include?("even") and not ts_study_month.even?
      end

      ts = self.tile_schedules.new(tile: tile, study_day: ts_study_day)

      # PLEASE NOTE THAT THE DATABASE HAS TIMESTAMPS IN UTC.
      # Select the correct time to start this survey or cognitive task or active task or...

      # Everyday day has a "quick-activity".  So find or make one for this day.
      qa = self.tile_schedules.where(study_day: ts_study_day, tile: ema_tile).first_or_create
      # All quick activities are available 12am to 11:59pm
      qa.active_at = day.in_time_zone(self.timezone).beginning_of_day.utc
      qa.expires_at = day.in_time_zone(self.timezone).end_of_day.utc
      qa.save

      if not options[:study_day] or options[:study_day] == ts_study_day

        if tile.referenced_type == "cognitive-task"
          ts.active_at = qa.active_at + 1.minute
          ts.expires_at = qa.active_at + 24.hours - 2.minute
        end

        if tile.referenced_type == "survey" or tile.referenced_type == "active-task"
          ts.active_at = day.in_time_zone(self.timezone).beginning_of_day.utc
          ts.expires_at = day.in_time_zone(self.timezone).end_of_day.utc
        end

        ts = qa if tile == ema_tile

        # A "weeedle bit" of magic here.  If the tile is supposed to persist, that means leave it around
        # until complete, or until the next one shows up.  That's easy - set the expiration date to the
        # next time it's supposed to show up.
        ts.expires_at = (ts.active_at + skip) if tile.is_persistent
        existing = self.tile_schedules.where(ts.attributes.slice("tile_id", "study_day")).first
        ts.save if not existing

      end

      # Now bump to the next day.
      day += skip
    end

  end

  # Study month
  #
  # How many months participant has been in the study (starts with month: 1)
  #   Uses calendar month + calendar day as a boundary. If a study starts on Dec 26
  # then month++ occurs on Jan 26, Feb 26, etc.
  #
  # Study day
  #   Day of the study month.  So study starts on Dec 26.  Dec 26's study_day is 1, Jan 3's is 9, etc.
  #   The day denominator is the number of days that there are in the study month
  #   (e.g., days between Dec 26 and Jan 26, which would be the number of days in December)
  #
  # Points
  #   Point scheme is detailed here
  #   Points shown should be total points earned during this study month
  #   [ I THINK THIS IS FALSE, BECAUSE THE INCENTIVES ARE FOR LIFETIME POINTS ]
  #   (resets at 0 on the first day of each study month)
  #
  def beginning_of_study_month(month = nil)
    month ||= study_month
    study_started_at.in_time_zone(self.timezone) + (month - 1).months
  end

  def info_tiles
    tiles = []
    if self.is_test_user
      tile = Tile.get("info-points-awarded")
      tiles.push(tile)
    end

    tiles
  end

  def feed_metadata
    res = {}
    if self.study_started_at
      res[:points] = self.study_points
      res[:month], res[:day_numerator] = study_monthday
      res[:day_denominator] = (beginning_of_study_month(study_month + 1).to_date - beginning_of_study_month.to_date).to_i
    else
      # study hasn't started. Show all zeroes.
      res = {
        points: 0,
        month: 0,
        day_denominator: 0
      }
    end

    res
  end

  def quick_activity(tile_id = nil)
    tile_id ||= SecureRandom.uuid
    { id: tile_id, position: 1, type: "ema" }
  end

  def send_all_surveys
    Survey.all.each { |s| self.send_tile(s.name) }
  end

  def test_feed
    res = { feed_metadata: self.feed_metadata, quick_activity: self.quick_activity }
    sections = []
    ftitle = "TEST Feed #{self.name.blank? ? '' : 'for ' + self.name}"
    section = FeedSection.new(title: ftitle)
    section.tiles = []

    # Add some information tiles at the top.
    tiles = info_tiles
    section.tiles += tiles

    # Add tiles for tasks that aren't surveys.
    task_names = %w(video-diary reaction-time trail-making walk-test)
    task_names.each { |name| section.tiles.push(Tile.get(name)) }

    if self.phone.number == "+1 (666) 666-6666"
      # Show all the surveys, each in a single tile:
      section.tiles.each { |t| t[:id] ||= SecureRandom.uuid }
      sections.push(section)
      section = FeedSection.new(title: "ALL THE SURVEYS", tiles: [])
      surveys = Survey.all
    else
      # Show just the surveys with the following names:
      survey_names = %w(healthcare-utilization-base family-medical-history pilot healthcare-utilization-ongoing)
      surveys = Survey.where(name: survey_names)
    end

    surveys.each { |survey| section.tiles.push(Tile.for_survey(survey)) }
    section.tiles.each { |t| t[:id] ||= SecureRandom.uuid }
    sections.push(section)

    res[:feed_sections] = sections.as_json(legacy: true)

    res
  end

  def next_scheduled_action
    ts = self.tile_schedules.includes(:tile).order(active_at: :asc).
      where(completed_at: nil).where("active_at > :date", date: Time.zone.now).
      where.not(tiles: { tile_type: ["info"] }).first
    ts.reload if ts and not ts.tile
    ts
  end

  def drop_tile(tile = nil)
    ts = tile_schedules.order(active_at: :asc).where(':date between active_at and expires_at', date: Time.zone.now).first if not tile
    ts.destroy if ts
  end

  def feed(options = {})
    self.update_column(:last_fed_at, Time.zone.now) if self.respond_to?(:last_fed_at)

    res = { feed_metadata: self.feed_metadata }
    title_preamble = "" # previously was "Feed for "

    section = FeedSection.new(title: title_preamble + Time.zone.now.in_time_zone(self.timezone).strftime("%b %-d %Y"))

    # if the user hasn't started yet, show the welcome tile. That's now what the Welcome tile is for:
    unless self.study_started_at
      section.tiles = [Tile.copy("welcome")]
      res[:feed_sections] = [section].as_json(legacy: true)
      return res
    end

    # Show only the end tile if the study has ended. We're not showing past tiles, since I haven't been able to figure
    # out how to determine which tiles should still be shown (e.g. the points awarded tiles) and which shouldn't (e.g.
    # the "do this activity" tiles)
    if !self.study_ended_at.nil? and self.study_ended_at <= DateTime.now
      section.tiles = [Tile.copy("study-ended")]
      res[:feed_sections] = [section].as_json(legacy: true)
      return res
    end

    sections = []

    # Get the feed from the already made schedule.
    now = Time.zone.now
    scheds = tile_schedules.where(':date BETWEEN ACTIVE_AT AND EXPIRES_AT', date: now)
                           .where(completed_at: nil)
                           .order(active_at: :desc, created_at: :desc)

    section_date = scheds[0].active_at.beginning_of_day rescue Time.zone.now.beginning_of_day
    section = FeedSection.new(title: title_preamble + section_date.strftime("%b %-d %Y"))

    section.tiles = []

    skip_reminders = []
    num_tasks = 0

    ema_tile = Tile.ema_tile

    scheds.each do |tile_schedule|
      tile = nil

      # Of course, there's another special case for EMA.
      if tile_schedule.tile.referenced_type == "ema"
        res[:quick_activity] = self.quick_activity(ema_tile.id)
        num_tasks += 1
      else
        if section_date != tile_schedule.active_at.in_time_zone(self.timezone).beginning_of_day
          sections.push(section) if section.tiles.length > 0
          section_date = tile_schedule.active_at.in_time_zone(self.timezone).beginning_of_day
          section = FeedSection.new(title: title_preamble + section_date.in_time_zone(self.timezone).strftime("%b %-d %Y"))
          section.tiles = []
        end

        # NB `tile` might be json or it might be a Tile... :/
        tile = Tile.for_survey(Survey.get(tile_schedule.tile.referenced)) if tile_schedule.tile.referenced_type == "survey"
        tile = Tile.for_task(Task.get(tile_schedule.tile.referenced)) if tile_schedule.tile.referenced_type == "task"
        tile ||= tile_schedule.tile

        # If this tile schedule has been updated with a reminded_at date, then the tile
        # should not present a reminder button.  Remove it if there.
        if tile_schedule.reminded_at
          # We have to pass information about the TS down to the button json renderer.  We're
          # going to do that by passing in a list of tile IDs that this option should be set for.
          skip_reminders.push(tile.id)

          skip_reminder = true
        else
          skip_reminder = false
        end
        tile = tile.as_json(user: self, tile_schedule: tile_schedule, skip_reminder: skip_reminder)
        tile[:tile_schedule_id] = tile_schedule.id
      end
      num_tasks += 1 if tile and tile_schedule.tile.tile_type != "info"
      section.tiles.push(tile) if tile
    end
    sections.push(section) if section.tiles.length > 0

    res[:feed_sections] = sections.as_json(legacy: true, skip_reminders: skip_reminders)

    # If there's nothing to do in the feed, maybe say when the next thing will be.

    if num_tasks == 0
      num_sections = res[:feed_sections].length

      next_ts = self.next_scheduled_action

      if next_ts
        distance = distance_of_time_in_words(next_ts.active_at - Time.zone.now)
        name = next_ts.tile.name.titleize rescue "doozy"
        name = "Quick Activity" if name == "Ema"
        ttype = "cognitive task" if next_ts.tile.referenced_type == "cognitive-task" rescue nil
        ttype = "active task" if next_ts.tile.referenced_type == "active-task" rescue nil
        ttype = "survey" if next_ts.tile.referenced_type == "survey" rescue nil
        ttype ||= Task.get(next_ts.tile.name).task_detail.task_type rescue ""
        tile = Tile.copy("upcoming-task-survey-info")

        # Note that the copy hasn't saved the property yet, so tile.properties.where(...) fails.
        heading = tile.properties.first
        heading.value = heading.value.gsub("DISTANCE", distance) if heading
        heading.value = heading.value.gsub("NAME", TileSchedule.canonicalize_task_name(name, remove_copy_task: true)) if heading
        ttype = " " + ttype if not ttype.blank?
        heading.value = heading.value.gsub("TYPE", ttype) if heading

        first_section = sections[0] || section
        first_section.tiles.push(tile) if tile
        # first_section.tiles.each_with_index { |tile, position| tile.position = position }
        first_section.tiles.each_with_index { |t, position| t[:position] = position }
        sections.push(section) if num_sections == 0
        res[:feed_sections] = sections.as_json(legacy: true)
      end
    end

    res
  end

  # Register the user as a CamCog subject, if not already registered
  def register_camcog
    return if roster.camcog_subject_id
    return if Rails.env.test?

    post_body = {
      "subjectIds": [roster.id],
      "subjectItems": [
        {
          "subjectItemDef": ENV['CAMCOG_SUBJECT_ITEM_DEF'],
          "text": nil,
          "multiText": nil,
          "date": nil,
          "integer": nil,
          "locale": "en-US",
          "hidesPII": false
        }
      ],
      "groupDef": ENV["CAMCOG_GROUPDEF"],
      "site": ENV["CAMCOG_SITE"],
      "studyDef": ENV["CAMCOG_STUDYDEF"],
      "status": "NEW"
    }

    register_response = HTTP.basic_auth({ user: ENV['CAMCOG_USERNAME'], pass: ENV['CAMCOG_PASSWORD'] })
                            .post("#{ENV['CAMCOG_URL']}/api/subject", json: post_body)

    unless register_response.status.success?
      Rails.logger.error("could not register user #{id} with CamCog; status: #{register_response.status}")
      Rails.logger.error("#{register_response.body}") if register_response.body
      return
    end

    register_response_json = JSON.parse(register_response.body)
    if register_response_json['total'] != 1
      Rails.logger.error("Wrong number of CamCog subjects created; was #{register_response_json['total']} and should be 1")
      Rails.logger.error("Response body: #{register_response_json}")
      return
    end

    camcog_subject_id = register_response_json['records'].first['id']

    access_code_response = HTTP.basic_auth({ user: ENV['CAMCOG_USERNAME'], pass: ENV['CAMCOG_PASSWORD'] })
                               .get("#{ENV['CAMCOG_URL']}/api/subjectLoginInfo?limit=1&filter={subject:\"#{camcog_subject_id}\"}")
    unless access_code_response.status.success?
      Rails.logger.error("could not get CamCog access code for user #{id} using subject ID #{camcog_subject_id}; status: #{access_code_response.status}")
      Rails.logger.error("#{access_code_response.body}") if access_code_response.body
      return
    end

    access_code_response_json = JSON.parse(access_code_response.body)

    camcog_access_code = access_code_response_json["records"].first["accessCode"]

    roster.camcog_subject_id = camcog_subject_id
    roster.camcog_access_code = camcog_access_code

    roster.save
  end

  def add_tile_at(tile_or_name, beg = Time.zone.now, duration = 2.hours)
    tile = Tile.copy(tile_or_name) if tile_or_name.is_a?(String)
    tile ||= tile_or_name
    tile = Tile.get(tile.name.gsub("-copy", "")) if tile.name.include?("-copy") and not tile.tile_type == "info"
    t = tile_schedules.create(tile: tile, active_at: beg, expires_at: beg + duration)
    raise("Bad tile name: '#{tile_or_name}'") if not t or not t.id
    t.save
    t
  end

  # Add the specified tile to the feed, but iff it won't already appear within the same study day.
  def add_tile_once(name, opts = { beg: nil, duration: nil })
    beg = opts[:beg] || (Time.zone.now.in_time_zone(self.timezone).beginning_of_day).utc
    duration = opts[:duration] || 12.hours
    fin = beg + (24.hours - 1.second)

    ts = self.tile_schedules.includes(:tile).where(active_at: beg..fin).where(tiles: { name: name }).first
    ts ||= self.tile_schedules.includes(:tile).where(active_at: beg..fin).where(tiles: { name: name + "-copy" }).first
    ts ||= add_tile_at(name, beg, duration)
    ts
  end

  def schedules_for_study_day(day)
    user.tile_schedules.where(study_day: day)
  end

  def tiles_for_study_day(day)
    Tile.where(id: schedules_for_study_day.pluck(:tile_id))
  end

  def upcoming_tiles(duration)
    present = Time.zone.now.in_time_zone(self.timezone)
    future = earlier + duration
    active_at = TileSchedule.arel_table[:active_at]
    expires_at = TileSchedule.arel_table[:expires_at]
    Tile.where(id: tile_schedules.where(active_at.gt(present)).where(active_at.lt(future)).pluck(:tile_id))
  end

  def reset(options = {})
    self.completed_tasks.destroy_all
    self.measurements.destroy_all
    self.point_rewards.destroy_all
    self.answers.destroy_all
    self.access_tokens.destroy_all
    self.validation_code.destroy if not self.validation_code.blank?

    self.start_study() if options[:restart_study]
    self.tile_schedules.destroy_all
    self.make_tile_schedule if not options[:make_schedule] == false
  end

  def destroy
    r = self.roster rescue nil
    self.reset(make_schedule: false)
    self.devices.destroy_all
    self.notifications.destroy_all
    r.destroy rescue nil
    super
  end

  def demo_feed
    res = { feed_metadata: self.feed_metadata, quick_activity: self.quick_activity }
    sections = []
    self.feed_sections.each do |section|
      tiles = section.tiles_for_study_day(self.study_day)
      if tiles.length > 0
        obj = section.as_json(legacy: true)
        obj[:tiles] = tiles.as_json(legacy: true)
        sections.push(obj)
      end
    end

    res[:feed_sections] = sections

    res
  end

  def dump_measurements(options = {})
    beg = options[:beg]; beg ||= Time.zone.now.in_time_zone(timezone) - 2.days
    fin = options[:fin]; fin ||= Time.zone.now.in_time_zone(timezone)
    beg = Time.zone.parse(beg).in_time_zone(timezone) if beg.is_a?(String)
    fin = Time.zone.parse(fin).in_time_zone(timezone) if fin.is_a?(String)
    limit = options[:limit]; limit ||= 20
    limit = limit.to_i
    base = self.measurements.order(gathered_at: :desc).where(gathered_at: (beg.utc)..(fin.utc))
    base = base.where(datasource: options[:datasource]) if options[:datasource]
    base = base.where(datatype: options[:datatype]) if options[:datatype]
    base = base.limit(limit) if limit
    list = base.collect { |m| m.pretty_print }
  end

  def as_json(options = {})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:person] = self.person.as_json
    res[:devices] = self.devices.as_json
    res
  end

  def maybe_guess_timezone
    if not self.timezone
      self.timezone = AreaCode.where(code: self.phone&.number&.to_i).first&.timezone
      # If in USA, default to a USA timezone
      self.timezone ||= self.phone&.number&.start_with?("+1 ") && "America/New_York"
      self.timezone ||= "Europe/London"
    end
  end

  def adjust_for_changed_zone
    # If the timezone has just changed, add the time difference to everything that needs to change.
    if self.timezone_changed? and self.timezone_was and self.study_started_at
      was = self.study_started_at.in_time_zone(self.timezone_was).time_zone.utc_offset
      now = self.study_started_at.in_time_zone(self.timezone).time_zone.utc_offset
      diff = was - now
      if diff != 0
        self.study_started_at += diff
        self.tile_schedules.each do |ts|
          ts.active_at += diff
          ts.expires_at += diff
          ts.reminded_at += diff if ts.reminded_at
          ts.save
        end
      end
    end
  end

  def make_base_measurements
    %w(empatica bodyport oura pooply rescuetime).each do |datasource|
      self.measurements.where(datasource: datasource, datatype: "basebase", value: 0, gathered_at: self.study_started_at || Time.zone.now).first_or_create
    end
  end

  def show_phone_number
    Phone.obfuscate(self.person.phone.number) rescue "???"
  end

  # Return a range of time, from (now - duration) through now.
  def within_the_past(duration)
    (Time.zone.now - duration)..Time.zone.now
  end

  # Return a range of time, from now through (now + duration).
  def within_the_next(duration)
    Time.zone.now..(Time.zone.now + duration)
  end

  # Send a notification to the user.  Be careful not to send too many to the user at once, and
  # not to send duplicates of the notification.
  # +:force+ boolean; If truthy causes the notification to get sent no matter what
  def send_notification(options = {})
    n = nil
    similar = self.notifications.where(title: options[:title], message: options[:message])

    # Find out if the user has "recently" received a notification that looks like this one.
    recent = similar.where(delivered_at: within_the_past(12.hours)).first

    # Find out if the user is already scheduled to a notification similar to this one.
    soon = similar.where(deliver_at: within_the_next(12.hours)).first

    # Make a notification if there aren't any recently delivered or to be delivered soon.
    if options[:force] or (not recent and not soon)
      n = self.notifications.new(title: options[:title], message: options[:message], deliver_at: options[:deliver_at])
      n.payload = { type: "task", task_id: options[:task].id } if options[:task]
      n.save
    end
    n or recent or soon or options[:force] #???
  end

  def change_study_start(new_time)
    diff = study_started_at - new_time
    study_started_at = new_time
    save
    tile_schedules.each do |ts|
      ts.active_at -= diff
      ts.expires_at -= diff
      ts.completed_at -= diff if ts.completed_at
      ts.reminded_at -= diff if ts.reminded_at
      ts.save
    end
  end

  def update_organization
    s = self.site || "Default"
    org = Organization.lookup(s)
    org.users << self if not org.users.include?(self)
  end

  def study_offsets(time = Time.zone.now)
    day = ((time.in_time_zone(timezone).end_of_day + 1.second).to_date - beginning_of_study_month.to_date).to_i
    month = study_month(time)
    return day, month
  end

  def self.once_per_study_day
    self.where.not(study_started_at: nil)
        .where(study_ended_at: nil).or(User.where("study_ended_at >= ?", DateTime.now))
        .each do |user|
      begin
        user.once_per_study_day
      rescue Exception => ex
        Rails.logger.error("Failed to run once_per_study_day for user #{user.id}; #{ex.full_message}")
      end
    end
  end

  # +options[:study_day]+: The study day to run for. Defaults to the user's current study day
  def once_per_study_day(options = {})
    the_study_day = options[:study_day] || study_day
    month, day_in_month = study_monthday(date_of_study_day(the_study_day))

    missing_data_duration = 24.hours

    today = self.study_started_at.in_time_zone(self.timezone) + (month - 1).months + (day_in_month - 1).days
    yesterday = today - 1.day

    # Check motivation.
    if day_in_month == 20
      ts = add_tile_once("motivation-earned")
      contents = ts.tile.properties.first
      contents.value = contents.value.gsub("XXXX", lifetime_points.to_s)
      ts.save
    end

    add_tile_once("motivation-activity") if (month == 2 and day_in_month == 5)

    if day_in_month == 25
      points = self.study_points

      if points < 2000
        if points > 1500
          needed = 2000 - points
          us_money = "$40"
          uk_money = "£30"
        elsif points > 1000
          needed = 1500 - points
          us_money = "$30"
          uk_money = "£25"
        elsif points > 500
          needed = 100 - points
          us_money = "$20"
          uk_money = "£15"
        else
          needed = 500 - points
          us_money = "$15"
          uk_money = "£12"
        end

        if self.timezone.starts_with?("America")
          money = us_money
        elsif self.timezone.starts_with?("Europe/London")
          money = uk_money
        else
          money = "#{us_money} (#{uk_money})"
        end
        ts = add_tile_once("points-remaining")
        content = ts.tile.properties.first
        content.value = content.value.gsub("POINTS-NEEDED", number_with_delimiter(needed))
        content.value = content.value.gsub("MONEY", money)
        content.save
      end
    end

    if day_in_month == 15
      if lifetime_points < 18000
        ts = add_tile_once("xxx-to-go")
        if lifetime_points >= 12000
          threshold = 18000
          contributor = "Groundbreaking"
        elsif lifetime_points >= 7000
          threshold = 12000
          contributor = "Super"
        elsif lifetime_points >= 3000
          threshold = 7000
          contributor = "Major"
        else
          threshold = 3000
          contributor = "Growing"
        end
        needed = threshold - lifetime_points
        content = ts.tile.properties.first
        content.value = content.value.gsub("POINTS-NEEDED", number_with_delimiter(needed))
        content.value = content.value.gsub("POINTS-THRESHOLD", number_with_delimiter(threshold))
        content.value = content.value.gsub("POINTS-CONTRIBUTOR", contributor)
        content.save
      else
        add_tile_once("keep-up-groundbreaking")
      end
    end

    add_tile_once("thanks-for-contribution") if month > 2 and day_in_month == 5
    add_tile_once("would-not-be-here") if month == 2 and day_in_month == 15

    yesterdays_measurements = Measurement.for_user_study_days(self, the_study_day - 1, the_study_day - 1)

    bodyport_measurement = yesterdays_measurements.where(datasource: "bodyport", datatype: "weight").first
    empatica_measurement = yesterdays_measurements.where(datasource: "empatica", datatype: ["Worn correctly", "data_uploaded"]).first


    empatica_needs_help = yesterdays_measurements.where(datasource: "empatica", datatype: "Worn too loose").first
    rescuetime_measurement = yesterdays_measurements.where(datasource: "rescuetime", datatype: "mobile").first
    pooply_measurement = yesterdays_measurements.where(datasource: "pooply", datatype: "pooply").first
    # Only check for steps from Oura; we assume if we have steps, we have the other Oura measurements as well
    oura_measurement = yesterdays_measurements.where(datasource: "oura").where(datatype: "steps").first

    missing_measurements = []

    missing_measurements.push("Bodyport scale") if not bodyport_measurement
    missing_measurements.push("Empatica Embrace") unless empatica_measurement
    missing_measurements.push("Oura ring") if not oura_measurement

    time_to_deliver = "8:45am".in_time_zone(self.timezone).utc
    notification_title = "Reminder to wear and sync devices"
    notification_body = "We didn't get any data from your devices yesterday"

    # Handle the notifications differently than the tiles.
    notification_body.sub!("devices", "#{missing_measurements[0]} or #{missing_measurements[1]}") if missing_measurements.length == 2
    notification_body.sub!("devices", "#{missing_measurements[0]}") if missing_measurements.length == 1
    self.send_notification(title: notification_title, message: notification_body, deliver_at: time_to_deliver) if missing_measurements.length > 0

    add_tile_once("bodyport-missing-data", duration: missing_data_duration) unless bodyport_measurement
    add_tile_once("oura-missing-data", duration: missing_data_duration) unless oura_measurement
    add_tile_once("empatica-missing-data", duration: missing_data_duration) unless empatica_measurement
    add_tile_once("pooply-missing-data", duration: missing_data_duration) unless pooply_measurement
    add_tile_once("rescuetime-missing-data", duration: missing_data_duration) unless rescuetime_measurement

    # If the item was worn loosely, tell the user about that.
    if empatica_needs_help
      self.send_notification(
        title: "Your Empatica Embrace was worn loosely at times yesterday, #{yesterday.strftime('%A (%D)')}",
        message: "That makes it hard to get good measurements from it. " + "Please reread the instructions for how to get the best fit.",
        deliver_at: time_to_deliver)
    end

    # Calculate device rewards for yesterday
    award_device_compliance_points_for_study_day(the_study_day - 1)

    # Check reminders.
    add_tile_once("empatica-reminder") if month == 1 and day_in_month == 6
    add_tile_once("oura-reminder") if month == 1 and day_in_month == 10
    add_tile_once("bodyport-reminder") if month == 1 and day_in_month == 13

    # Check App function
    add_tile_once("find-study-activities") if month == 1 and day_in_month == 2
    add_tile_once("checkout-study-info") if month == 1 and day_in_month == 4

    # Oh, compute the roster compliance, too.
    compute_roster_compliance
  end

  def send_tile(name)
    ts = add_tile_at(name)
    ts.send_notification(force: true)
  end

  def send_tiles(names)
    return send_tile(names) if names.is_a?(String)
    names.each { |name| send_tile(name) }
  end

  def find_feed_tile(name)
    now = Time.zone.now
    self.tile_schedules.find_named(name).where(completed_at: nil).where(":now BETWEEN ACTIVE_AT AND EXPIRES_AT", now: now).first
  end

  def find_named(name, options = nil)
    options ||= { skip_completed: true, first: true }
    basis = self.tile_schedules.order(study_day: :asc, active_at: :asc).find_named(name)
    basis = basis.incomplete if options[:skip_completed]
    basis = basis.first if options[:first]
    basis
  end

  def on_deck(named = nil)
    base = self.tile_schedules.currently_active.noninfo.incomplete
    base = base.find_named(named) if named
    base
  end

  def opt_in_questions
    titles = ["Relative location", "Sharing options", "Future research contact"]
    Question.where(title: titles)
  end

  def opt_in_answers
    res = []
    questions = opt_in_questions
    questions.each do |q|
      answer = self.answers.where(question: q).first
      next if not answer
      akey = answer.answer
      atxt = q.options.where(value: akey).first.contents.first.content
      res.push({ question_text: q.title, answer_text: atxt, answered_at: answer.created_at })
    end
    res
  end

  private

  # Returns the times to send notifications if the user hasn't completed onboarding
  #
  # Requirements:
  # Reminders should go out at 3pm, 8am, and 8 pm in users’ local time (or close). Users should not receive more than
  # one push notification in 12 hours. Users should not receive more than 3 reminder push notifications total.
  def get_signup_notification_send_times

    # Also, don't schedule a notification to occur within time_buffer_hours hours of the current time
    time_buffer_hours = 1
    now_in_tz = Time.zone.now.in_time_zone(self.timezone)
    hour_in_tz = now_in_tz.hour
    send_times = []

    # This is not very clever, but it works.
    if hour_in_tz < 8 - time_buffer_hours
      send_times << now_in_tz.change({ hour: 8 })
      send_times << now_in_tz.change({ hour: 20 })
      send_times << now_in_tz.tomorrow.change({ hour: 8 })
    elsif hour_in_tz < 15 - time_buffer_hours
      send_times << now_in_tz.change({ hour: 15 })
      send_times << now_in_tz.tomorrow.change({ hour: 8 })
      send_times << now_in_tz.tomorrow.change({ hour: 20 })
    else
      send_times << now_in_tz.tomorrow.change({ hour: 8 })
      send_times << now_in_tz.tomorrow.change({ hour: 20 })
      send_times << now_in_tz.tomorrow.tomorrow.change({ hour: 8 })
    end

    send_times
  end

  # Schedule SMS notifications to be sent if the user does not finish signup
  # Note that these SMS notifications are _in addition_ to the push notifications
  # that get scheduled after the user completes the consent form
  def schedule_signup_sms_notifications
    schedule_signup_notifications(
      "",
      "Thanks for your interest in the Stress in Crohn's study. You have not fully enrolled - please return to the Stress in Crohn's app to finish signing up.",
      NotificationType::SMS
    )
  end

  # Schedule notifications that are sent to users who don't finish signing up
  def schedule_signup_notifications(title, message, notification_type)
    get_signup_notification_send_times.each do |t|
      Notification.create(user: self,
                          notification_type: notification_type,
                          deliver_at: t,
                          title: title,
                          message: message,
                          send_condition: Notification.instance_method(:validated_user_not_on_dashboard).name)
    end
  end
end
