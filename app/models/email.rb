# email.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Sep 10 10:41:12 2019.
require_dependency Openopus::Core::People::Engine.root.join('app', 'models', 'email').to_s

class Email < ApplicationRecord
  
  def self.from_hash(hash)
    cols = self.column_names
    email = self.where(id: hash[:id]).first
    email ||= self.new(hash.slice(*cols))
    email
  end
  
  def make_confirmation_code
    100.times do
      candidate = (rand(9) + 1).to_s
      candidate += 4.times.map{rand(10)}.join
      if not self.class.where(confirmation_code: candidate).first
        self.confirmation_code = candidate
        break
      end
    end
    self.confirmation_code
  end

  def send_confirmation_code(request=nil)
    self.make_confirmation_code and self.save if not self.confirmation_code
    UserMailer.please_confirm(user: self.emailable, base_url: (request ? request.base_url : "https://api.4youandme.net"), email_object: self).deliver_later
  end
  
  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:label] = self.label
    res.delete(:label_id)
    res.delete(:emailable_type)
    res.delete(:emailable_id)
    res
  end
end
