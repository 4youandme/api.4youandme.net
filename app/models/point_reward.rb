# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class PointReward < ApplicationRecord
  belongs_to :user
  has_one :roster, through: :user
  has_one :person, through: :user
  has_many :phones, through: :person

  before_save :maybe_updated_awarded_at
  after_save  :maybe_make_compliance_tile

  def self.roster(value)
    self.joins(:roster).where(rosters: { id: value })
  end

  def self.roster_ends_with(value)
    res = nil
    if value.is_a? Roster
      res = self.joins(user: :person).where(person: { rosters: value })
    elsif value.is_a? String
      res = self.joins(user: :person).joins(person: :phones).where("phones.number like :val", val: '%' + value)
    end

    res
  end

  # Finds or creates a PointReward with the given reward_date, trigger, and user. If a PointReward exists and has fewer points than +points+, updates the PointReward with the new points
  # Returns true if a new PointReward was created, and false if a PointReward already existed and was updated
  def self.create_or_increase_points(study_day, trigger, points, user)
    date_of_study_day = user.date_of_study_day(study_day)

    matching_point_reward = self.where(awarded_at: date_of_study_day.beginning_of_day..date_of_study_day.end_of_day,
               trigger: trigger,
               user: user).first rescue nil
    if matching_point_reward
      matching_point_reward.update_attributes(points: points)
      return false
    else
      PointReward.create(awarded_at: date_of_study_day, trigger: trigger, points: points, user: user)
      return true
    end
  end

  def self.device_compliance_trigger_name(datasource)
    "device compliance: #{datasource}"
  end

  def self.ransackable_scopes(_auth_object=nil)
    [:roster]
  end
  
  def phone_number
    self.phones.first.number rescue nil
  end
    
  def maybe_updated_awarded_at
    self.awarded_at ||= Time.zone.now
  end

  def maybe_make_compliance_tile
    points_after = user.lifetime_points
    points_before = points_after - points

    if points_before < 3000 and points_after >= 3000
      make_tile(heading: "Way to go!", body: "You've earned 3,000 points and are now a Growing Contributor!")
    elsif points_before < 7000 and points_after >= 7000
      make_tile(heading: "This is major!", body: "You've earned 7,000 points and are now a Major Contributor!")
    elsif points_before < 12000 and points_after >= 12000
      make_tile(heading: "Keep it up!", body: "You've earned 12,000 points and are now a Super Contributor!")
    elsif points_before < 18000 and points_after >= 18000
      make_tile(heading: "You are AWESOME!", body: "You've earned 18,000 points and are now a Groundbreaking Contributor!")
    end
  end

  def make_tile(options={})
    tile = Tile.from_hash(name: "reward-info", type: "info", icon: "reward", style_class: "light-style",
                          contents: { heading: options[:heading], body: options[:body], alignment: "center" })
    user.add_tile_at(tile)
  end
end
