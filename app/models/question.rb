# question.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Jan  3 17:53:32 2020.
class Question < ApplicationRecord
  belongs_to :questionable, polymorphic: true
  belongs_to :consent_section_info, required: false
  has_many :source_targets, as: :target_source, class_name: "Target", dependent: :destroy
  has_many :destination_targets, as: :target_dest, class_name: "Target", dependent: :destroy
  
  has_one :consent_section, through: :consent_section_info
  has_many :options, class_name: "QuestionOption", dependent: :destroy
  has_many :contents, as: :contentable, dependent: :destroy
  has_many :properties, as: :owner, dependent: :destroy
  default_scope { order(position: :asc, created_at: :asc) }
  
  COLS = [:value, :position, :language, :question_type, :correct_response, :title]

  def self.from_hash(hash, pos=1)
    question = self.new(hash.slice(*COLS))
    question.properties << Property.new(name: "destination_id", value: hash[:id]) if hash[:id]

    question.position ||= pos
    question.language ||= "en"
    question.title ||= hash["question-title"]
    
    question.contents << Content.from_hash(hash)

    hash[:options].each_with_index {|d, i| question.options << QuestionOption.from_hash(d, i)} if hash[:options]
    hash[:targets].each_with_index {|t, i| question.source_targets << Target.from_hash(t, i)} if hash.has_key?(:targets)

    # BEG: How to make TECH-DEBT for fun and profit
    question.properties << Property.new(name: "details", value: hash[:details].to_json) if hash[:details]
    # FIN: How to make TECH-DEBT for fun and profit

    question
  end

  def text
    self.contents.first.content rescue nil
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:options] = self.options.as_json(options) if self.options.length > 0
    res["question-title"] = res[:title]
    content = self.contents.first
    if content
      res[:content] = content.content
      res[:content_type] = content.content_type
      res[:style_class] = content.style_class
    end

    # BEG: How to make TECH-DEBT for fun and profit
    debt_details = self.properties.where(name: "details").first.value rescue nil
    res[:details] = JSON.parse(debt_details) if debt_details
    # FIN: How to make TECH-DEBT for fun and profit

    if self.source_targets.length > 0
      res[:targets] = []
      self.source_targets.each {|t| res[:targets].push(t.as_json(options))}
    end

    if options[:legacy]
      res.delete(:created_at)
      res.delete(:updated_at)
      res.delete(:questionable_id)
      res.delete(:questionable_type)
    end

    res
  end

end
