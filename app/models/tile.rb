# tile.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Mon Dec 16 11:27:48 2019.
class Tile < ApplicationRecord
  # TODO: Remove references to Tileables; they no longer exist
  belongs_to :tileable, polymorphic: true, required: false
  has_many :properties, as: :owner
  has_many :contents, as: :contentable
  has_many :buttons, as: :buttonable
  has_many :tile_schedules

  def self.get(name)
    self.order(updated_at: :desc).where(name: name).first
  end

  # Create a copy of the tile with the name +name+, and give it the name "<+name+>-copy"
  def self.copy(name)
    template = self.get(name)
    hash = JSON.parse(template.to_json(legacy:true)).merge(copied: true)
    hash["name"] = (hash["name"] || "") + "-copy"
    tile = self.from_hash(hash)
    tile
  end
  
  def self.import_and_replace(file, hash=nil, options={})
    if not File.exists?(file)
      if File.exists?("./db/seed-data/static/tiles/#{file}")
        file = "./db/seed-data/static/tiles/#{file}"
      elsif File.exists?("./db/seed-data/static/tiles/#{file}.json")
        file = "./db/seed-data/static/tiles/#{file}.json"
      end
    end
    return self.import_and_replace(file, JSON.parse(File.read(file)), options) if not hash
    return hash.each {|h| self.import_and_replace(file, h, options)} if hash.is_a?(Array)

    hash = ActiveSupport::HashWithIndifferentAccess.new(hash)
    hash[:name] ||= File.basename(file).gsub(/.json/, "")

    if options[:rename]
      self.where(name: hash[:name]).each {|tile| tile.rename_to("#{tile.name}-#{tile.created_at}") }
    else
      self.where(name: hash[:name]).destroy_all if hash[:name]
    end
    tile = self.from_hash(hash)

    print "(#{tile.name})" if tile and tile.name
    tile.save
    tile
  end

  def self.from_hash(hash, pos=0)
    hash = ActiveSupport::HashWithIndifferentAccess.new(hash) if hash.class != ActiveSupport::HashWithIndifferentAccess
    cols = self.column_names
    cols.delete("id")
    tile = self.new(hash.slice(*cols))
    tile.position ||= pos + 1
    tile.tile_type ||= hash[:type]
    tile.first_study_day ||= 1

    tile.contents << Content.from_hash(hash) if hash[:content]

    if hash.has_key?(:contents)
      contents_object = hash[:contents]

      if contents_object.is_a?(Array)
        hash[:contents].each_with_index do |chash|
          tile.contents << Content.from_hash(chash, (tile.contents.length + 1))
        end
      elsif contents_object.is_a?(Hash) and contents_object.has_key?(:heading)
        tile.contents << Content.new(content: contents_object[:heading], tag: "heading",
                                     content_type: contents_object[:content_type] || "text/html",
                                     style_class: "heading " + (contents_object[:alignment] || ""))
        # BEG: How to make TECH-DEBT for fun and profit
        tile.properties << Property.new(name: "contents", value: contents_object.to_json)
        # FIN: How to make TECH-DEBT for fun and profit
      end
    end

    tile.buttons << Button.from_hash(hash[:button]) if hash.has_key?(:button)
    tile.update_referenced_type(no_save: hash.has_key?(:copied))
    
    tile
  end
  
  def update_referenced_type(options={})
    if not self.referenced_type or options[:force]
      # Is this tile a survey?
      self.referenced_type = "survey" if self.tile_type == "survey-reference"
      self.referenced_type = "task" if self.tile_type == "task"
      if self.tile_type == "task-reference"
        detail = Task.get(self.referenced).details.where.not(task_type: ["intro", "info", "outro"]).first rescue nil
        self.referenced_type = detail.task_type if detail
      end

      self.save unless options[:no_save]
    end
  end

  # Returns true if the tile is a video diary tile
  def is_video_diary?
    !!name&.starts_with?('video-diary')
  end

  # Returns true if the tile is a walk test tile
  def is_walk_test?
    !!name&.starts_with?('walk-test')
  end

  # If a task is associated with the tile, returns the task. Otherwise returns nil.
  def task
    self&.buttons&.first&.tasks&.first
  end

  # Create a tile for point awards, and schedule it to be shown on +study_day+
  def self.for_point_award(user, body, heading=nil, points, study_day)
    if heading.nil?
      congratulatory_words = %w(Congratulations Awesome Fantastic Spectacular Great)
      heading = congratulatory_words[rand(congratulatory_words.length)]
      heading += "!\n#{points.to_s} Points"
    end
    body = body.gsub(/POINTS/, points.to_s)

    tile = Tile.create(tile_type: "info", icon: "reward", name: "reward-info", title: heading, style_class: "light-style", position: 1)
    contents = { heading: heading, body: body, alignment: "center" }
    tile.properties.create(name: "contents", value: contents.to_json)
    # Make the tile active at the beginning of the next day
    active_at = user.date_of_study_day(study_day)
    user.tile_schedules.create(tile: tile, active_at: active_at, expires_at: active_at + 3.months, study_day: study_day)
  end

  # Create a tile which contains SURVEY.  This tile doesn't even need to be in the database.
  # It is a construct for sending to the client.
  def self.for_survey(survey)
    title = survey.title
    title ||= survey.name.gsub("-", " ").titleize rescue "Survey"
    title = TileSchedule.canonicalize_task_name(title) if title

    core = {
      type: "task", icon: "timer", button: { label: "Start Now" },
      contents: { heading: "#{title}\nPlease complete the survey", alignment: "center" }
    }
    tile = self.from_hash(core).as_json(legacy: true)
    click_action = survey.task.as_json(legacy: true)
    click_action[:task_name] = click_action[:name]
    click_action[:name] = "survey"
    tile[:button][:click_action] = click_action

    tile
  end

  # Create a tile which contains TASK.  This tile doesn't even need to be in the database.
  # It is a construct for sending to the client.
  def self.for_task(task)
    title = task.name.gsub("-", " ").capitalize rescue "Survey"
    core = {
      type: "task", icon: "timer", button: { label: "Start Now" },
      contents: { heading: "#{title}\nPlease complete the survey", alignment: "center" }
    }
    tile = self.from_hash(core).as_json(legacy: true)
    click_action = task.as_json(legacy: true)
    click_action[:task_name] = click_action[:name]
    click_action[:name] = "survey"
    tile[:button][:click_action] = click_action
    tile
  end

  def self.for_study_day()
    tiles = []
    tasks = []
    tiles.push(Tile.get("trail-making"))

    tasks.each_with_index do |task, pos|
      tile = { name: task.name, tile_type: "task", button: { label: "Start Now" } }
      tile[:button][:click_action] = task.as_json(legacy: true)
      tiles.push(tile)
    end

    tiles
  end

  # Returns the EMA (a.k.a. "quick activity") tile
  def self.ema_tile
    Tile.get("ema")
  end

  def rename_to(new_name)
    self.name = new_name
    self.save
  end

  # Returns the Tile name, with suffixes like "-copy" or "-2020-11-23 01:46:04 UTC" removed
  def base_name
    self.name.gsub('-copy', '').sub(/-\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} [A-Z]*$/, "") rescue nil
  end

  # +options+ can include +user+ and +tile_schedule+; these will be used to generate user-specific button actions
  def as_json(options={})
    options[:skip_reminder] = true if options[:skip_reminders] && options[:skip_reminders].include?(self.id)
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:type] = res.delete(:tile_type)

    content = self.contents.select {|c| c.tag == nil}.first
    if content
      res[:content] = content.content
      res[:content_type] = content.content_type || "text/plain"
      res[:style_class] = content.style_class
    end

    # BEG: How to make TECH-DEBT for fun and profit
    debt_contents = self.properties.select {|p| p.name == "contents"}.first.value rescue nil
    res[:contents] = JSON.parse(debt_contents) if debt_contents
    # FIN: How to make TECH-DEBT for fun and profit

    self.contents.select {|c| c.tag != nil }.each do |c|
      res[c.tag] = c.as_json(options)
      res[c.tag].delete("tag")
    end

    res = res.merge(Button.json_helper(self, options))

    res.delete(:title) if not res[:title]
    res.delete(:icon) if not res[:icon]
    res.delete(:type) if not res[:type]

    if options[:legacy]
      res.delete(:tileable_type)
      res.delete(:tileable_id)
      res.delete(:created_at)
      res.delete(:updated_at)
    end

    if options[:tile_schedule]
      res[:tile_schedule_id] = options[:tile_schedule].id
    end

    res
  end
end
