# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class PhoneEvent < ApplicationRecord
  belongs_to :device

  def event_ts=(value)
    self.event_at = Time.strptime(value.to_s, "%s")
  end

  def battery=(value)
    self.battery_level = value
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:hashed_ssid] = "notstoredonpurpose"
    res
  end
end
