class TaskDetail < ApplicationRecord
  belongs_to :task
  has_many :buttons, as: :buttonable
  has_many :contents, as: :contentable
  has_many :surveys, as: :surveyable
  has_many :properties, as: :owner

  def self.from_hash(hash, pos = 0)
    cols = self.column_names
    cols.delete("id")
    detail = self.new(hash.slice(*cols))
    detail[:position] ||= pos + 1
    detail[:task_type] ||= hash[:type]
    detail.contents << Content.from_hash(hash) if hash[:content]

    if hash.has_key?(:contents)
      # BEG: How to make TECH-DEBT for fun and profit
      #
      # Support this monstrosity:
      #
      #     "contents": {
      #        "heading": "Click to open the Oura app",
      #        "body": "The button should go to Oura in the app store if Oura is not installed",
      #        "alignment": "center"
      #      },
      #
      if hash[:contents].is_a?(Hash) and hash[:contents].has_key?(:heading)
        detail.properties << Property.new(name: "contents", value: hash[:contents].to_json)
      else
        #
        # FIN: How to make TECH-DEBT for fun and profit
        hash[:contents].each_with_index do |chash|
          detail.contents << Content.from_hash(chash, (detail.contents.length + 1))
        end
      end
    end

    # This is a *very* flexible API.  We accept a single :button, an array of :buttons, and
    # a special "reminder_button".
    detail.buttons << Button.from_hash(hash[:button]) if hash.has_key?(:button)
    hash[:buttons].each { |b| detail.buttons << Button.from_hash(b, 0) } if hash[:buttons].is_a?(Array)
    detail.buttons << Button.from_hash(hash[:reminder_button].merge(tag: "reminder")) if hash.has_key?(:reminder_button)

    # Handle tasks that have surveys.  Either spelled out, or referenced.
    survey = Survey.from_hash(hash, pos) if hash[:survey_ref] or (hash[:name] == "survey" and hash[:questions] != nil)
    if survey
      detail.surveys << survey
      detail.format = survey.format
      detail.name = survey.name
      detail.task_type = "survey"
    end

    # Handle tasks that have options.
    detail.properties << Property.new(name: "options", value: hash[:options].to_json) if hash.has_key?(:options)

    detail
  end

  def as_json(options = {})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:type] ||= res.delete(:task_type)

    if (opts = self.properties.where(name: "options").first)
      res[:options] = JSON.parse(opts.value)

      # For web tasks, we generate user-specific URLs using ERB templates stored in the DB
      if self.name == "web-task" && options[:user] && options[:tile_schedule]
        # +user+ and +tile_schedule+ may be used in the destination template
        user = options[:user]
        tile_schedule = options[:tile_schedule]
        res[:options].each do |k, v|
          next unless k.end_with?("_url")
          puts "VALUE:" + v
          erb = ERB.new(v)
          res[:options][k] = erb.result(binding)
        end
      end
    end

    # BEG: How to make TECH-DEBT for fun and profit
    debt_contents = self.properties.where(name: "contents").first.value rescue nil
    res[:contents] = JSON.parse(debt_contents) if debt_contents
    # FIN: How to make TECH-DEBT for fun and profit

    if self.contents.length > 0
      c = self.contents.first
      res[:content] = c.content
      res[:content_type] = c.content_type if not c.content_type.blank?
      res[:style_class] = c.style_class if not c.style_class.blank?

      res[:contents] ||= self.contents.as_json(options) if self.contents.length > 1
    end

    res = res.merge(Button.json_helper(self, options))

    # There's only ever one survey per task detail.
    self.surveys.each do |survey|
      res[:questions] = survey.questions.order(position: :asc, created_at: :asc).as_json(options)
      res[:source] = res[:name]
      res[:name] = "survey"
    end

    if options[:legacy]
      res.delete(:created_at)
      res.delete(:updated_at)
      res.delete(:format) if self.format.nil?
      res.delete(:title) if self.title.nil?
      res.delete(:type) if res[:type].nil?
    end

    res
  end
end
