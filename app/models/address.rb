# address.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sun Feb 23 20:23:51 2020.
require_dependency Openopus::Core::People::Engine.root.join('app', 'models', 'address').to_s

class Address < ApplicationRecord

  def self.from_hash(hash)
    cols = self.column_names
    address = self.where(id: hash[:id]).first
    address ||= self.new(hash.slice(*cols))
    address
  end
end
