class FailedToken < ApplicationRecord
  belongs_to :user
  belongs_to :provider, foreign_key: :oauth_provider
end
