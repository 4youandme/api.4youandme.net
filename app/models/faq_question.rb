# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class FaqQuestion < ApplicationRecord
  belongs_to :category, class_name: "FaqCategory", foreign_key: "faq_category_id"
  before_save :ensure_position

  def ensure_position
    if not self.position
      last = self.class.unscoped.where(faq_category: self.faq_category).pluck(:position).max rescue 0
      self.position = (last || 0) + 1
    end
  end
end
