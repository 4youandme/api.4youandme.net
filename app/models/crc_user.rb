class CrcUser < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :validatable, password_length: 5..128

  def self.from_hash(hash)
    hash = ActiveSupport::HashWithIndifferentAccess.new(hash)
    user = self.new(hash)
    user
  end
  
  def as_json(options={})
    options[:methods] ||= []
    options[:methods] << :encrypted_password
    super(options)
  end

  def after_database_authentication
    who = username ? username : id
    ip = $request.ip rescue 'unknown'
    AuditLog.create(who: who, what: "login", which: id, when: Time.now, ip_address: ip)
  end

end
