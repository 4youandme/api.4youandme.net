# measurement.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
#
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Mon Dec  9 05:34:37 2019
class Measurement < ApplicationRecord
  # These datasources timestamp their data for 12am UTC the beginning of the day the measurement is for
  MIDNIGHT_UTC_DATASOURCES = ['oura', 'empatica', 'rescuetime']

  WEARABLE_DATASOURCES = ["bodyport", "empatica", "oura"]

  belongs_to :user
  before_save :must_have_gathered_at

  # Get all measurements for the user for the given study days. Some datasources timestamp data by date, whereas others
  # timestamp by actual time, so this method sorts that out
  def self.for_user_study_days(user, start_study_day, end_study_day)
    start_utc_date = (user.study_started_at.to_date + (start_study_day - 1).days).in_time_zone
    end_utc_date = (user.study_started_at.to_date + (end_study_day - 1).days).in_time_zone.end_of_day
    start_tz_date = user.date_of_study_day(start_study_day)
    end_tz_date = user.date_of_study_day(end_study_day) + 24.hours

        Measurement.where(user: user).where(%{(datasource IN (?) AND gathered_at >= ? AND gathered_at < ?) OR
                        (datasource NOT IN (?) AND gathered_at >= ? AND gathered_at < ?)},
                       MIDNIGHT_UTC_DATASOURCES,
                       start_utc_date,
                       end_utc_date,
                       MIDNIGHT_UTC_DATASOURCES,
                       start_tz_date,
                       end_tz_date)
  end

  # Returns an ActiveRecord::Relation for all Measurements from yesterday for the given datasource
  def self.yesterday_for_datasource(datasource)
    if self.datasource_timestamp_is_at_midnight_utc(datasource)
      beg = DateTime.now.beginning_of_day - 1.day
    else
      beg = DateTime.now.in_time_zone(timezone).beginning_of_day - 1.day
    end
    fin = beg + 1.day
    self.where(datasource: datasource).where("gathered_at >= ? and gathered_at < ?", beg, fin)
  end

  # Returns the Date that the measurement is for.
  # Since timestamp semantics vary with different data providers, this function tries to sort out what date the measurement is for.
  def measurement_date
    if self.timestamp_is_at_midnight_utc
      return self.gathered_at.to_date
    else
      return self.gathered_at.in_time_zone(user.timezone).to_date
    end
  end

  # Returns the study day of the measurement
  def study_day
    if self.timestamp_is_at_midnight_utc
      adjusted = gathered_at - gathered_at.in_time_zone(user.timezone).utc_offset.seconds
      user.study_day(adjusted)
    else
      user.study_day(self.measurement_date)
    end
  end

  # Returns true if the Measurement's timestamp is at midnight utc. Returns false if the timestamp is the actual time.
  def timestamp_is_at_midnight_utc
    Measurement.datasource_timestamp_is_at_midnight_utc(self.datasource)
  end

  # Returns true if the +datatype+'s timestamp is at midnight utc. Returns false if the timestamp is the actual time
  def self.datasource_timestamp_is_at_midnight_utc(datatype)
    # oura, empatica, and rescuetime timestamp data for the day at midnight UTC
    # All other sources timestamp an actual time
    ['oura', 'empatica', 'rescuetime'].include?(datatype)
  end

  MOOD_TYPES = ["Very Negative", "Negative", "Neutral", "Positive", "Very Positive"]
  ENERGY_TYPES = ["Very Relaxed", "Relaxed", "Neutral", "Energetic", "Very Energetic"]
  STRESS_TYPES = ["Very Stressed", "Stressed", "Neutral", "Calm", "Very Calm"]

  def self.text_as_value(text)
    return text if not text or text.is_a?(Numeric)
    text = text.split.map(&:capitalize).join(' ')
    candidate = MOOD_TYPES.index(text)
    candidate ||= ENERGY_TYPES.index(text)
    candidate ||= STRESS_TYPES.index(text)
    candidate + 1 if candidate
  end

  def value_as_text
    val = value.round
    return MOOD_TYPES[val - 1] if datatype == "mood"
    return ENERGY_TYPES[val - 1] if datatype == "energy"
    return STRESS_TYPES[val - 1] if datatype == "stress"
    return value.to_s
  end

  def text_as_value(text)
    text = text.split.map(&:capitalize).join(' ')
    return MOOD_TYPES.index(text) + 1 if datatype == "mood"
    return ENERGY_TYPES.index(text) + 1 if datatype == "energy"
    return STRESS_TYPES.index(text) + 1 if datatype == "stress"
    return text.to_f
  end

  def self.make_fake_value(of_type)
    case of_type
    when :weight
      rand(117.0..135.9)
    when :steps
      rand(10200..11000)
    when :mood
      rand(5)
    when :energy
      rand(5)
    when :stress
      rand(5)
    when :oura
      1
    when :pooply
      1
    when :rescuetime
      1
    when :bodyport
      1
    end
  end

  def self.make_fake_datapoint(gathered_at, datatype)
    self.new(datatype: datatype, gathered_at: gathered_at, value: self.make_fake_value(datatype))
  end

  def must_have_gathered_at
    self.gathered_at ||= Time.zone.now
  end

  def pretty_print
    date = gathered_at.in_time_zone(user.timezone).strftime("%Y-%m-%d %H:%M:%S")
    "#{date} [#{'%10.10s' % datasource}:#{'%8.8s' % datatype}] = #{value_as_text}"
  end

end
