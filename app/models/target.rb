class Target < ApplicationRecord
  belongs_to :target_source, polymorphic: true, required: false
  belongs_to :target_dest, polymorphic: true, required: false
  has_many :properties, as: :owner

  def self.from_hash(hash, pos=0)
    cols = self.column_names
    cols.delete("id")
    target = self.new(hash.slice(*cols))
    target.properties << Property.new(name: "targeted_destination", value: hash[:question_id]) if hash[:question_id]

    target
  end

  def as_json(options)
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))

    if not self.target_dest_type.blank?
      res[self.target_dest_type.underscore + "_id"] = self.target_dest_id
    else
      res[:question_id] = "exit"
    end
      
    res.delete(:criteria) if res[:criteria].blank?
    res.delete(:min) if res[:min].blank?
    res.delete(:max) if res[:max].blank?
    res[:criteria] ||= "range" if res[:min]
    if options[:legacy]

      # res.delete(:id)
      res.delete(:target_source_type)
      res.delete(:target_source_id)
      res.delete(:target_dest_type)
      res.delete(:target_dest_id)
      res.delete(:created_at)
      res.delete(:updated_at)
    end

    res
  end

end
