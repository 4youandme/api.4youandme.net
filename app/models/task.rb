# task.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Jan  3 17:53:05 2020.
class Task < ApplicationRecord
  belongs_to :taskable, polymorphic: true, required: false
  has_many :details, class_name: "TaskDetail", dependent: :destroy
  has_many :properties, as: :owner
  has_many :completed_tasks
  has_many :users, through: :completed_tasks
  has_and_belongs_to_many :buttons

  # Return the name of the task that is presented to the user.
  # Currently only meaningful for web-tasks
  def human_name
    self.properties.find_by(name: 'human-name')&.value || name
  end
  
  def self.get(name)
    self.where(name: name).first || self.where(name: name + "-task").first
  end
  
  def self.for_survey(survey)
    task = self.new(name: "survey")
    intro = TaskDetail.from_hash(
      ActiveSupport::HashWithIndifferentAccess.new(
        { name: "intro", type: "info", title: survey.title, button: { label: "Let's start" },
          content: "<p>You are about to start a survey! It has a total " +
          "of #{survey.questions.count} questions in it.</p>", style_class: "light-style"}))

    body = TaskDetail.new(task_type: "survey", format: survey.format, name: survey.name).as_json
    body[:surveys] = [survey.as_json(legacy: true)]

    outro = TaskDetail.from_hash(
      ActiveSupport::HashWithIndifferentAccess.new(
        {name: "outro", type: "info", content_type: "text/plain", style_class: "light-style",
         content: "You've earned 10 points!?", "title": "Thanks!"}))
    task.details << [intro, body, outro]

    task
  end

  def self.from_hash(hash, pos=0)
    task = self.where(name: hash[:task_ref]).first if not hash[:task_ref].blank?

    if not task
      cols = self.column_names
      cols.delete("id")
      task = self.new(hash.slice(*cols))
      task.name ||= hash[:task_name]
      
      task.position ||= pos + 1
      
      if hash.has_key?(:task_details)
        hash[:task_details].each_with_index do |deet, pos|
          task.details << TaskDetail.from_hash(deet, pos)
        end
      end

      hash[:properties]&.each do |prop|
        task.properties.new(name: prop[:name], value: prop[:value])
      end

      # BEG: How to make TECH-DEBT for fun and profit
      [:destination, :type, :app_id].each do |key|
        task.properties.new(name: key.to_s, value: hash[key]) if hash.has_key?(key)
      end
      # END: How to make TECH-DEBT for fun and profit
    end

    task
  end

  def self.for_study_day(day)
    tasks  = self.where(delivery: "daily")
    tasks += self.where(delivery: "even") if day.even?
    tasks += self.where(delivery: "odd") if day.odd?
    tasks += self.where(delivery: "weekly") if (day % 7) == 0
    tasks += self.where(delivery: "biweekly") if (day % 14) == 0

    # Do the right thing for month.  This probably isn't it.
    tasks += self.where(delivery: "monthly") if (day % 30) == 0  # Time.zone.now.day == 1

    tasks
  end

  def self.ema
    task = Task.where(name: "ema", delivery: "daily", first_study_day: 1).first_or_create
    task
  end
  
  def rename_to(new_name)
    self.name = new_name
    self.save
  end

  def task_detail
    deet = details.where.not(task_type: ["intro", "info", "outro"]).first rescue nil
    # SQL IS BROKEN, SO THIS IS REQUIRED.
    deet ||= details.where(task_type: nil).first rescue nil
    deet
  end

  # Returns the task name
  # The seed/update process generates copies of tasks with names like  "trail-making-task-2020-11-23 01:46:03 UTC", which the app does not understand. This function strips the date part from the name
  def base_name
    name.sub(/-\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} [A-Z]*$/, "") rescue nil
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:type] ||= res.delete(:task_type)

    # BEG: How to make TECH-DEBT for fun and profit
    #  id => task_id
    # res[self.class.name.downcase + "_id"] = self.id
    # res.delete(:id)
    tname = self.base_name
    res[self.class.name.downcase + "_name"] = tname
    res[:name] = tname

    #  destination and app_id
    self.properties.each { |p| res[p.name] = p.value }

    # Wait, it gets better...
    if res[:destination]
      res.delete(:task_id)
      res.delete(:position)
    end
    # FIN: How to make TECH-DEBT for fun and profit

    if self.details.length > 0
      res[:task_details] = []
      self.details.each { |deet| res[:task_details].push(deet.as_json(options)) }
    end

    if options[:legacy]
      res.delete(:created_at)
      res.delete(:updated_at)
      res.delete(:taskable_id)
      res.delete(:taskable_type)
      res.delete(:delivery)

      res.delete(:type) if res[:type].nil?
      res.delete(:name) if res[:name].nil?
    end

    res
  end
end
