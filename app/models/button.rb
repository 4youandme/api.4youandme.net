# button.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sat Dec 14 07:54:44 2019.
class Button < ApplicationRecord
  belongs_to :buttonable, polymorphic: true
  has_many :properties, as: :owner
  has_and_belongs_to_many :tasks

  def self.from_hash(hash, pos=0)
    hash = ActiveSupport::HashWithIndifferentAccess.new(hash) if hash.class == Hash
    cols = self.column_names
    cols.delete("id")
    button = self.new(hash.slice(*cols))
    button.position ||= pos + 1
    # BEG: How to make TECH-DEBT for fun and profit
    thash = hash[:click_action]
    if thash and not thash[:type].blank?
      button.properties << Property.new(name: "click_action", value: thash.to_json)
    else
      button.tasks << Task.from_hash(hash[:click_action], 0) if hash.has_key?(:click_action)  # The correct line.
    end
    # FIN: How to make TECH-DEBT for fun and profit
 
    button
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new()
    res[:label] = self.label
    res[:target] = self.target if self.target
    res[:tag] = self.tag if self.tag

    # BEG: How to make TECH-DEBT for fun and profit 
    self.properties.each {|p| res[p.name] = (JSON.parse(p.value) rescue p.value)}   
    # FIN: How to make TECH-DEBT for fun and profit 

    res[:click_action] ||= self.tasks.first.as_json(options) if self.tasks.length > 0
    res.delete(:format) if not res[:format]

    res
  end

  def self.json_helper(for_buttonable, options={})
    res = {}
    if for_buttonable.buttons.length > 0
      res[:buttons] = []
      button = for_buttonable.buttons.select {|b| b.tag == nil }.first
      res[:button] = button.as_json(options) if button

      for_buttonable.buttons.select {|b| b.tag != nil }.each do |b|
        res[:buttons].push(b.as_json(options))

        # Support random legacy stuff.
        # BEG: How to make TECH-DEBT for fun and profit
        if b.tag == "button_content"
          res[b.tag] = b.label
        else
          res[b.tag + "_button"] = b.as_json(options) if b.tag and (b.tag != "reminder" || !options[:skip_reminder])
        end
        # FIN: How to make TECH-DEBT for fun and profit
      end

      res.delete(:buttons) if res[:buttons].blank?
    end


    res
  end

end
