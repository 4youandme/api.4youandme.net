class CompletedTask < ApplicationRecord
  require 'action_view'
  include ActionView::Helpers::DateHelper

  belongs_to :task
  belongs_to :user

  # Note that this relations was added fairly late; it won't always exist. In cases where it doesn't exist, you'll have to find the corresponding tile_schedule by comparing TileSchedule#active_at/#expires_at with CompletedTask#started_at
  belongs_to :tile_schedule, optional: true
  after_commit :update_tile_schedule, on: [:create, :update]

  def name
    username = user.name.blank? ? user.id[0..5] : user.name
    "#{username}-#{task.name}-#{self.completed_at}"
  end

  def user_identifier
    user.name.empty? ? user.id[0..5] : user.name
  end

  def duration
    completed_at - started_at
  end

  def display_duration
    "#{Time.at(self.duration).utc.strftime('%H:%M:%S')} - (#{distance_of_time_in_words(duration)})"
  end

  def survey
    self.task.details.where(task_type: "survey").first.surveys.first rescue nil
  end

  def questions
    survey.questions.order(position: :asc) rescue []
  end

  def survey_answers
    Answer.where(user: self.user, created_at: (self.created_at - 1)..(self.created_at + 1))
  end

  def ema_answers
    Measurement.where(user: self.user, created_at: (self.created_at - 1)..(self.created_at + 1))
  end

  def answers
    result = []
    result = survey_answers if task_type == "survey"
    result = ema_answers if task_type == "ema"

    result
  end

  def update_tile_schedule

    # if we have the tile_schedule_id, use that
    if self.tile_schedule
      found = self.tile_schedule
    else
      # We want to get the tile_schedule entry that matches this completed task.
      # There isn't enough totally unique information being passed back and forth,
      # so we have to heuristically look it up.
      found = nil
      # tile_type = "survey-reference" if self.task_type == "survey"
      # Except that all of our tiles reference a "task".
      tile_type = "task-reference" if self.task_type == "survey"
      tile_type ||= "task"

      candidates = user.tile_schedules.includes(:tile).joins(:tile).
        where(tiles: { tile_type: tile_type }).
        where(completed_at: nil).
        where(':started_at BETWEEN ACTIVE_AT AND EXPIRES_AT', started_at: self.started_at.utc).
        order(active_at: :asc)
      # The previous "includes" statement actually causes the tile to be loaded, but not associated with the
      # fetched candidates!
      candidates.each do |tile_schedule|
        tile_schedule.reload if not tile_schedule.tile

        if self.task.base_name.sub(/-task$/, "") == tile_schedule.tile.base_name || self.task.base_name == tile_schedule.scheduled&.name&.gsub("-copy", "")
          found = tile_schedule
          break
        end
      end
    end

    if found
      found.completed_at = self.completed_at
      found.save
    end

    found
  end

end
