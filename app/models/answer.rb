# answer.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Jan  3 17:54:05 2020.
class Answer < ApplicationRecord
  belongs_to :user
  belongs_to :question

  def question_text
    self.question.contents.first.content
  end

  def options
    self.question.options
  end

  def option_answer
    result = nil
    if question.question_type == "pick-many"
      answers = self.answer.split(",")
      selected = []
      answers.each {|a| x = options.where(value: a).first.contents.first.content; selected.push(x) if x}
      result = selected.to_sentence
    else
      option = options.where(value: self.answer).first rescue nil
      result = option.contents.first.content rescue nil if option
    end
    result
  end
  
  def pretty_print
    "Q: #{question.contents.first.content}\nA: #{answer}"
  end
end
