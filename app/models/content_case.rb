# content_case.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sun Dec 15 09:30:30 2019.
class ContentCase < ApplicationRecord
  belongs_to :content_switch
  has_many :contents, as: :contentable
  has_many :buttons, as: :buttonable

  def add_button(hash)
    self.buttons.new(hash.slice(:label, :target, :tag)) if hash
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new()
    res[:match] = self.match
    c = self.contents.first
    if c
      res[:content] = c.content
      res[:content_type] = c.content_type
      res[:style_class] = c.style_class if c.style_class
    end

    res = res.merge(Button.json_helper(self, options))

    res
  end
end
