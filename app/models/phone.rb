# phone.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Sep 10 10:41:12 2019.
require_dependency Openopus::Core::People::Engine.root.join('app', 'models', 'phone').to_s
class Phone < ApplicationRecord

  def self.obfuscate(number)
    number[0..2] + "(XXX) XXX-" + number[(number.length - 4)..(number.length)] rescue nil
  end

  def obfuscate
    self.class.obfuscate(self.number) rescue nil
  end
  
  def areacode
    code = nil
    code = number[4..6] if not number.blank? and number.start_with?("+1")
    code
  end

  def self.from_hash(hash)
    cols = self.column_names
    phone = self.where(id: hash[:id]).first
    phone ||= self.new(hash.slice(*cols))
    phone
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    res[:label] = self.label;
    res.delete(:label_id)
    res.delete(:phoneable_type)
    res.delete(:phoneable_id)
    res
  end
end
