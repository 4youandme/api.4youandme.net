# content_switch.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sat Dec 14 06:45:30 2019.
class ContentSwitch < ApplicationRecord
  belongs_to :switchable, polymorphic: true
  has_many :cases, class_name: "ContentCase"

  def self.from_hash(hash, pos=0)
    cols = self.column_names
    cols.delete("id")
    switch = self.new(hash.slice(*cols))
    hash.delete(:tag)
    hash.keys.each do |key|
      data = hash[key]
      the_case = switch.cases.new(match: key)
      the_case.contents.new(data.slice(:content, :content_type, :style_class))
      the_case.add_button(data[:button]) if data.has_key?(:button)
    end

    switch
  end

  def as_json(options={})
    res = ActiveSupport::HashWithIndifferentAccess.new(super(options))
    clauses = {}
    self.cases.each do |the_case|
      tmp = the_case.as_json
      clauses[tmp.delete(:match)] = tmp
    end
    res[self.tag] = clauses
    res
  end
end



