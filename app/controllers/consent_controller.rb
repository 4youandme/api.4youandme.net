# consent_controller.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sun Dec 15 08:17:12 2019.

class ConsentController < ApplicationController
  before_action :get_user
  before_action :user_required, only: [:create_signed_consent, :save_answers]

  def consent_sections
    render json: success_result("Here's your consent sections", 200, @user.organization.current_consent_sections.as_json(legacy: true))
  end

  def apps_sections
    data = AppSection.current_app_sections.as_json(legacy: true).sort_by { |as| as[:position] }
    render json: success_result("Here's your app sections", 200, data)
  end

  def create_signed_consent
    filedata = (params[:file].read)
    @user.save_consent_document(filedata)
    render json: success_result("Stored consent document for user #{@user.id}")
  end

  def save_answers
    keys = %w(opt-ins full-consent comprehension-questions screening-questions)
    saved = []
    failed = []

    keys.each do |k|
      if (params.has_key?(k))
        result = nil
        sym = ("save_" + k.gsub("-", "_")).to_sym
        result = self.send(sym, params[k]) if self.respond_to?(sym, true)
        if result
          saved.push(k)
        else
          failed.push(k)
        end
      end
    end

    if failed.blank?
      render json: success_result("#{saved.to_sentence} are saved")
    else
      render json: failure_result("#{failed.to_sentence} failed to save")
    end
  end

  private

  def save_opt_ins(data)
    data[:answers].each do |answer|
      rec = @user.answers.where(question: answer[:question_id]).first_or_create
      rec.answer = answer[:answer]
      rec.save
    end
    true
  end

  def save_full_consent(data)
    @user.gave_full_consent = data[:isAgreed]
    @user.save
  end

  def save_comprehension_questions(data)
    data[:answers].each do |answer|
      rec = @user.answers.where(question: answer[:question_id]).first_or_create
      rec.answer = answer[:answer]
      rec.save
    end
    true
  end

  def save_screening_questions(data)
    data[:answers].each do |answer|
      rec = @user.answers.where(question: answer[:question_id]).first_or_create
      rec.answer = answer[:answer]
      rec.save
    end
    true
  end

  def failure_result(message = "failure", status = 400)
    { success: false, status: status, message: message }
  end

  def success_result(message = "success", status = 201, data = nil)
    return { status: status, message: message, result: data }
  end

  def get_user
    @user ||= @current_user
    @user ||= User.where(id: params[:participant_id]).first if params.has_key?(:participant_id) rescue nil
  end

  def user_required
    render json: failure_result("This function requires a valid user", 401) and return if not @user
  end
end
