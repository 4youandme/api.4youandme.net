# internal_controller.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Thu Feb  6 12:39:32 2020.
class InternalController < ActionController::Base
  protect_from_forgery with: :null_session, prepend: true
  before_action :authenticate_request
  
  def get_accesstokens
    provider = OauthProvider.lookup(params[:provider_name])
    render(json: [], status: 400) and return if not provider
    raw_tokens = AccessToken.joins(:user)
                     .where("users.study_ended_at is null OR users.study_ended_at >= ?", DateTime.now)
                     .where(oauth_provider_id: provider.id).where.not(token: nil)
    tokens = []
    raw_tokens.each do |token|
      token.reload if not token.user
      json = token.as_json.merge(study_started_at: token.user.study_started_at, study_ended_at: token.user.study_ended_at)
      json = ActiveSupport::HashWithIndifferentAccess.new(json)
      json.delete(:refresh_token)
      datasource = provider.name.downcase
      last = token.user.measurements.where(datasource: datasource).order(gathered_at: :desc).first
      last = last.as_json.merge(study_started_at: token.user.study_started_at,
                                study_ended_at: token.user.study_ended_at,
                                timezone: token.user.timezone) if last
      json[:last_measurement] = last
      tokens.push(json)
    end
    
    render json: tokens.as_json
  end
  
  def last_measurements
    datasource = params[:datasource] || "bodyport"
    measurements = Measurement.select('DISTINCT ON ("user_id") *').where(datasource: datasource).order(:user_id, gathered_at: :desc)
    results = []
    measurements.each do |m|
      next if not m.user or not m.user.roster_by_number
      measurement = m.as_json
      measurement[:study_started_at] = m.user.study_started_at
      measurement[:study_ended_at] = m.user.study_ended_at
      measurement[:user_id] = m.user.roster_by_number.empatica_id if datasource == "empatica"
      measurement[:user_id] = m.user.roster_by_number.bodyport_id if datasource == "bodyport"
      measurement[:timezone] = m.user.timezone
      results.push(measurement)
    end
    render json: results.as_json
  end

  def create_measurement
    incoming = params["_json"]
    incoming ||= params
    if incoming.is_a?(Array)
      users_datasource = {}
      incoming.each do |item|
        m = one_measurement(item, update_points: true)
        # After you've done all of the measurements, group them by user_id and datasource
        # and maybe award points and create tiles for that user.
        if m
          users_datasource[m.user_id] ||= []
          users_datasource[m.user_id].push(m.datasource) if not users_datasource[m.user_id].include?(m.datasource)
        end
      end
    else
      m = one_measurement(incoming, update_points: true)
    end

    render json: { result: m }.as_json
  end

  def failed_tokens
    datasource = params[:datasource]
    provider = OauthProvider.lookup(datasource)
    render(json: { error: "Invalid datasource: #{datasource}" }, status: 401) and return if not provider
    incoming = params["_json"]
    incoming ||= params
    bad_tokens = []
    incoming.each do |hash|
      token = AccessToken.where(token: hash[:token], oauth_provider: provider).first
      if not token
        Rails.logger.warn("failed_token failed to match token and provider: #{hash}")
        bad_tokens.push(hash)
      else
        token.user.send_notification(
          title: "Unable to retrieve data from #{provider.name}",
          message: "Please reauthorize the #{provider.name} app from within StressInCrohns. See FAQ for more info.",
          deliver_at: Time.zone.now)
      end
    end

    bad_tokens.each { |t| FailedToken.create(token: t, user: user, provider: provider) rescue nil }
    render json: { bad_tokens: bad_tokens }
  end

  private

  def empatica_massage(hash)
    empatica_id = hash[:user_id].split('-').last rescue nil
    hash.delete(:user_id)
    roster = Roster.where(empatica_id: empatica_id).first
    hash[:user_id] = roster.user.id if roster and roster.user
  end

  def bodyport_massage(hash)
    hash[:value] = (hash[:value].to_f * 2.20462)
    bodyport_id = hash[:user_id].split.last
    hash.delete(:user_id)
    roster = Roster.where(bodyport_id: bodyport_id).first
    hash[:user_id] = roster.user.id if roster and roster.user
  end

  def pooply_massage(hash)
    pooply_id = hash[:user_id]
    hash.delete(:user_id)
    roster = Roster.where(pooply_id: pooply_id).first
    hash[:user_id] = roster.user.id if roster and roster.user
  end

  def oura_massage(hash)
    hash[:gathered_at] = Time.parse(hash[:creation_date]).utc
  end

  def one_measurement(hash, options={})
    hash.permit! if hash.respond_to?("permit!".to_sym)
    hash = ActiveSupport::HashWithIndifferentAccess.new(hash)
    massager = (params[:datasource] + "_massage").to_sym
    self.send(massager, hash) if self.respond_to?(massager, true)
    measurement = nil
    hash[:gathered_at] ||= Time.zone.parse(hash[:creation_date])
    if params[:dataype] == "User not found"
      Rails.logger.info("The user #{hash[:user_id]} wasn't found for the datasource #{hash[:datasource]}")
    else
      existing = Measurement.where(hash.slice(:gathered_at, :user_id, :datasource, :datatype)).first
      # At least in the case of Oura, the value for a given day can change, presumably because the first time we asked
      # for data not all data had been received from the device yet. So here we check if the value has changed, and if
      # it has we update it.
      if existing and existing.value != hash[:value].to_d
        existing.value = hash[:value].to_d
        existing.save
        # Don't return measurement; this function only returns newly created measurements
      elsif hash[:user_id] and not existing
        measurement = Measurement.create(hash.slice(:gathered_at, :user_id, :datasource, :datatype, :value))
        # measurement.maybe_award_points if (options[:update_points]) and measurement
        #
        # ACTUALLY - DO NOT AWARD POINTS AT DATA INGESTION TIME.  INSTEAD, AWARD POINTS IN THE
        # ONCE_PER_STUDY_DAY FUNCTION.  (See User::once_per_study_day.)
      end
    end
    measurement
  end

  def authenticate_request
    api_token = request.headers["api-token"]
    authorized = (not api_token.blank? and api_token == ENV['INTERNAL_API_TOKEN'])
    render(json: { error: 'Not Authorized' }, status: 401) unless authorized
  end
end
