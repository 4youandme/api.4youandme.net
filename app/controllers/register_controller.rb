# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class RegisterController < ApplicationController
  before_action :get_roster, only: [:create, :resend_code]
  before_action :roster_required, only: [:resend_code]
  before_action :get_phone, only: [:create, :resend_code]
  before_action :phone_required, only: [:create]
  before_action :get_user
  skip_before_action :authenticate_request

  SITE_MSSM = 'MSSM'
  SITE_OXFORD = 'Oxford'

  def create

    # Anyone can sign up for the study; if their phone number is not in the system, create a new Roster
    if not @roster
      # TODO: There should be some way to configure the sites; we don't want to hard code this stuff
      if @phone.number.start_with?("+44")
        site = SITE_OXFORD
      else # Currently the only other country codes we allow users to select is "+1"
        site = SITE_MSSM
      end
      @roster = Roster.create(phone: @phone.number, site: site)
    end

    @user = User.create(phone: @phone) if not @user
    @roster.participant = @user
    @roster.save
    @user.is_test_user = true if @roster.is_test_user
    @user.timezone = params[:localTimeZone] if params[:localTimeZone]
    @user.save

    person = @user.person
    person.fname = params[:fname] if params[:fname]
    person.lname = params[:lname] if params[:lname]
    if params[:birthdate]
      begin
        person.birthdate = Date.strptime(params[:birthdate].gsub('-','/'), "%m/%d/%Y")
      rescue ArgumentError => e
        return render(json: failure_result("Invalid 'birthdate' parameter.  Must be of the form 'MM/DD/YYYY'"), status: 400)
      end
    end
    person.save

    validation_code = ValidationCode.where(user: @user).first_or_create
    validation_code.make_code

    # test user for Apple to test app
    is_apple_test_user = @user.phone.number == Phone.canonicalize("+1 (888) 888-8888")
    validation_code.code = "12345" if is_apple_test_user

    validation_code.save
    validation_code.send_code unless is_apple_test_user

    message = "Participant created. "
    message += "Participant is a test user with phone: #{@user.phone.number}." if @user.is_test_user
    message += "Validation code "
    message += "is #{validation_code.code}." if @user.is_test_user
    message += "has been sent via SMS." if not @user.is_test_user
    render json: { success: true, status: 201, message: message }
  end

  def resend_code
    if @user.validation_code
      @user.validation_code.send_code
      render json: success_result("Validation code sent", 201, {})
    else
      render(json: failure_result("No validation code found", 400), status: 400)
    end
  end

  def validate
    if params[:type] == "phone"
      validate_phone
    elsif params[:type] == "email"
      validate_email
    else
      render(json: failure_result("Invalid 'type' parameter.  Must be one of 'phone' or 'email'"), status: 400)
    end
  end

  def validate_phone
    validation_code = ValidationCode.where(code: params[:code]).first if params.has_key?(:code)
    @user = validation_code.user if validation_code
    if @user
      @user.validated = true
      
      if (roster = Roster.where(phone: @user.phone.number).first)
        if roster.after_phone_validation
          @user.status ||= roster.after_phone_validation
        end
      end

      @user.status ||= "consentSummaryScreens"
      @user.save
      p = @user.phone
      p.confirmed = true
      p.save
      render(json: success_result("Successfully validated code", 200, { status: @user.status, participant_id: @user.id }), status: 200)
    else
      render(json: failure_result("Failed to validate", 404), status: 404)
    end
  end

  def validate_email
    email = Email.where(confirmation_code: params[:code]).first if params.has_key?(:code)
    person = email.emailable if email
    @user = person.user if person
    data = { participant_id: nil, status: "verifyEmail" }

    if email
      if @user
        previous_confirmed_status = email.confirmed

        roster = @user.roster
        if roster.respond_to?(:after_email_validation) and roster.after_email_validation
          @user.status = roster.after_email_validation
        end
        data[:participant_id] = @user.id
        data[:status] = @user.status
        @user.save

        # send consent form if we are confirming the email for the first time
        if @user.signed_consent&.attachment && !previous_confirmed_status
          UserMailer.consent(@user).deliver_later
        end
      end

      email.confirmed_at = Time.zone.now
      email.confirmed = true
      email.save

      status = email.confirmed ? "success" : "failed"
    else
      status = "failed"
    end

    # Hack to deal with the fact that the app posts with the wrong content-type
    # Remove this when the app is fixed.
    if request.method == "POST" and status == "success"
      render(json: success_result("Successfully validated code", 200, data), status: 200) and return
    end

    respond_to do |format|
      format.html do
        application_url = "foryouandmesic://validate_email?status=#{status}&email=#{email.address}"
        redirect_to application_url
      end
      
      format.json do
        if status == "success"
          render(json: success_result("Successfully validated code", 200, data), status: 200)
        else
          render(json: failure_result("Invalid code", 200), status: 200)
        end
      end
    end
  end
  
  private

  def get_phone
    @phone = nil
    if params.has_key?(:phone_number)
      @phone = Phone.where(number: Phone.canonicalize(params[:phone_number])).first rescue nil
      @phone ||= Phone.new(number: params[:phone_number])
    end
  end

  def get_roster
    @roster = Roster.where(phone: Phone.canonicalize(params[:phone_number])).first if params.has_key?(:phone_number)
  end

  def phone_required
    render(json: failure_result("The given phone number is not valid: '#{params[:phone_number]}'", 400), status: 400) and return if not @phone
  end

  def roster_required
    render(json: failure_result("Invalid participant: '#{params[:phone_number]}'", 400), status: 400) and return if not @roster
  end

  def get_user
    @user ||= User.where(person: @phone.phoneable).first rescue nil
  end

  def failure_result(message="failed", status=404)
    { success: false, status: status, message: message }
  end

  def success_result(message="success", status=200, data=nil)
    return { status: status, message: message, result: data }
  end

end
