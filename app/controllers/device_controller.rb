# device_controller.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Dec 24 06:36:15 2019.
class DeviceController < ApplicationController
  before_action :get_user
  skip_before_action :authenticate_request, only: [:create]

  def get
    device = Device.where(id: params[:device_id]).first rescue nil
    if device
      render json: success_result("Here's your device", 200, device)
    else
      render json: failure_result("That device just ain't on this server")
    end
  end

  def create
    device = Device.where(id: params[:device_id]).first if params.has_key?(:device_id) && params[:device_id]
    device ||= Device.create(user: @user, device_type: params[:device_type], device_os: params[:device_os])

    if device.save
      render json: success_result("Now have device with device_id = #{device.id}", 201, device.as_json)
    else
      explanation = device.errors.messages.collect { |key, val| "#{key} #{val.join}" }.join('; ') rescue "Oh boy"
      render json: failure_result("Failed to create device: " + explanation), status: 400
    end
  end

  def set_push_token
    @device.push_token_choice_status = params[:status] if params.has_key?(:status)
    @device.push_token = params[:push_token] if params.has_key?(:push_token)
    @device.save

    render json: success_result("push_token and status updated")
  end
      
  private
  def failure_result(message="failure", status=400)
    { success: false, status: status, message: message }
  end

  def success_result(message="success", status=201, data=nil)
    return { success: true, status: status, message: message, result: data }
  end

  def get_user
    @user ||= @current_user
    @user ||= User.where(id: params[:participant_id]).first if params.has_key?(:participant_id) rescue nil
  end

  def user_required
    render json: failure_result("This function requires a valid user", 401) and return if not @user
  end

end
