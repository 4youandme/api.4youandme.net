# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class StaticController < ApplicationController
  skip_before_action :authenticate_request, only: [:contact, :faq, :points]
  before_action :attempt_authentication, only: [:contact, :faq, :points]
  after_action :allow_iframe
  layout false

  def contact
    if @site_based_contact_page
      site = @current_user.site rescue "generic"
      page = "static/contact-#{site}"
      page = "static/contact-generic" if not File.exists?(Rails.root + "/app/views/static/#{page}.html.erb")
    else
      page = "static/contact-fancy"
    end
    render page, content_type: "text/html", formats: [:json, :html]
  end

  def faq
    render "static/faq", content_type: "text/html", formats: [:json, :html]
  end

  def points
    render "static/points", content_type: "text/html", formats: [:json, :html]
  end

  def consent_sections
    deprecated or render_static_json("consent-sections")
  end

  def apps_sections
    deprecated or render_static_json("apps-sections")
  end

  def feed
    deprecated or render_static_json("feed")
  end

  def study_progress
    deprecated or render_static_json("study-progress")
  end

  def profile_content
    render_static_json("profile-content")
  end

  private

  def deprecated
    render json: failure_result("This action: #{params[:controller]}##{params[:action]} has been deprecated and should not be called")
    true
  end
  
  def success_result(message="success", status=201, data=nil)
    return { status: status, message: message, result: data }
  end

  def failure_result(message="failure", status=400)
    return { success: false, status: status, message: message }
  end

  def render_static_json(filebase)
    data = JSON.parse(File.read(Rails.root.join("app", "views", "static", filebase + ".json")))
    render json: success_result("Enjoy the data", 200, data)
  end

  
  def attempt_authentication
    api_token   = request.headers["api-token"]
    api_token ||= request.headers["App-Token"]
    if api_token
      @device = Device.where(api_token: api_token).first
      @current_user = @device.user if @device
    end
  end

  def allow_iframe
    response.headers['X-Frame-Options'] = 'ALLOWALL'
  end
end
