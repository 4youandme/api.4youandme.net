# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session, prepend: true
  before_action :globalize_request
  before_action :authenticate_request, except: [:docs, :healthcheck]
  #after_action :set_paper_trail_whodunnit
  # around_action :setup_timezone

  def docs
    redirect_to "/docs"
  end

  def healthcheck
    render json: { number_of_rosters: Roster.count }
  end

#   def set_paper_trail_whodunnit
#     AuditLog.create(who: "some user", what: "Some action", when: Time.now, ip_address: request.ip)
#   end

  private

  def globalize_request
    $request = request
  end
  
  def authenticate_request
    return if %(devise/sessions administrators).include?(params[:controller])
    return if params[:controller].starts_with?("crc/")
    return if params[:controller].starts_with?("active_admin/")
    return if params[:controller].starts_with?("rails_admin/")
    
    # @current_user = AuthorizeApiRequest.call(request.headers).result
    if not @current_user
      api_token   = request.headers["api-token"]
      api_token ||= request.headers["App-Token"]
      if api_token
        @device = Device.where(api_token: api_token).first
        @current_user = @device.user if @device
      end
    end
    render(json: { error: 'Not Authorized' }, status: 401) unless @current_user
  end

  def setup_timezone
    if @current_user and @current_user.timezone
      Time.use_zone(@current_user.timezone) { yield }
    else
      yield
    end
  end
end

