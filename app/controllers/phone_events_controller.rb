# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class PhoneEventsController < ApplicationController
  before_action :must_have_device, only: [:create]

  def create
    p = event_params
    event = @device.phone_events.create(p)
    if params.has_key?(:localTimeZone) and not params[:localTimeZone].blank?
      @current_user.timezone = params[:localTimeZone]
      @current_user.save
    end
    render json: success_result("Event saved", 201, event.as_json(legacy: true))
  end

  private
  def must_have_device
    render(json: { status: "failure", message: "No associated device found" }, status: 400) and return if not @device
  end

  def event_params
    eparams = {}
    e = PhoneEvent.new
    params.each do |k, v|
      setter = ((k.to_s + "=").to_sym)
      params.permit(k)
      eparams[k] = v if e.respond_to?(setter)
    end
    eparams
  end

  def failure_result(message="failure", status=400)
    { success: false, status: status, message: message }
  end

  def success_result(message="success", status=201, data=nil)
    return { status: status, message: message, result: data }
  end

end
