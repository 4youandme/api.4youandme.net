# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
class FaqController < ApplicationController
  skip_before_action :authenticate_request

  def static
    render "faq/static", content_type: "text/html", formats: [:json, :html]
  end
  
  def all
    result = []
    FaqCategory.where(visible: true).order(position: :asc).each do |cat|
      qs = []
      cat.questions.where(visible: true).order(position: :asc).each do |q|
        qs.push({ question: q.question, answer: q.answer})
      end
      result.push({ category: cat.name, questions: qs })
    end
    render json: result
  end

  def categories
    cats = FaqCategory.all.order(position: :asc)
    render json: cats
  end
  
  def verbatim
    result = []
    FaqCategory.where(visible: true).order(position: :asc).each do |cat|
      out = cat.as_json
      qs = []
      cat.questions.where(visible: true).order(position: :asc).each do |q|
        qs.push(q.as_json)
      end
      out[:questions] = qs
      result.push(out)
    end
    render json: result
  end

  def getone
    faq = FaqQuestion.where(id: params[:id]).first if params.has_key?(:id)
    render json: faq
  end
  
end
