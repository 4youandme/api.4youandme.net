# oauth_controller.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Thu Dec 19 17:01:01 2019.
require 'net/http'

class OauthController < ApplicationController
  before_action :get_provider
  before_action :get_code, except: [:authorize]
  before_action :get_user
  skip_before_action :authenticate_request, except: [:authorize]

  def authorize
    name = OauthProvider.as_identifier(params[:provider_name])
    redirection = "foryouandmesic://#{name}_auth?status=failure"

    if @provider and @user
      redirection = "#{@provider.authorize_uri}?response_type=code"
      redirection += "&client_id=#{@provider.client_id}"
      redirection += "&scope=#{@provider.scope}" if not @provider.scope.blank?
      redirection += "&redirect_uri=#{@provider.redirect_uri}"
      redirection += "&state=#{@user.id}"
    end

    redirect_to redirection
  end
  
  def callback
    status = 'failure'
    provider_identifier = @provider.as_identifier rescue nil
    endpoint = "foryouandmesic://#{provider_identifier}_auth"

    if not @provider
      Rails.logger.error("Callback called with bad provider_name!")
    elsif not @code
      Rails.logger.error("OAuth failed to provide auth code from app")
    elsif not @user
      Rails.logger.error("OAuth failed to provide valid user ID")
    else
      # User authorized via her device.  Now get a lasting access token.
      access_token = @provider.token_post(code: @code, grant_type: "authorization_code", redirect_uri: @provider.redirect_uri, user: @user)

      if not access_token
        Rails.logger.error("OAuth failed to provide access token: #{@provider}: #{@code}")
      else
        status = 'success'
      end
    end

    redirect_to endpoint + "?status=#{status}"
  end

  def providers
    data = OauthProvider.all.as_json(redirect_detail: true, for_user: @user)
    if not params[:v2]
      render json: success_result("Here's the list of providers", 201, data)
    else
      render json: data
    end
  end
  
  private
  
  def get_provider
    @provider = OauthProvider.lookup(params[:provider_name]) if params.has_key?(:provider_name)
    @debugging =  @provider and @provider.oauth_debug
  end

  def get_user
    @user = User.where(id: params[:state].downcase).first if params.has_key?(:state)
    @user ||= @current_user

    if not @user
      api_token = request.headers["api-token"]
      api_token ||= request.headers["App-Token"]
      if api_token
        @device = Device.where(api_token: api_token).first
        @user = @device.user if @device
      end
    end

  end

  def get_code
    @code = params[:code]
  end

  def failure_result(message="failure", status=400)
    { success: false, status: status, message: message }
  end

  def success_result(message="success", status=201, data=nil)
    return { status: status, message: message, result: data }
  end

end
