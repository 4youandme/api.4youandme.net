# user_controller.rb -*- Ruby -*-
#
# EVIXP - Generic API Server Backend for use in health studies.
# 
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# 
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Author: Brian J. Fox (bfox@opuslogica.com)
# Birthdate: Mon Dec  2 17:41:17 2019
#
class UserController < ApplicationController
  before_action :get_user
  before_action :user_required
  before_action :email_required, only: [:register_email]

  def get_status
    render json: success_result("Fetched status for participant_id #{@user.id}", 200, { participant_id: @user.id, status: @user.status })
  end

  def update_status
    @user.status = params[:status]
    @user.save
    render json: success_result("Updated user status for participant #{@user.id}")
  end

  def study_progress
    render json: success_result("Study Progress for participant #{@user.id}", 200, { summary_tile: @user.study_progress }.as_json(legacy: true))
  end

  def feed
    res = @user.feed
    render json: success_result("Feed for #{@user.name} (#{@user.id})", 200, res)
  end

  def tiles
    feed = @user.feed
    sections = feed[:feed_sections]
    if feed[:quick_activity]
      sections ||= []
      sections[0] ||= { tiles:[], title: "Feed for today" }
      tiles = sections[0][:tiles] || []
      ema = feed[:quick_activity]
      ema[:icon] = "heart"
      tiles.unshift(ema)
    end
    
    render json: sections
  end

  def metadata
    render json: @user.feed_metadata
  end
  
  def register_email
    ((@user.email = @email) and @user.save) if @user
    e = @user.emails.where(address: Email.canonicalize(@email)).first if @user
    e ||= @user.emails.order(updated_at: desc).first if @user

    # Always send confirmation code, even if already confirmed.  However, we might want
    # to tell the user that somebody is trying to reconfirm an already confirmed address
    # here. [bfox]
    e.send_confirmation_code(request)
    render json: success_result("Validation code: #{e.confirmation_code}.")
  end

  def your_data
    stream = params[:stream]
    if stream.is_a?(String)
      interim = stream.sub("[", "").sub("]", "").gsub('"',"").gsub("'", "").split(",")
      datatype = interim.collect {|s| s.strip}
    else
      datatype = stream
    end

    # If given an array of strings, turn them into an array of symbols.
    datatype = datatype.collect {|elt| elt.to_sym} if datatype.is_a?(Array)
    
    # Be flexible for the frequency parameter.
    freq = params[:period] || params[:freq] || params[:frequency]
    buckets = params[:buckets]

    if freq
      your_data = @user.get_measurements(beg:params["start-date"], fin:params["end-date"], datatype:datatype, freq:freq)
    else
      buckets ||= 4
      your_data = @user.get_measurements(beg:params["start-date"], fin:params["end-date"], datatype:datatype, buckets:buckets)
    end

    result = your_data.as_json(legacy:true)

    render json: success_result("Your summary data is here", 200, result)
  end

  def calendar_feed
    schedule = @user.schedule_as_calendar_events(beg: params[:beg], fin: params[:fin])
    render json: success_result("Here's the calendar-based feed", 200, schedule)
  end
  
  private
  def failure_result(message="failure", status=400)
    { success: false, status: status, message: message }
  end

  def success_result(message="success", status=201, data=nil)
    return { status: status, message: message, result: data }
  end

  def get_user
    @user ||= @current_user
    @user ||= User.where(id: params[:participant_id]).first if params.has_key?(:participant_id) rescue nil
  end

  def user_required
    render json: failure_result("This function requires a valid user", 401) and return if not @user
  end

  def email_required
    @email = params[:email] if params.has_key?(:email)
    render json: failure_result("Missing 'email' parameter") and return if not @email
    render json: failure_result("Invalid email address format: '#{@email}'") if not Email.valid_format?(@email)
  end

end
