# authentication_controller.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
#  Birthdate: Mon Sep 30 12:21:40 2019.
class AuthenticationController < ApplicationController
  protect_from_forgery with: :null_session
  skip_before_action :authenticate_request
  
  def device_authenticate
    api_token = request.headers["api-token"]
    Rails.logger.info("DEVICE_AUTHENTICATE GOT API-TOKEN HEADER: '#{api_token}'") if api_token
    api_token ||= request.headers["App-Token"]
    Rails.logger.info("DEVICE_AUTHENTICATE GOT APP-TOKEN HEADER: '#{api_token}'") if request.headers["App-Token"]

    device = Device.where(access_token: api_token).first
    @user = device.user if device
    render(json: { error: "not authorized!" }, status: 402) and return if not @user
    @current_user = @user
  end
  
  def authenticate
    command = AuthenticateUser.call(params[:username], params[:password])

    if command.success?
      user = User.where(email: params[:username]).first.as_json(token: command.result)
      render json: user
    else
      render(json: { error: command.errors }, status: :unauthorized)
    end
  end
end
