# tasks_controller.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sun Feb 16 09:35:31 2020.

require 'aws-sdk-s3'

class TasksController < ApplicationController
  before_action :user_required, except: [:redirect_injest, :external_redirect_done]
  skip_before_action :authenticate_request, only: [:redirect_injest, :external_redirect_done]

  def ingest
    # Okay.  Massage the kuso out of the incoming "data" in order to get something useful.
    # Or, perhaps change the frontend to just deliver something useful.  FMHWA12ID.
    data = params[:data] rescue nil
    task_type = params[:task_type]
    task = TaskDetail.where(id: params[:id]).first.task rescue nil
    task ||= Task.find_by(id: params[:id])
    # TASK can be empty if the task_type is "ema", or, if the name has been mangled.
    task ||= Task.ema if task_type == "ema"
    task ||= Task.get(task_type)
    c = CompletedTask.new(task: task, user: @user, tile_schedule_id: params[:tile_schedule_id])

    # Sometimes, the client gives us startTime/endTime in seconds since epoch ( ~ 1.5e9)
    # Other times, the client gives us startTime/endTime in ms since epoch ( ~ 1.5e10)
    c.started_at = client_people_time(data[:startTime])
    c.completed_at = client_people_time(data[:endTime])

    # This is wrong... the current day might not be the study day of the task. The task startTime should be used to figure out what the study day is. In any case, keeping it wrong for the sake of consistency.
    c.study_day = @user.study_day
    c.task_type = task_type

    # If we have a specific function for handling this type of ingestible data, call it.
    ingester = "#{task_type.underscore}_ingester".to_sym
    ingester = :default_ingester if not self.respond_to?(ingester, true)
    completed = self.send(ingester, data, c)
    completed = c.save if completed

    if completed
      @user.add_point_reward_for_completed_task(c)
      render json: success_result("Task #{task.name} completed")
    else
      render json: failure_result("Task #{task.name} failed to save: #{c.errors.to_s}")
    end
  end

  def default_ingester(data, completed_task = nil)
    survey_ingester(data, completed_task)
  end

  def survey_ingester(data, completed_task = nil)
    data[:answers].each do |answer|
      if not answer[:question_id].blank?
        Answer.create(user: @user, question_id: answer[:question_id], answer: answer[:answer])
      elsif not answer[:question].blank?
        # These will become measurements.
        gathered_at = completed_task ? completed_task.completed_at : Time.zone.now
        gathered_at ||= Time.zone.now
        @user.measurements << Measurement.create(datatype: answer[:question], gathered_at: gathered_at,
                                                 datasource: "ema", value: Measurement.text_as_value(answer[:answer]))
      else
        # debugger
      end
    end

    true
  end

  def trail_making_ingester(data, completed_task = nil)
    #  Parameters: {"data"=>{"startTime"=>1580400424.7834182, "taps"=>[{"timestamp"=>2.71609890460968, "incorrect"=>false, "index"=>0}, {"index"=>1, "timestamp"=>3.770328998565674, "incorrect"=>false}, {"incorrect"=>false, "index"=>2, "timestamp"=>4.6123799085617065}, {"timestamp"=>7.062704920768738, "incorrect"=>false, "index"=>3}, {"index"=>4, "timestamp"=>9.513457894325256, "incorrect"=>false}, {"incorrect"=>false, "timestamp"=>10.12193489074707, "index"=>5}, {"index"=>6, "timestamp"=>11.6303049325943, "incorrect"=>false}, {"incorrect"=>false, "timestamp"=>12.247810959815979, "index"=>7}, {"incorrect"=>false, "timestamp"=>12.93093192577362, "index"=>8}, {"timestamp"=>13.538915991783142, "index"=>9, "incorrect"=>false}, {"incorrect"=>false, "index"=>10, "timestamp"=>15.189064979553223}, {"incorrect"=>false, "timestamp"=>15.86456298828125, "index"=>11}, {"timestamp"=>18.580633997917175, "incorrect"=>false, "index"=>12}], "endTime"=>1580400444.87008, "numberOfErrors"=>0}, "id"=>"5b0c9e48-16fc-4778-bc51-d8671cd10ace", "task_type"=>"trail-making", "task"=>{"id"=>"5b0c9e48-16fc-4778-bc51-d8671cd10ace"}}
    time = client_people_time(data[:startTime])
    filename = aws_upload_filename(params[:task_type], time)
    completed_task.data_location = filename if completed_task
    save_and_upload(data, time)

    true
  end

  def walk_test_ingester(data, completed_task = nil)
    time = client_people_time(data[:startTime])
    filename = aws_upload_filename(params[:task_type], time)
    completed_task.data_location = filename if completed_task
    save_and_upload(data, time)

    true
  end

  def reaction_time_attempt(attempt)
    timestamp = attempt[:timestamp]
    dmi = ActiveSupport::HashWithIndifferentAccess.new(JSON.parse(attempt[:deviceMotion_info])) rescue nil
    items = dmi[:items]
  end

  def reaction_time_ingester(data, completed_task = nil)
    time = client_people_time(data[:startTime])
    filename = aws_upload_filename(params[:task_type], time)
    # attempts = data[:attempts]
    # attempts.each { |attempt| reaction_time_attempt(attempt) }
    completed_task.data_location = filename if completed_task
    save_and_upload(data, time)
    true
  end

  def video_upload_url
    v = VideoDiary.where(user: @user, size: params[:video_size]).first_or_create
    v.started_at = client_people_time(params[:startTime]) rescue Time.zone.now - 5.seconds
    v.completed_at = client_people_time(params[:endTime]) rescue Time.zone.now
    url = v.upload_url(Date.current)
    v.save

    task_type = "video-diary"
    task = Task.get("video-diary")

    c = @user.completed_tasks.create(task: task, task_type: task_type, completed_at: v.completed_at, started_at: v.started_at, study_day: @user.study_day)
    c.save

    # Except that the client can't do the right thing yet, so we'll just handle it.
    url = request.url.gsub(/video-upload-url.*$/, "consume-video")
    @user.add_point_reward_for_completed_task(c)
    render json: success_result("Video Diary Upload URL", 200, { upload_uri: url })
  end

  def consume_video
    # The client ships a variable which is the randomly created name of a file.
    # The value of that variable is the file data.  So, get rid of the "rest" of
    # the variables, and what's left is the variable we care about.
    xxx = params
    xxx.delete(:id)
    xxx.delete(:startTime)
    xxx.delete(:endTime)
    xxx.delete(:controller)
    xxx.delete(:action)
    file = xxx[xxx.keys[0]]
    body = (file.read)

    v = VideoDiary.where(user: @user, size: body.size).first_or_create
    url = v.upload_url
    Net::HTTP.start(url.host) do |http|
      http.send_request("PUT", url.request_uri, body, { "content-type" => "" })
    end

    render json: success_result("Video finally uploaded", 200, v)
  end

  def set_reminder
    # The only information coming from the client is the USER and the TASK_ID.
    # The intent is to add a notification related to the tile that contained
    # the "Remind me in an hour".  We will derive the information needed.
    success = nil
    right_now = Time.zone.now

    task = Task.find(params[:task_id]) rescue nil
    basis = @user.tile_schedules.incomplete.where(':date BETWEEN ACTIVE_AT AND EXPIRES_AT', date: right_now)

    ts = basis.find_named(task.name.gsub(/[-](copy|task).*$/, "")).first rescue nil
    ts ||= basis.find_named(task.name).first rescue nil

    if ts
      ts.reload if not ts.tile
      ts.reminded_at = right_now
      ts.expires_at = ts.expires_at + 1.hour
      ts.save # or debugger
      name = TileSchedule.canonicalize_task_name(task.name, remove_copy_task: true)
      success = @user.notifications.create(title: "You asked me to remind you...",
                                           message: "It's time to complete the #{name} task!",
                                           deliver_at: right_now + 1.hour,
                                           payload: { type: "task", task_id: task.id })
    else
      # debugger
    end

    render json: success_result("Ok, the reminder is set for #{@user.name}")
  end

  # handles a GET caused by a redirect from an external service (e.g. CamCog)
  # Skips authentication, but that's okay because you would have to have the GUID of a TileSchedule to do anything with
  # this endpoint
  def redirect_injest
    ts = TileSchedule.find_by(id: params[:tile_sched_id])

    if ts.nil?
      render status: 404, body: nil
      return
    end

    if ts.completed_at.nil?
      # ts.update(completed_at: DateTime.now)
      task = ts.scheduled
      user = ts.user

      completed_task = CompletedTask.new(
        task: task,
        user: ts.user,
        started_at: DateTime.now,
        completed_at: DateTime.now,
        study_day: user.study_day,
        task_type: 'web-task'
      )
      completed_task.save

      ts.user.add_point_reward_for_completed_task(completed_task)
    end

    redirect_to "/app/api/v1/tasks/external-redirect-done"
  end

  # "dummy" endpoint that does nothing. Used by the app to determine when the user is done with the webview
  def external_redirect_done
    # We could put some content in the page here
    render html: "" "
<html>
<head>
</head>
<body>
</body>
</html>

" "".html_safe
  end

  private

  # Sometimes, the client gives us startTime/endTime in seconds since epoch ( ~ 1.5e9)
  # Other times, the client gives us startTime/endTime in ms since epoch ( ~ 1.5e10)
  # Thanks, random behavior client.
  def client_people_time(ms_or_sec)
    result = ms_or_sec
    result = result.to_i / 1000 if result.to_i > 3.0e9
    Time.zone.at(result)
  end

  def failure_result(message = "failure", status = 400)
    { success: false, status: status, message: message }
  end

  def success_result(message = "success", status = 201, data = nil)
    return { status: status, message: message, result: data }
  end

  def user_required
    @user ||= @current_user
    data render json: failure_result("This function requires a valid user", 401) and return if not @user
  end

  def aws_upload_filename(filename_base, time = Time.zone.now)
    date = time.in_time_zone(@user.timezone).to_datetime.iso8601
    filename = "evidation/sic/users/#{@user.id}/#{filename_base}/#{filename_base}-#{@user.id}-#{date}.json"
    filename
  end

  def aws_upload_url(filename_base, time = Time.zone.now)
    filename = aws_upload_filename(filename_base, time)
    bucket = ENV['AWS_BUCKET']
    region = ENV['AWS_REGION']
    s3 = Aws::S3::Resource.new(access_key_id: ENV['AWS_KEY'], secret_access_key: ENV['AWS_SECRET'], region: region)
    obj = s3.bucket(bucket).object(filename)
    url = URI.parse(obj.presigned_url(:put))
    url
  end

  def save_and_upload(data, time = Time.zone.now)
    task_type = params[:task_type]
    filename = "/tmp/#{task_type}-data.json"
    File.open(filename, "wb") { |f| f.write(data.to_json) }
    body = File.read(filename)
    url = aws_upload_url(task_type, time)
    Net::HTTP.start(url.host, url.port, use_ssl: url.scheme == 'https') do |http|
      res = http.send_request("PUT", url.request_uri, body, { "content-type": "application/json" })
      File.open("./tmp/aws-output.txt", "wb") { |file| file.write(res.body) }
    end
  end
end
