# coding: utf-8
# user_mailer.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Mon Dec 16 14:12:48 2019.

class UserMailer < ApplicationMailer

  def please_confirm(options={})
    eobj = options[:email_object] rescue nil
    # eobj ||= User.where(is_test_user: true).first.email rescue nil
    # eobj ||= Email.first
    # options ||= {
    #  user: User.where(is_test_user: true).first,
    #  base_url: "http://localhost:3000",
    #  email_object: eobj
    # }

    @user = options[:user]
    @base_url = options[:base_url]
    @email = options[:email_object]
    @validate_url = "#{@base_url}/app/api/v1/validate-code?code=#{@email.confirmation_code}&type=email"
    mail(to: @email.address, subject: "Please confirm your email address...")
  end

  def consent(user)
    consent_pdf = user.signed_consent.attachment.blob.download
    attachments['sic-consent.pdf'] = { mime_type: "application/pdf", content: consent_pdf }
    @user = user
    @email = @user.email
    mail(to: @email.address, subject: "Your signed consent form from Stress in Crohn’s study")
  end

  def notification(options={})
    eobj = options[:email_object] rescue nil
    @user = options[:user]
    @notification = options[:notification]
    mail(to: @email.address, subject: @notification.title)
  end

end
