require 'test_helper'

class UserTest < ActiveSupport::TestCase

  DATASOURCE_DATATYPES = {
    bodyport: "weight",
    empatica: "Worn correctly",
    oura: "steps"
  }

  setup do
    # phone = Phone.new(number: '+1 (555) 555-5555') # Europe/London
    phone = Phone.new(number: '+1 (303) 555-5555') # MST (-7h UTC)
    r = Roster.create(phone: phone.number, site: 'mssm')
    r.start_study(false)
    @user = r.user
  end

  def clean_user
    @user.point_rewards.destroy_all
    @user.measurements.destroy_all
    @user.tile_schedules.destroy_all
  end

  # Create a measurement for @user for the given datasource on the given study day
  def create_measurement(datasource, study_day, value = 1234)
    if Measurement::MIDNIGHT_UTC_DATASOURCES.include?(datasource)
      gathered_at = (@user.study_started_at.to_date + (study_day - 1).days)
    else
      gathered_at = @user.study_started_at + (study_day - 1).days
    end

    Measurement.create(datasource: datasource,
                       user: @user,
                       datatype: DATASOURCE_DATATYPES[datasource.to_sym],
                       value: value,
                       gathered_at: gathered_at)

  end

  # Test that everything works when creating a single measurement for the given datasource
  def single_measurement_test(datasource)
    create_measurement(datasource, 2)

    @user.award_device_compliance_points_for_study_day(1)

    # make sure tomorrow's measurement doesn't get counted today
    assert_equal 0, @user.point_rewards.length, "No points are rewarded"
    assert_equal 0, @user.tile_schedules.length

    @user.reload

    @user.award_device_compliance_points_for_study_day(3)
    assert_equal 1, @user.point_rewards.length, "One point reward is rewarded"

    assert_equal 5, @user.point_rewards.pluck(:points).sum

    assert_equal 1, @user.tile_schedules.length

    # Verify that the scheduled award tile is for the day after the measurement
    new_tile_schedule = @user.tile_schedules.first
    assert_equal 3, new_tile_schedule.study_day

    clean_user
  end

  test "the user initilizes" do
    assert @user
    assert @user.study_day == 1
  end

  test "award_device_compliance_points_for_study_day assigns no points when user has no measurements" do
    @user.award_device_compliance_points_for_study_day(10)
    assert @user.point_rewards.length == 0
  end

  test "award_device_compliance_points_for_study_day for single oura measurement" do
    single_measurement_test('oura')
  end

  test "award_device_compliance_points_for_study_day for single bodyport measurement" do
    single_measurement_test('bodyport')
  end

  test "award_device_compliance_points_for_study_day assigns no points when user has one oura device measurement with 0 steps" do
    create_measurement('oura', 2, 0)

    @user.award_device_compliance_points_for_study_day(2)
    # @user.once_per_study_day(study_day: 2)
    assert_equal 0, @user.point_rewards.length, "Points should not be rewarded for zero steps"
    assert_equal 0, @user.point_rewards.pluck(:points).sum

    clean_user
  end

  # Make sure awards still get awarded after first month (there used to be a bug where they weren't)
  test "award_device_compliance_points_for_study_day assigns 5 points when user has one bodyport device measurement after 31 days (beg of day)" do
    create_measurement('bodyport', 45)

    @user.award_device_compliance_points_for_study_day(45)

    assert_equal 1, @user.point_rewards.length, "One point reward is rewarded"
    assert_equal 5, @user.point_rewards.pluck(:points).sum, "Five points are rewarded"
    assert_equal 1, @user.tile_schedules.length

    clean_user
  end

  test " award_device_compliance_points_for_study_day assigns 10 points when user has a run of measurements " do
    (1..3).each do |study_day|
      create_measurement('oura', study_day)
    end

    # Run on previous days too, so that we can ensure that point_rewards are updated correctly
    @user.award_device_compliance_points_for_study_day(1)
    @user.award_device_compliance_points_for_study_day(2)
    @user.award_device_compliance_points_for_study_day(3)

    assert_equal 3, @user.point_rewards.length
    assert_equal 30, @user.point_rewards.pluck(:points).sum
    assert_equal 3, @user.tile_schedules.length

    clean_user
  end

  test " award_device_compliance_points_for_study_day assigns 20 points / device / day
for full compliance " do

    ['bodyport', 'empatica', 'oura'].each do |datasource|
      (1..3).each do |study_day|
        create_measurement(datasource, study_day)
      end

    end

    @user.award_device_compliance_points_for_study_day(3)

    assert 9, @user.point_rewards.count
    # 3 devices * 3 days * 20 points
    assert 180, @user.point_rewards.pluck(:points).sum

    clean_user
  end

  test "#once_per_study_day causes rewards to be made after first month" do
    create_measurement('bodyport', 45)

    @user.once_per_study_day({ study_day: 46 })

    assert_equal 1, @user.point_rewards.length, "One point reward is rewarded"
    assert_equal 5, @user.point_rewards.pluck(:points).sum, "Five points are rewarded"
    tiles = @user.tile_schedules.map { |ts| ts.tile }

    # Make sure there are tiles for the missing data
    tile_names = tiles.map { |t| t.name }
    assert_includes(tile_names, "oura-missing-data-copy")
    assert_includes(tile_names, "pooply-missing-data-copy")
    assert_includes(tile_names, "rescuetime-missing-data-copy")

    # Make sure there is a single tile for the Bodyport measurement
    reward_tile = tiles.filter { |t| t.name == "reward-info" }
    assert_equal(1, reward_tile.length, "There should be a single reward tile")

    clean_user
  end

  test "User#once_per_study_day sends missing data notifications when there is no data" do
    assert_equal 0 , Measurement.for_user_study_days(@user, 2, 2).count, "Test should start with no measurements"
    assert_equal 0, @user.tile_schedules.count, "Test should start with not tile schedules"

    # Notifications for study day 2 are generated on study day 3
    @user.once_per_study_day(study_day: 3)


    reminder_notifications = @user.notifications.where(message: "We didn't get any data from your devices yesterday")
    assert_equal 1, reminder_notifications.count


    ["bodyport-missing-data", "oura-missing-data","empatica-missing-data","pooply-missing-data", "rescuetime-missing-data"].each do |tile_name|
      ts = @user.tile_schedules.joins(:tile).where("tiles.name like '#{tile_name}%'")
      assert_equal 1, ts.count, "There should be one tile_schedule for the tile #{tile_name}"
    end
  end

  test "add_point_reward_for_completed_task" do
    video_diary_task = Task.find_by(name: 'video-diary');

    completed_task = CompletedTask.create(
      user: @user,
      started_at: DateTime.now - 1.hours,
      completed_at: DateTime.now + 5.minutes,
      task_type: 'video-diary',
      task: video_diary_task,
    )

    assert_empty @user.point_rewards
    user_tile_schedule_number = @user.tile_schedules.size

    @user.add_point_reward_for_completed_task(completed_task)

    # User should now have one PointReward and an additional TileSchedule
    assert_equal 1, @user.point_rewards.size
    assert_equal user_tile_schedule_number + 1, @user.tile_schedules.size


    # Make sure a duplicate CompletedTask does not cause new points/tiles to be created
    duplicate_completed_task = completed_task.dup
    duplicate_completed_task.save

    @user.add_point_reward_for_completed_task(duplicate_completed_task)

    assert_equal 1, @user.point_rewards.size
    assert_equal user_tile_schedule_number + 1, @user.tile_schedules.size
  end

end