require 'test_helper'

class UserControllerTest < ActionDispatch::IntegrationTest

  test "Feed is updated when survey is submitted" do

    user = Person.find_by(lname: 'Appleseed').user
    user.start_study

    now = DateTime.now

    # Add task to user's feed
    ibd_task = Task.find_by(name: 'ibd-pro')
    ibd_task_details = ibd_task.details.where(name: ibd_task.name)
    user.add_tile_at(ibd_task.name, now - 30.minutes)

    before_feed_body = get_feed(user)

    feed_task = find_task_tile_in_feed(before_feed_body, ibd_task.id)

    assert_not_nil feed_task, "Task should be in feed"
    assert_equal 0, before_feed_body['result']['feed_metadata']['points'], "User should start with no points"


    # submit the survey
    post "/app/api/v1/tasks/survey", as: :json, params: {
      "data" => {
        "endTime" => now.to_i * 1000,
        "startTime" => (now - 2.minutes).to_i * 1000,
        # These question_ids won't match anything in the database; the endpoint apparently doesn't care
        "answers" => [
          { "answer" => "1", "question_id" => "a6f39ec1-dd42-41d0-8ed6-b1e526a1d871", "question_type" => "numerical" },
          { "answer" => "a4", "question_id" => "00c897a0-e3be-4d7b-8095-9b160ba9ff22", "question_type" => "pick-one" }
        ]
      },
      "id" => ibd_task_details.first.id,
      "tile_schedule_id" => feed_task['tile_schedule_id'],
      "task_type" => "survey",
      "task" => { "id" => ibd_task.id }
    },
         headers: {
           'api-token': user.devices.first.api_token
         }

    assert_response :success

    after_feed_body = get_feed(user)

    assert_nil find_task_tile_in_feed(after_feed_body, ibd_task.id), "Task should be removed from feed"
    assert_equal 5, after_feed_body['result']['feed_metadata']['points'], "Points were assigned"
  end

  # Gets the feed for the user
  def get_feed(user)
    get feed_url, as: :json, params: {
      participant_id: user.id
    }, headers: {
      'api-token': user.devices.first.api_token
    }
    assert_response :success
    JSON.parse(@response.body)
  end

  # Returns the task tile for the task with task_id. Returns nil if there is no matching tile
  def find_task_tile_in_feed(feed_body, task_id)

    feed_body['result']['feed_sections'].each do |section|
      section['tiles'].each do |tile|
        if tile&.[]('button')&.[]('click_action')&.[]('id') == task_id
          return tile
        end
      end
    end

    nil
  end

end
