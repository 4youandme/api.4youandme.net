# user_mailer_preview.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Mon Dec 16 13:52:35 2019.
class UserMailerPreview < ActionMailer::Preview

  def please_confirm()
    UserMailer.please_confirm(user: User.where(is_test_user: true).first, base_url: "http://localhost:3000/", email_object: Email.first)
  end
end
