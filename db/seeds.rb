# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

print "Seeding data:"

if Rails.env != "production" or ENV["DEVELOPER_CONTAINER"] == "true"
  print "\nAdministrators..."
  require "./db/seed-data/administrators.rb"
end

print "\nArea code to Timezone mapping"
require "./db/seed-data/areacodes.rb"

print "\nOrganizations..."
require "./db/seed-data/organization.rb"

print "\nRoster Entries..."
require "./db/seed-data/roster.rb"

print "\nOauth Providers..."
require "./db/seed-data/providers.rb"

print "\nFAQ Entries..."
require "./db/seed-data/faqs.rb"

print "\nConsent Sections..."
require "./db/seed-data/consent-sections.rb"

print "\nApp Sections..."
require "./db/seed-data/app-sections.rb"

print "\nSurveys..."
require "./db/seed-data/surveys.rb"

print "\nTasks..."
require "./db/seed-data/tasks.rb"

print "\nTiles..."
require "./db/seed-data/tiles.rb"

print "\n Users: "
require "./db/seed-data/appleseed.rb"
# require "./db/seed-data/bfox-roster.rb"
# require "./db/seed-data/evidation-rosters.rb"
# require "./db/seed-data/bfox-user.rb"
# require "./db/seed-data/yml-test-user.rb"

print "\nDone."
