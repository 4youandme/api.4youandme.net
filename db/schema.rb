# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_12_30_192730) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "access_tokens", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "oauth_provider_id"
    t.string "token"
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "refresh_token"
    t.index ["oauth_provider_id"], name: "index_access_tokens_on_oauth_provider_id"
    t.index ["user_id"], name: "index_access_tokens_on_user_id"
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "active_storage_attachments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.uuid "record_id", null: false
    t.uuid "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "addresses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "label_id"
    t.string "addressable_type"
    t.uuid "addressable_id"
    t.string "line1"
    t.string "line2"
    t.string "city"
    t.string "state"
    t.string "postal"
    t.string "country"
    t.float "lat"
    t.float "lon"
    t.boolean "confirmed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
    t.index ["label_id"], name: "index_addresses_on_label_id"
  end

  create_table "administrators", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_administrators_on_email", unique: true
    t.index ["reset_password_token"], name: "index_administrators_on_reset_password_token", unique: true
  end

  create_table "answers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "question_id"
    t.string "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_answers_on_question_id"
    t.index ["user_id"], name: "index_answers_on_user_id"
  end

  create_table "app_section_infos", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "app_section_id"
    t.string "name"
    t.integer "position"
    t.string "title"
    t.string "section_type"
    t.string "format"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["app_section_id"], name: "index_app_section_infos_on_app_section_id"
  end

  create_table "app_sections", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.string "group"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "area_codes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "code"
    t.string "timezone"
    t.string "location"
    t.date "assigned_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "audit_logs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "who"
    t.string "what"
    t.string "which"
    t.string "when"
    t.string "ip_address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "buttons", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "buttonable_type"
    t.uuid "buttonable_id"
    t.integer "position"
    t.string "tag"
    t.string "label"
    t.string "target"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["buttonable_type", "buttonable_id"], name: "index_buttons_on_buttonable_type_and_buttonable_id"
  end

  create_table "buttons_tasks", id: false, force: :cascade do |t|
    t.uuid "button_id"
    t.uuid "task_id"
    t.index ["button_id"], name: "index_buttons_tasks_on_button_id"
    t.index ["task_id"], name: "index_buttons_tasks_on_task_id"
  end

  create_table "completed_tasks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "task_id"
    t.uuid "user_id"
    t.string "data_location"
    t.string "task_type"
    t.integer "study_day"
    t.datetime "started_at"
    t.datetime "completed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "tile_schedule_id"
    t.index ["task_id"], name: "index_completed_tasks_on_task_id"
    t.index ["user_id"], name: "index_completed_tasks_on_user_id"
  end

  create_table "consent_section_infos", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "consent_section_id"
    t.string "name"
    t.string "title"
    t.string "section_type"
    t.integer "position"
    t.string "format"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["consent_section_id"], name: "index_consent_section_infos_on_consent_section_id"
  end

  create_table "consent_sections", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.boolean "gatekeeper", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sectionable_type"
    t.uuid "sectionable_id"
    t.index ["sectionable_type", "sectionable_id"], name: "index_consent_sections_on_sectionable_type_and_sectionable_id"
  end

  create_table "content_cases", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "content_switch_id"
    t.string "match"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["content_switch_id"], name: "index_content_cases_on_content_switch_id"
  end

  create_table "content_switches", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "switchable_type"
    t.uuid "switchable_id"
    t.string "tag"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["switchable_type", "switchable_id"], name: "index_content_switches_on_switchable_type_and_switchable_id"
  end

  create_table "contents", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "contentable_type"
    t.uuid "contentable_id"
    t.string "tag"
    t.text "content"
    t.string "content_type"
    t.string "style_class"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contentable_type", "contentable_id"], name: "index_contents_on_contentable_type_and_contentable_id"
  end

  create_table "crc_users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "username"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.boolean "superuser"
    t.string "site"
    t.index ["email"], name: "index_crc_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_crc_users_on_reset_password_token", unique: true
  end

  create_table "devices", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.string "name"
    t.string "push_token"
    t.string "push_token_choice_status"
    t.string "api_token"
    t.string "device_type"
    t.string "device_os"
    t.string "dimensions"
    t.string "resolution"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_devices_on_user_id"
  end

  create_table "emails", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "label_id"
    t.string "emailable_type"
    t.uuid "emailable_id"
    t.string "address"
    t.string "confirmation_code"
    t.boolean "confirmed"
    t.datetime "confirmed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["emailable_type", "emailable_id"], name: "index_emails_on_emailable_type_and_emailable_id"
    t.index ["label_id"], name: "index_emails_on_label_id"
  end

  create_table "failed_tokens", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "token"
    t.uuid "user_id"
    t.uuid "oauth_provider_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["oauth_provider_id"], name: "index_failed_tokens_on_oauth_provider_id"
    t.index ["user_id"], name: "index_failed_tokens_on_user_id"
  end

  create_table "faq_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "language", default: "en"
    t.integer "position"
    t.boolean "visible", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "faq_questions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "faq_category_id"
    t.string "language", default: "en"
    t.integer "position"
    t.boolean "visible", default: true
    t.string "question"
    t.string "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["faq_category_id"], name: "index_faq_questions_on_faq_category_id"
  end

  create_table "labels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "value"
    t.string "lang"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "login_activities", force: :cascade do |t|
    t.string "scope"
    t.string "strategy"
    t.string "identity"
    t.boolean "success"
    t.string "failure_reason"
    t.string "user_type"
    t.bigint "user_id"
    t.string "context"
    t.string "ip"
    t.text "user_agent"
    t.text "referrer"
    t.string "city"
    t.string "region"
    t.string "country"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at"
    t.index ["identity"], name: "index_login_activities_on_identity"
    t.index ["ip"], name: "index_login_activities_on_ip"
    t.index ["user_type", "user_id"], name: "index_login_activities_on_user_type_and_user_id"
  end

  create_table "measurements", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.string "datatype"
    t.string "datasource"
    t.datetime "gathered_at"
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_measurements_on_user_id"
  end

  create_table "nicknames", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "nickname"
    t.string "nicknameable_type"
    t.uuid "nicknameable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nicknameable_type", "nicknameable_id"], name: "index_nicknames_on_nicknameable_type_and_nicknameable_id"
  end

  create_table "notifications", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.integer "notification_type"
    t.string "title"
    t.string "message"
    t.string "payload_json"
    t.datetime "deliver_at"
    t.datetime "delivered_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "send_condition"
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "oauth_providers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "base_uri"
    t.string "scope"
    t.string "custom_url"
    t.string "redirect_uri"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "organizations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "nationality"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "timezone"
  end

  create_table "organizations_users", id: false, force: :cascade do |t|
    t.uuid "organization_id", null: false
    t.uuid "user_id", null: false
    t.index ["organization_id", "user_id"], name: "index_organizations_users_on_organization_id_and_user_id"
    t.index ["user_id", "organization_id"], name: "index_organizations_users_on_user_id_and_organization_id"
  end

  create_table "people", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "prefix", limit: 10
    t.string "fname"
    t.string "minitial"
    t.string "lname"
    t.string "suffix", limit: 10
    t.string "birthdate"
    t.string "nationality"
    t.string "language", limit: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "phone_events", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "device_id"
    t.datetime "event_at"
    t.string "relative_location"
    t.string "event_type"
    t.string "event_data", limit: 2048
    t.float "battery_level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["device_id"], name: "index_phone_events_on_device_id"
  end

  create_table "phones", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "label_id"
    t.string "phoneable_type"
    t.uuid "phoneable_id"
    t.string "number"
    t.boolean "confirmed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["label_id"], name: "index_phones_on_label_id"
    t.index ["phoneable_type", "phoneable_id"], name: "index_phones_on_phoneable_type_and_phoneable_id"
  end

  create_table "point_rewards", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.datetime "awarded_at"
    t.integer "points"
    t.string "trigger"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_point_rewards_on_user_id"
  end

  create_table "properties", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.string "owner_type"
    t.uuid "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_type", "owner_id"], name: "index_properties_on_owner_type_and_owner_id"
  end

  create_table "question_options", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "question_id"
    t.string "language"
    t.integer "position"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_question_options_on_question_id"
  end

  create_table "questions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "questionable_type"
    t.uuid "questionable_id"
    t.string "title"
    t.integer "position"
    t.string "language"
    t.string "question_type"
    t.string "correct_response"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["questionable_type", "questionable_id"], name: "index_questions_on_questionable_type_and_questionable_id"
  end

  create_table "redirect_details", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "screen_detail_id"
    t.string "success_url"
    t.string "failure_url"
    t.string "failure_msg"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["screen_detail_id"], name: "index_redirect_details_on_screen_detail_id"
  end

  create_table "rosters", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "phone"
    t.string "site"
    t.string "empatica_id"
    t.string "ibdoc_id"
    t.string "bodyport_id"
    t.string "pooply_id"
    t.string "codename"
    t.string "after_phone_validation", default: "consent-summary-screens"
    t.string "after_email_validation"
    t.boolean "is_test_user", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "participant_id"
    t.float "compliance"
    t.float "compliance_last_week"
    t.datetime "compliance_computed_at"
    t.string "camcog_subject_id"
    t.string "camcog_access_code"
  end

  create_table "screen_details", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "screen_id"
    t.integer "position"
    t.string "title"
    t.string "detail_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["screen_id"], name: "index_screen_details_on_screen_id"
  end

  create_table "screens", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "screenable_type"
    t.uuid "screenable_id"
    t.string "value"
    t.integer "position"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["screenable_type", "screenable_id"], name: "index_screens_on_screenable_type_and_screenable_id"
  end

  create_table "surveys", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "surveyable_type"
    t.uuid "surveyable_id"
    t.string "name"
    t.string "title"
    t.string "format"
    t.string "delivery"
    t.integer "first_study_day"
    t.boolean "is_persistent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["surveyable_type", "surveyable_id"], name: "index_surveys_on_surveyable_type_and_surveyable_id"
  end

  create_table "targets", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "target_source_type"
    t.uuid "target_source_id"
    t.string "target_dest_type"
    t.uuid "target_dest_id"
    t.string "criteria"
    t.float "min"
    t.float "max"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["target_dest_type", "target_dest_id"], name: "index_targets_on_target_dest_type_and_target_dest_id"
    t.index ["target_source_type", "target_source_id"], name: "index_targets_on_target_source_type_and_target_source_id"
  end

  create_table "task_details", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "task_id"
    t.string "name"
    t.integer "position"
    t.string "task_type"
    t.string "format"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["task_id"], name: "index_task_details_on_task_id"
  end

  create_table "tasks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "taskable_type"
    t.uuid "taskable_id"
    t.integer "position"
    t.string "name"
    t.string "delivery"
    t.integer "first_study_day"
    t.boolean "is_persistent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["taskable_type", "taskable_id"], name: "index_tasks_on_taskable_type_and_taskable_id"
  end

  create_table "tile_schedules", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "tile_id"
    t.integer "study_day"
    t.datetime "active_at"
    t.datetime "expires_at"
    t.datetime "reminded_at"
    t.datetime "last_notified_at"
    t.datetime "completed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tile_id"], name: "index_tile_schedules_on_tile_id"
    t.index ["user_id"], name: "index_tile_schedules_on_user_id"
  end

  create_table "tiles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "tileable_type"
    t.uuid "tileable_id"
    t.string "name"
    t.string "tile_type"
    t.string "referenced"
    t.string "referenced_type"
    t.string "title"
    t.string "icon"
    t.integer "position"
    t.string "style_class"
    t.string "delivery"
    t.integer "first_study_day"
    t.boolean "is_persistent", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tileable_type", "tileable_id"], name: "index_tiles_on_tileable_type_and_tileable_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "person_id"
    t.boolean "validated", default: false
    t.boolean "gave_full_consent"
    t.datetime "study_started_at"
    t.datetime "study_ended_at"
    t.string "status"
    t.boolean "is_test_user", default: false
    t.integer "delivery_preference", default: 1
    t.datetime "last_fed_at"
    t.integer "notification_badge_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "timezone"
    t.index ["person_id"], name: "index_users_on_person_id"
  end

  create_table "validation_codes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code"
    t.uuid "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_validation_codes_on_user_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.uuid "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "video_diaries", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.string "filename"
    t.string "bucket"
    t.string "region"
    t.datetime "started_at"
    t.datetime "completed_at"
    t.integer "size"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_video_diaries_on_user_id"
  end

  add_foreign_key "access_tokens", "oauth_providers"
  add_foreign_key "access_tokens", "users"
  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "addresses", "labels"
  add_foreign_key "answers", "questions"
  add_foreign_key "answers", "users"
  add_foreign_key "app_section_infos", "app_sections"
  add_foreign_key "buttons_tasks", "buttons"
  add_foreign_key "buttons_tasks", "tasks"
  add_foreign_key "completed_tasks", "tasks"
  add_foreign_key "completed_tasks", "users"
  add_foreign_key "consent_section_infos", "consent_sections"
  add_foreign_key "content_cases", "content_switches"
  add_foreign_key "devices", "users"
  add_foreign_key "emails", "labels"
  add_foreign_key "failed_tokens", "oauth_providers"
  add_foreign_key "failed_tokens", "users"
  add_foreign_key "faq_questions", "faq_categories"
  add_foreign_key "measurements", "users"
  add_foreign_key "notifications", "users"
  add_foreign_key "phone_events", "devices"
  add_foreign_key "phones", "labels"
  add_foreign_key "point_rewards", "users"
  add_foreign_key "question_options", "questions"
  add_foreign_key "redirect_details", "screen_details"
  add_foreign_key "rosters", "users", column: "participant_id"
  add_foreign_key "screen_details", "screens"
  add_foreign_key "task_details", "tasks"
  add_foreign_key "tile_schedules", "tiles"
  add_foreign_key "tile_schedules", "users"
  add_foreign_key "users", "people"
  add_foreign_key "validation_codes", "users"
  add_foreign_key "video_diaries", "users"
end
