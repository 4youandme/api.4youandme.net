class CreateRedirectDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :redirect_details, id: :uuid do |t|
      t.references :screen_detail, type: :uuid, foreign_key: true
      t.string :success_url
      t.string :failure_url
      t.string :failure_msg

      t.timestamps
    end
  end
end
