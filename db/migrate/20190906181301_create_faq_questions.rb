class CreateFaqQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :faq_questions, id: :uuid do |t|
      t.references :faq_category, foreign_key: true, type: :uuid
      t.string :language, default: "en"
      t.integer :position
      t.boolean :visible, default: true
      t.string :question
      t.string :answer

      t.timestamps
    end
  end
end
