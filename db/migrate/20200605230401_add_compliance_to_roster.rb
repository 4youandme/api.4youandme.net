class AddComplianceToRoster < ActiveRecord::Migration[5.2]
  def change
    add_column :rosters, :compliance, :float
    add_column :rosters, :compliance_last_week, :float
    add_column :rosters, :compliance_computed_at, :datetime
  end
end
