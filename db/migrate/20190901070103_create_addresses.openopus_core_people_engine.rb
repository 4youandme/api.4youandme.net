# This migration comes from openopus_core_people_engine (originally 2)
class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses, id: :uuid do |t|
      t.references :label, foreign_key: true, type: :uuid
      t.references :addressable, polymorphic: true, type: :uuid
      t.string :line1
      t.string :line2
      t.string :city
      t.string :state
      t.string :postal
      t.string :country
      t.float :lat
      t.float :lon
      t.boolean :confirmed

      t.timestamps
    end
  end
end
