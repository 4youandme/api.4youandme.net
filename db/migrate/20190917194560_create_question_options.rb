class CreateQuestionOptions < ActiveRecord::Migration[5.2]
  def change
    create_table :question_options, id: :uuid do |t|
      t.references :question, type: :uuid, foreign_key: true
      t.string :language
      t.integer :position
      t.string :value

      t.timestamps
    end
  end
end
