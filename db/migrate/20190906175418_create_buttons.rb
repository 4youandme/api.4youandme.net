class CreateButtons < ActiveRecord::Migration[5.2]
  def change
    create_table :buttons, id: :uuid do |t|
      t.references :buttonable, type: :uuid, polymorphic: true
      t.integer :position
      t.string :tag
      t.string :label
      t.string :target

      t.timestamps
    end
  end
end
