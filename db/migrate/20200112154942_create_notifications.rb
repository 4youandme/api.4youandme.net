class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications, id: :uuid do |t|
      t.references :user, type: :uuid, foreign_key: true
      t.integer :notification_type
      t.string :title
      t.string :message
      t.string :payload_json
      t.datetime :deliver_at
      t.datetime :delivered_at

      t.timestamps
    end
  end
end
