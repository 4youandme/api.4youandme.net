class CreateTargets < ActiveRecord::Migration[5.2]
  def change
    create_table :targets, id: :uuid do |t|
      t.references :target_source, type: :uuid, polymorphic: true
      t.references :target_dest, type: :uuid, polymorphic: true
      t.string :criteria
      t.float :min
      t.float :max

      t.timestamps
    end
  end
end
