# This migration comes from openopus_core_people_engine (originally 3)
class CreatePhones < ActiveRecord::Migration[5.2]
  def change
    create_table :phones, id: :uuid do |t|
      t.references :label, foreign_key: true, type: :uuid
      t.references :phoneable, polymorphic: true, type: :uuid
      t.string :number
      t.boolean :confirmed

      t.timestamps
    end
  end
end
