class MakeConsentSectionsSectionable < ActiveRecord::Migration[5.2]
  def up
    change_table :consent_sections do |t|
      t.references :sectionable, type: :uuid, polymorphic: true
    end
  end

  def down
    change_table :consent_sections do |t|
      t.remove_references :sectionable, polymorphic: true
    end
  end
end
