class CreateAppSections < ActiveRecord::Migration[5.2]
  def change
    create_table :app_sections, id: :uuid do |t|
      t.string :name
      t.integer :position
      t.string :group

      t.timestamps
    end
  end
end
