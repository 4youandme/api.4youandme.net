class CreateFailedTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :failed_tokens, id: :uuid do |t|
      t.string :token
      t.references :user, type: :uuid, foreign_key: true
      t.references :oauth_provider, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
