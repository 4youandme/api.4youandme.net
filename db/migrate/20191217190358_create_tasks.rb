class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks, id: :uuid do |t|
      t.references :taskable, type: :uuid, polymorphic: true
      t.integer :position
      t.string :name
      t.string :delivery
      t.integer :first_study_day
      t.boolean :is_persistent
      t.timestamps
    end
  end
end
