class CreateConsentSectionInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :consent_section_infos, id: :uuid do |t|
      t.references :consent_section, type: :uuid, foreign_key: true
      t.string :name
      t.string :title
      t.string :section_type
      t.integer :position
      t.string :format

      t.timestamps
    end
  end
end
