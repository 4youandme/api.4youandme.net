class CreateAccessTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :access_tokens, id: :uuid do |t|
      t.references :user, type: :uuid, foreign_key: true
      t.references :oauth_provider, type: :uuid, foreign_key: true
      t.string :token
      t.datetime :expires_at

      t.timestamps
    end
  end
end
