class CreateTileSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :tile_schedules, id: :uuid do |t|
      t.references :user, type: :uuid, foreign_key: true
      t.references :tile, type: :uuid, foreign_key: true
      t.integer :study_day
      t.datetime :active_at
      t.datetime :expires_at
      t.datetime :reminded_at
      t.datetime :last_notified_at
      t.datetime :completed_at

      t.timestamps
    end
  end
end
