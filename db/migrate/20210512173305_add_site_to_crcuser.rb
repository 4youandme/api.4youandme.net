class AddSiteToCrcuser < ActiveRecord::Migration[5.2]
  def change
    add_column :crc_users, :site, :string

    CrcUser.where(superuser: false).update_all(site: 'MSSM')
  end
end
