class AddTileScheduleRefToCompletedTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :completed_tasks, :tile_schedule_id, :uuid
  end
end
