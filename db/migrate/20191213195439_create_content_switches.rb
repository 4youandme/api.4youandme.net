class CreateContentSwitches < ActiveRecord::Migration[5.2]
  def change
    create_table :content_switches, id: :uuid do |t|
      t.references :switchable, type: :uuid, polymorphic: true
      t.string :tag

      t.timestamps
    end
  end
end
