class CreatePointRewards < ActiveRecord::Migration[5.2]
  def change
    create_table :point_rewards, id: :uuid do |t|
      t.references :user, type: :uuid, foreign_key: true
      t.datetime :awarded_at
      t.integer :points
      t.string :trigger

      t.timestamps
    end
  end
end
