class CreateFaqCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :faq_categories, id: :uuid do |t|
      t.string :name
      t.string :language, default: "en"
      t.integer :position
      t.boolean :visible, default: true

      t.timestamps
    end
  end
end
