class CreateCrcUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :crc_users, id: :uuid do |t|
      t.string :username

      t.timestamps
    end
  end
end
