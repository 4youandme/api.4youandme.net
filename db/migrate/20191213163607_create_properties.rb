class CreateProperties < ActiveRecord::Migration[5.2]
  def change
    create_table :properties, id: :uuid do |t|
      t.string :name
      t.string :value
      t.references :owner, type: :uuid, polymorphic: true

      t.timestamps
    end
  end
end
