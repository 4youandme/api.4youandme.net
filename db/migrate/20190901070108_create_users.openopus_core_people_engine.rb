# This migration comes from openopus_core_people_engine (originally 7)
class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users, id: :uuid do |t|
      t.references :person, foreign_key: true, type: :uuid
      t.boolean :validated, default: false
      t.boolean :gave_full_consent
      t.datetime :study_started_at
      t.datetime :study_ended_at
      t.string :status
      t.boolean :is_test_user, default: false
      t.integer :delivery_preference, default: 1
      t.datetime :last_fed_at
      t.integer :notification_badge_count, default: 0
      t.timestamps
    end

    create_join_table :organizations, :users, column_options: {type: :uuid} do |t|
      t.index [:organization_id, :user_id]
      t.index [:user_id, :organization_id]
    end
  end
end
