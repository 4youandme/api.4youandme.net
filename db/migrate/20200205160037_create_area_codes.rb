class CreateAreaCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :area_codes, id: :uuid do |t|
      t.integer :code
      t.string :timezone
      t.string :location
      t.date :assigned_at

      t.timestamps
    end
  end
end
