class JoinButtonsTasks < ActiveRecord::Migration[5.2]
  def change
    create_join_table :buttons, :tasks do |t|
      t.references :button, type: :uuid, foreign_key: true
      t.references :task, type: :uuid, foreign_key: true
    end
  end
end
