class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers, id: :uuid do |t|
      t.references :user, type: :uuid, foreign_key: true
      t.references :question, type: :uuid, foreign_key: true
      t.string :answer

      t.timestamps
    end
  end
end
