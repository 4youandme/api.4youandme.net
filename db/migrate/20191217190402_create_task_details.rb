class CreateTaskDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :task_details, id: :uuid do |t|
      t.references :task, type: :uuid, foreign_key: true
      t.string :name
      t.integer :position
      t.string :task_type
      t.string :format
      t.string :title

      t.timestamps
    end
  end
end
