class CreateVideoDiaries < ActiveRecord::Migration[5.2]
  def change
    create_table :video_diaries, id: :uuid do |t|
      t.references :user, type: :uuid, foreign_key: true
      t.string :filename
      t.string :bucket
      t.string :region
      t.datetime :started_at
      t.datetime :completed_at
      t.integer :size

      t.timestamps
    end
  end
end
