# This migration comes from openopus_core_people_engine (originally 1)
class CreateNicknames < ActiveRecord::Migration[5.2]
  def change
    create_table :nicknames, id: :uuid do |t|
      t.string :nickname
      t.references :nicknameable, polymorphic: true, type: :uuid
      t.timestamps
    end
  end
end
