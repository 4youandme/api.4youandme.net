class CreateOauthProviders < ActiveRecord::Migration[5.2]
  def change
    create_table :oauth_providers, id: :uuid do |t|
      t.string :name
      t.string :base_uri
      t.string :scope           # A string specifying to the provider the data types that we want access to, like "email,phone"
      t.string :custom_url      # Don't know why it's called this.  It's the URL that open the associated app on the iphone
      t.string :redirect_uri

      t.timestamps
    end
  end
end
