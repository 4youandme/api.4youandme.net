class CreateValidationCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :validation_codes, id: :uuid do |t|
      t.string :code
      t.references :user, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
