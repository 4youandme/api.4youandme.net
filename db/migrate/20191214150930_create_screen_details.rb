class CreateScreenDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :screen_details, id: :uuid do |t|
      t.references :screen, type: :uuid, foreign_key: true
      t.integer :position
      t.string :title
      t.string :detail_type

      t.timestamps
    end
  end
end
