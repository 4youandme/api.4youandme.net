# This migration comes from openopus_core_people_engine (originally 4)
class CreateEmails < ActiveRecord::Migration[5.2]
  def change
    create_table :emails, id: :uuid do |t|
      t.references :label, foreign_key: true, type: :uuid
      t.references :emailable, polymorphic: true, type: :uuid
      t.string :address
      t.string :confirmation_code, default: nil
      t.boolean :confirmed
      t.datetime :confirmed_at

      t.timestamps
    end
  end
end
