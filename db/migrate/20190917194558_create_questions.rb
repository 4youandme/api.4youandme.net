class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions, id: :uuid do |t|
      t.references :questionable, type: :uuid, polymorphic: true
      t.string :title
      t.integer :position
      t.string :language
      t.string :question_type
      t.string :correct_response                # Some questions have a right answer.
      t.string :value                           # Don't know why, by Norah Jones.

      t.timestamps
    end
  end
end
