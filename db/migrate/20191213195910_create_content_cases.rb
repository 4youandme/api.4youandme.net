class CreateContentCases < ActiveRecord::Migration[5.2]
  def change
    create_table :content_cases, id: :uuid do |t|
      t.references :content_switch, type: :uuid, foreign_key: true
      t.string :match

      t.timestamps
    end
  end
end
