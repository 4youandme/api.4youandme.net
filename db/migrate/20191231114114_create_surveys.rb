class CreateSurveys < ActiveRecord::Migration[5.2]
  def change
    create_table :surveys, id: :uuid do |t|
      t.references :surveyable, type: :uuid, polymorphic: true
      t.string :name
      t.string :title
      t.string :format
      t.string :delivery
      t.integer :first_study_day
      t.boolean :is_persistent
      
      t.timestamps
    end
  end
end
