class CreateMeasurements < ActiveRecord::Migration[5.2]
  def change
    create_table :measurements, id: :uuid do |t|
      t.references :user, type: :uuid, foreign_key: true
      t.string :datatype
      t.string :datasource
      t.datetime :gathered_at
      t.decimal :value

      t.timestamps
    end
  end
end
