class AddCamcogToRosters < ActiveRecord::Migration[5.2]
  def change
    add_column :rosters, :camcog_subject_id, :string
    add_column :rosters, :camcog_access_code, :string
  end
end
