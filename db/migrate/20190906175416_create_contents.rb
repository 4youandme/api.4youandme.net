class CreateContents < ActiveRecord::Migration[5.2]
  def change
    create_table :contents, id: :uuid do |t|
      t.references :contentable, type: :uuid, polymorphic: true
      t.string :tag
      t.text :content
      t.string :content_type
      t.string :style_class

      t.timestamps
    end
  end
end
