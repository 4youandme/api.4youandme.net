# This migration comes from openopus_core_people_engine (originally 6)
class CreateOrganizations < ActiveRecord::Migration[5.2]
  def change
    create_table :organizations, id: :uuid do |t|
      t.string :name
      t.string :nationality

      t.timestamps
    end
  end
end
