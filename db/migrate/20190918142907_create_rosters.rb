class CreateRosters < ActiveRecord::Migration[5.2]
  def change
    create_table :rosters, id: :uuid do |t|
      t.string :phone
      t.string :site

      # These are specific the this study.  We would normally have this information
      # be stored in dynamic table, but for ease-of-use building the tool for the CRC
      # we have placed them right here.
      t.string :empatica_id
      t.string :ibdoc_id
      t.string :bodyport_id
      t.string :pooply_id

      t.string :codename
      t.string :after_phone_validation, default: "consent-summary-screens"
      t.string :after_email_validation, default: nil
      t.boolean :is_test_user, default: false

      t.timestamps
    end
  end
end
