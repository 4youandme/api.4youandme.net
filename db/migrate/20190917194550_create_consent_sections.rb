class CreateConsentSections < ActiveRecord::Migration[5.2]
  def change
    create_table :consent_sections, id: :uuid do |t|
      t.string :name
      t.integer :position
      t.boolean :gatekeeper, default: false
      t.timestamps
    end
  end
end
