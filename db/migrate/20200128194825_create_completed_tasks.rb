class CreateCompletedTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :completed_tasks, id: :uuid do |t|
      t.references :task, type: :uuid, foreign_key: true
      t.references :user, type: :uuid, foreign_key: true
      t.string :data_location, length: 1023
      t.string :task_type
      t.integer :study_day
      t.datetime :started_at
      t.datetime :completed_at

      t.timestamps
    end
  end
end
