class CreateDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :devices, id: :uuid do |t|
      t.references :user, foreign_key: true, type: :uuid
      t.string :name
      t.string :push_token
      t.string :push_token_choice_status
      t.string :api_token
      t.string :device_type
      t.string :device_os
      t.string :dimensions
      t.string :resolution

      t.timestamps
    end
  end
end
