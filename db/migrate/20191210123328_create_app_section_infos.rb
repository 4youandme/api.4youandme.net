class CreateAppSectionInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :app_section_infos, id: :uuid do |t|
      t.references :app_section, type: :uuid, foreign_key: true
      t.string :name
      t.integer :position
      t.string :title
      t.string :section_type
      t.string :format

      t.timestamps
    end
  end
end
