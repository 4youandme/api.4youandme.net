class AddUserIdToRoster < ActiveRecord::Migration[5.2]
  def change
    add_column :rosters, :participant_id, :uuid, index: true
    add_foreign_key :rosters, :users, column: :participant_id
  end
end
