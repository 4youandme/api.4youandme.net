class CreateTiles < ActiveRecord::Migration[5.2]
  def change
    create_table :tiles, id: :uuid do |t|
      t.references :tileable, type: :uuid, polymorphic: true
      t.string :name
      t.string :tile_type
      t.string :referenced
      t.string :referenced_type
      t.string :title
      t.string :icon
      t.integer :position
      t.string :style_class
      t.string :delivery
      t.integer :first_study_day
      t.boolean :is_persistent, default: false
      
      t.timestamps
    end
  end
end
