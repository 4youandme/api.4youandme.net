class CreateScreens < ActiveRecord::Migration[5.2]
  def change
    create_table :screens, id: :uuid do |t|
      t.references :screenable, type: :uuid, polymorphic: true
      t.string :value
      t.integer :position
      t.string :title

      t.timestamps
    end
  end
end
