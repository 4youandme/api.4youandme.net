class AddSuperuserToCrcUser < ActiveRecord::Migration[5.2]
  def change
    add_column :crc_users, :superuser, :boolean
  end
end
