class AddRefreshTokenToAccessTokens < ActiveRecord::Migration[5.2]
  def change
    add_column :access_tokens, :refresh_token, :string
  end
end
