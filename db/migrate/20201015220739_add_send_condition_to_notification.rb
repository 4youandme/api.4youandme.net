class AddSendConditionToNotification < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :send_condition, :string
  end
end