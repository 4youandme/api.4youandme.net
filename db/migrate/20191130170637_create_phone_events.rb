class CreatePhoneEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :phone_events, id: :uuid do |t|
      t.references :device, type: :uuid, foreign_key: true
      t.datetime :event_at
      t.string :relative_location
      t.string :event_type
      t.string :event_data, limit: 2048
      t.float :battery_level

      t.timestamps
    end
  end
end
