class CreateAuditLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :audit_logs, id: :uuid do |t|
      t.string :who
      t.string :what
      t.string :which
      t.string :when
      t.string :ip_address

      t.timestamps
    end
  end
end
