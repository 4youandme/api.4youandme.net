# This migration comes from openopus_core_people_engine (originally 0)
class CreateLabels < ActiveRecord::Migration[5.2]
  def change
    create_table :labels, id: :uuid do |t|
      t.string :value
      t.string :lang

      t.timestamps
    end
  end
end
