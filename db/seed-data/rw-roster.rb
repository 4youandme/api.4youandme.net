# bfox-roster.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Wed Feb  5 12:36:31 2020.
print "\nCreating Roster entry for Ryan Weiss at 609.425.4382..."
phone = Phone.canonicalize("6094254382")
rost = Roster.where(phone: phone).first_or_create(is_test_user: false, site: "MSSM")
