# faqs.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Wed Nov 27 05:39:14 2019.
math = FaqCategory.where(name: "Math Comprehension").first_or_create(position: 1)
tech = FaqCategory.where(name: "Tech Understanding").first_or_create(position: 2)
show = FaqCategory.where(name: "About Succession").first_or_create(position: 3)

math.questions.where(question: "What is addition?").first_or_create(position: 1,
  answer: "the process or skill of calculating the total of two or more numbers or amounts. 'She began with simple arithmetic, addition and then subtraction'")
math.questions.where(question: "What is calculus?").first_or_create(position: 2,
  answer: "the branch of mathematics that deals with the finding and properties of derivatives and integrals of functions, by methods originally based on the summation of infinitesimal differences. The two main types are differential calculus and integral calculus.")

tech.questions.where(question: "What is Python?").first_or_create(position: 1,
  answer: "A relative programming language in wide use.")
tech.questions.where(question: "How was this API implemented?").first_or_create(position: 2,
  answer: "bfox wrote it in rails in a few hours.")

show.questions.where(question: "Who are the Succession children, from oldest to youngest?").first_or_create(position: 1,
  answer: "Connor, Kendall, Roman, and Shiv.")
