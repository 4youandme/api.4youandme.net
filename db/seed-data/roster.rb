# roster.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Wed Sep 18 07:55:18 2019.

# The real roster goes here.  The Test Roster is in test-user.rb
# Roster.where(phone: '+442079460444', site: 'site112').first_or_create

# Elisabeth Kipping
# Roster.where(phone: "+1 (908) 303-4892", site: "MSSM").first_or_create
