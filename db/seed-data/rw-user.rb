# bfox-user.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2019 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Dec 10 21:29:34 2019.

# Here's a roster entry who will actually receive a text with a validation code,
# but whose associated user will get test data.
print "\nCreating rw..."
phone = Phone.canonicalize("6094254382")
rost = Roster.where(phone: phone).first_or_create(is_test_user: false, site: "rw3iss")
rw = Person.where(fname: "Ryan", minitial: "S", lname: "Weiss").first_or_create(birthdate: "1986-04-20", phone: phone)
user = User.where(person: rw).first_or_create(is_test_user: rost.is_test_user)
# user.devices.create(name: "rw Test Device", device_type: "Not iPhone", device_os: "Not iOS")
