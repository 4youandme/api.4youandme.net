# evidation-rosters.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Thu Feb 20 16:45:55 2020.

# Here's a roster entry who will actually receive a text with a validation code,
# but whose associated user will get test data.
print "\nCreating Mackenzie..."
phone = Phone.canonicalize("3038091649")
rost = Roster.where(phone: phone).first_or_create(is_test_user: false, site: "MSSM")
mack = Person.where(fname: "Mackenzie", lname: "Wildman").first_or_create(birthdate: "1986-12-11", phone: phone)
user = User.where(person: mack).first_or_create(is_test_user: true)
user.devices.create(name: "Mack's Test Device", device_type: "Not iPhone", device_os: "Not iOS")
print "\n  Loading measurement data..."
user.load_measurements_from_file("app/views/static/your-data.json")

print "\nCreating Luca Foschini"
phone = Phone.canonicalize("8054525857")
rost = Roster.where(phone: phone).first_or_create(is_test_user: false, site: "MSSM")

print "\nCreating Elisabeth Kippling"
phone = Phone.canonicalize("(908) 303-4892")
rost = Roster.where(phone: phone).first_or_create(is_test_user: false, site: "MSSM")
