# providers.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Nov 26 15:00:36 2019.
#
official_api = "https://api.4youandme.net"
staging_api  = "https://api-staging.4youandme.dev"
callback_base= "#{official_api}/app/api/v1/callback"

rescuetime = OauthProvider.where(name: "RescueTime").first_or_create(
  base_uri: "https://www.rescuetime.com",
  scope: "time_data",
  redirect_uri: "#{callback_base}/rescuetime",
  custom_url: ""
)
rescuetime.use_form_post = true
rescuetime.save

pooply = OauthProvider.where(name: "Pooply").first_or_create(
  base_uri: "https://pooply.eu.api.healthmode.com" ,
  scope: "",
  redirect_uri: "#{callback_base}/pooply",
  custom_url: "healthmodestool://"
)
pooply.use_form_post = true
pooply.token_uri = pooply.base_uri + "/oauth/token/"
pooply.save

oura = OauthProvider.where(name: "Oura").first_or_create(
  base_uri: "https://cloud.ouraring.com",
  scope: "daily",
  redirect_uri: "#{callback_base}/oura",
  custom_url: "oura://"
)

# oura.oauth_debug = true
# oura.use_basic_auth = true
oura.token_uri = "https://api.ouraring.com/oauth/token"
oura.use_form_post = true
oura.save
