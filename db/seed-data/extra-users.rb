CrcUser.where(username: "crc@4youandme.net").first_or_create(email: "crc@4youandme.net", password: "rambo")

phones = ['+15058012345', '+15058012346', '+15058012347', '+15058012348', '+15058012349',
          '+442079460111', '+442079460222', '+442079460333', '+442079460444', '+442079460555']
sites =  ['MSSM', 'Oxford']
phones.each { |phone| Roster.where(phone: Phone.canonicalize(phone)).first_or_create(site: sites.sample, is_test_user: true) }

print "\nCreating Johnny Appleseed test user..."
phone = "+1888-888-8888"
Roster.where(phone: Phone.canonicalize(phone)).first_or_create(site: "Oxford", is_test_user: true)
apple = Person.where(fname: "Johnny", minitial: "Q", lname: "Appleseed").first_or_create(birthdate: "1976-04-01", phone: phone, email: "jappleseed@4youandme.dev")
user  = User.where(person: apple, is_test_user: true).first_or_create
user.devices.create(name: "Apple Test Device", device_type: "Not iPhone", device_os: "Not iOS")

# Also, another special case for Johnny Appleseed.  Instead of dynamically generating
# a confirmation code for this user, set it now, in advance, to '12345'.
e = user.email
e.confirmation_code = "12345"
e.save

print "\n  Creating measurement data..."
user.make_more_measurements(DateTime.now - 1.year, DateTime.now + 1.month)

print "\n  Creating feed tiles..."
file = "app/views/static/feed.json"
feed_json = JSON.parse(File.read(file))
feed_hash = ActiveSupport::HashWithIndifferentAccess.new(feed_json)
feed_hash[:feed_sections].each_with_index {|fs, pos| user.feed_sections << FeedSection.from_hash(fs, pos) }
user.save

print "\nCreating MSSM test user..."
phone = "+1 234-567-8901"
Roster.where(phone: phone).first_or_create(site: "MSSM", is_test_user: true, after_phone_validation: "appsConnection", after_email_validation: nil)
person = Person.where(fname: "Mount", minitial: "Q", lname: "Sinai").first_or_create(birthdate: "1976-04-01", phone: phone)
user  = User.where(person: person, is_test_user: true).first_or_create
user.devices.create(name: "MSSM Test Device", device_type: "Not iPhone", device_os: "Not iOS")
print "\n  Creating feed tiles..."
feed_hash[:feed_sections].each_with_index {|fs, pos| user.feed_sections << FeedSection.from_hash(fs, pos) }
