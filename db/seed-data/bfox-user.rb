# bfox-user.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2019 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Dec 10 21:29:34 2019.

# Here's a roster entry who will actually receive a text with a validation code,
# but whose associated user will get test data.
print "\nCreating bfox..."
phone = Phone.canonicalize("8056378642")
rost = Roster.where(phone: phone).first_or_create(is_test_user: false, site: "bfox")
bfox = Person.where(fname: "Brian", minitial: "J", lname: "Fox").first_or_create(birthdate: "1959-12-11", phone: phone)
user = User.where(person: bfox).first_or_create(is_test_user: rost.is_test_user)
# user.devices.create(name: "BFOX Test Device", device_type: "Not iPhone", device_os: "Not iOS")
