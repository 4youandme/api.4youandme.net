# surveys.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Jan  7 12:02:47 2020.
files = Dir[Rails.root.join("db/seed-data/static/surveys/*.json")]
print "\n["
files.each_with_index do |file, idx|
  print ", " if idx != 0
  print "#{File.basename(file)}"
  json = ActiveSupport::HashWithIndifferentAccess.new(JSON.parse(File.read(file)))
  survey = Survey.where(name: json[:name]).first if json[:name]
  survey.rename_to(survey.name + "-#{survey.created_at}") if survey
  survey = Survey.from_hash(json)
  survey.save or raise("Couldn't parse survey from hash: #{file}")
end
print "]"
print "\n   Validating targets..."
props_to_delete = []
Target.all.each do |target|
  target_prop = target.properties.where(name: "targeted_destination").first
  target_id = target_prop.value if target_prop
  next if not target_id
  destination_prop = Property.where(name: "destination_id", value: target_id).first
  destination = destination_prop.owner if destination_prop
  if not destination and not target_id == "exit"
    print "\n   ERROR IN TARGET DEFINITION: No mapping for `#{target_id}` found in Questions"
    next
  else
    if target_id == "exit"
      print "\n   Matched EXIT as target of #{target.target_source_type}:#{target.target_source_id}"
    else
      print "\n   Matched #{destination.class.to_s}:#{destination.id} as target of #{target.target_source_type}:#{target.target_source_id}"
    end
  end

  target.target_dest = destination
  target.save or debugger
end

# No longer need "destination_id" or "targeted_destination" properties.  Flush them.
Property.where(name: ['destination_id', 'targeted_destination']).destroy_all
