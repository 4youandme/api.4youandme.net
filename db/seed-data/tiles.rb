# tiles.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Jan 10 08:06:46 2020.

files = Dir[Rails.root.join("db/seed-data/static/tiles/*.json")]
print "\n["
files.each_with_index do |file, idx|
  print ", " if idx != 0
  print "#{File.basename(file)}"
  Tile.import_and_replace(file, nil, rename: true)
end
print "]"


