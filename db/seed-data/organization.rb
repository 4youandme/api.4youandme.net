# organization.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Wed Feb 12 13:20:33 2020.

dflt = Organization.where(name: "Default").first_or_create
dflt.nicknames.create(nickname: "dflt")
dflt.phone = "8055551212"
dflt.save

mssm = Organization.where(name: "Mount Sinai").first_or_create
mssm.nicknames.where(nickname: "mssm").first_or_create
mssm.phone = "(305) 674-2273"
mssm.save

oxfd = Organization.where(name: "Oxford").first_or_create
oxfd.nicknames.where(nickname: "oxford").first_or_create
oxfd.phone = "+44 (0)1865 289561"
oxfd.save

