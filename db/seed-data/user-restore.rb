# user-restore.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sun Feb 23 08:44:41 2020.
users = JSON.parse(File.read("./live-users.json")) rescue nil
users.each { |u| user = User.from_hash(u); user.save(validate: false) if user rescue nil } if users
User.all.each {|user| user.roster ||= user.roster_by_number}
