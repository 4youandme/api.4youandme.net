# areacodes.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Wed Feb  5 09:36:03 2020.
AreaCode.import("./db/seed-data/static/area-zones.csv")
