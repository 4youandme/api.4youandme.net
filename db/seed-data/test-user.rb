print "\nCreating MSSM test user..."
phone = "+1 234-567-8901"
Roster.where(phone: phone).first_or_create(site: "MSSM", is_test_user: true, after_phone_validation: "appsConnection", after_email_validation: nil)
person = Person.where(fname: "Mount", minitial: "Q", lname: "Sinai").first_or_create(birthdate: "1976-04-01", phone: phone)
user  = User.where(person: person, is_test_user: true).first_or_create
user.devices.create(name: "MSSM Test Device", device_type: "Not iPhone", device_os: "Not iOS")

print "\nCreating all survey test user"
phone = "+1 (666) 666-6666"
Roster.where(phone: phone).first_or_create(site: "Oxford", is_test_user: true, after_phone_validation: "appsConnection",
                                           after_email_validation: nil)
person = Person.where(fname: "Mackenzie's", lname: "Mom").first_or_create(birthdate: "1950-04-01", phone: phone)
user  = User.where(person: person, is_test_user: true).first_or_create
user.devices.create(name: "Mackenzie's QA Mom", device_type: "iPhone", device_os: "iOS")
# Also, another special case for this test user.  Instead of dynamically generating
# a confirmation code for this user, set it now, in advance, to '66666'.
e = user.email
e.confirmation_code = "66666"
e.save
