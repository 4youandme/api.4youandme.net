# yml-test-user.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Thu Feb 13 20:56:51 2020.
print "\nCreating yml..."
phone = Phone.canonicalize("8055551212")
rost = Roster.where(phone: phone).first_or_create(is_test_user: false, site: "mssm")
fml = Person.where(fname: "YMedia", minitial: "Q", lname: "Labs").first_or_create(birthdate: "1965-12-25", phone: phone)
user = User.where(person: fml).first_or_create(is_test_user: rost.is_test_user)

