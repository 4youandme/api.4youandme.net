# administrators.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Wed Nov 27 05:28:03 2019.
Administrator.where(email: "admin@evidation.com").first_or_create(password: "password", password_confirmation: "password")
CrcUser.where(username: "crc@4youandme.net").first_or_create(email: "crc@4youandme.net", password: "rambo")

