# bfox-roster.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Wed Feb  5 12:36:31 2020.
print "\nCreating Roster entry for Brian Fox at 805.637.8642..."
phone = Phone.canonicalize("8056378642")
rost = Roster.where(phone: phone).first_or_create(is_test_user: false, site: "MSSM")
