# appleseed.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Thu Jan 16 19:29:21 2020.
print "\nCreating Johnny Appleseed test user..."
phone = "+1888-888-8888"
r = Roster.where(phone: Phone.canonicalize(phone)).first_or_create(site: "Oxford", is_test_user: true)
apple = Person.where(fname: "Johnny", minitial: "Q", lname: "Appleseed").first_or_create(birthdate: "1976-04-01", phone: phone, email: "jappleseed@4youandme.dev")
user  = User.where(person: apple, is_test_user: true).first_or_create
user.devices.create(name: "Apple Test Device", device_type: "Not iPhone", device_os: "Not iOS")
r.participant = user
r.save

# Also, another special case for Johnny Appleseed.  Instead of dynamically generating
# a confirmation code for this user, set it now, in advance, to '12345'.
e = user.email
e.confirmation_code = "12345"
e.save

print "\n  Creating measurement data..."
user.make_more_measurements(DateTime.now - 1.year, DateTime.now + 1.month)

=begin
print "\n  Creating feed tiles..."
file = "app/views/static/feed.json"
feed_json = JSON.parse(File.read(file))
feed_hash = ActiveSupport::HashWithIndifferentAccess.new(feed_json)
feed_hash[:feed_sections].each_with_index {|fs, pos| user.feed_sections << FeedSection.from_hash(fs, pos) }
user.save
=end
