# consent-sections.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2019 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Mon Dec  9 12:31:35 2019.
dflt = Organization.lookup("Default")
mssm = Organization.lookup("mssm")
oxfd = Organization.lookup("Oxford")

ConsentSection.from_file("app/views/static/consent-sections.json", dflt)
ConsentSection.from_file("app/views/static/consent-sections-oxford.json", oxfd)
ConsentSection.from_file("app/views/static/consent-sections-mssm.json", mssm)
