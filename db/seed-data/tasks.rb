# tasks.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Jan  7 12:36:23 2020.

def one_task(json)
  hash = ActiveSupport::HashWithIndifferentAccess.new(json)
  task = Task.where(name: hash[:name]).first if hash[:name]

  # task.destroy if task
  task.rename_to(task.name + "-#{task.created_at}") if task

  task = Task.from_hash(hash)
  print "(#{task.name})" if task and task.name
  task.save or debugger
  task
end

files = Dir[Rails.root.join("db/seed-data/static/tasks/*.json")]
print "\n["
files.each_with_index do |file, idx|
  print ", " if idx != 0
  print "#{File.basename(file)}"
  json = JSON.parse(File.read(file))
  if json.is_a?(Array)
    json.each {|tjson| one_task(tjson)}
  else
    one_task(json)
  end
end
print "]"


