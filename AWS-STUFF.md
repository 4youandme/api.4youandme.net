# AWS-STUFF.md: -*- Fundamental -*-  DESCRIPTIVE TEXT.
# Author: Brian J. Fox (bfox@opuslogica.com)
# Birthdate: Wed Jan 22 15:14:59 2020.

# Getting to the host
1. Login to the AWS console
1. Go to the EC2 application
1. View the instances
1. Get the IP address of the bastion host from the listing
1. Get the IP address of the API host by clicking that line, and viewing the internal IP below
1. Login using your EVI.key

   ssh -i ~/.ssh/EVI/evi.key -J bastion.4youandme.net 10.0.3.242
