namespace :db do
  desc "Delete existing Oauth Providers from the database"
  task drop_providers: :environment do
    OauthProvider.all.each {|p| p.destroy}
  end

  desc "Reseed Oauth Providers table from db/seed-data/providers.rb"
  task reseed_providers: :environment do
    OauthProvider.all.each {|p| p.destroy}
    require "./db/seed-data/providers.rb"
  end

  desc "Reseed the entire set of surveys and questions from db/seed-data/surveys.rb"
  task reseed_surveys: :environment do
    # We no longer delete all of the surveys first.  Instead, each existing survey
    # is renamed, preserving the integrity of completed_tasks, answers, etc.
    # Survey.all.destroy_all
    require "./db/seed-data/surveys.rb"
  end

  desc "Reseed the entire set of tasks from db/seed-data/tasks.rb"
  task reseed_tasks: :environment do
    # We no longer delete all of the tasks first.  Instead, each existing task
    # is renamed, preserving the integrity of completed_tasks, answers, etc.
    # Task.all.destroy_all
    require "./db/seed-data/tasks.rb"
  end

  desc "Reseed the entire set of tiles from db/seed-data/tiles.rb"
  task reseed_tiles: :environment do
    # We no longer delete all of the tiles first.  Instead, each existing tile
    # is renamed, preserving the integrity of the tile schedules for each user,
    # etc.
    # Tile.all.destroy_all
    require "./db/seed-data/tiles.rb"
  end

  desc "Make a couple of administrator accounts"
  task make_admins: :environment do
    require "./db/seed-data/administrators.rb"
  end

  desc "Save roster, CRC, and user information"
  task save_users: :environment do
    crc_users = CrcUser.order(created_at: :asc).to_json(dump: true)
    roster_users = Roster.order(created_at: :asc).to_json(dump: true)
    live_users = User.order(created_at: :asc).to_json(dump: true)
    File.open("./crc-users.json", "wb") {|f| f.write(crc_users)}
    File.open("./roster-users.json", "wb") {|f| f.write(roster_users)}
    File.open("./live-users.json", "wb") {|f| f.write(live_users)}
  end


  desc "Save admins and CRCs"
  task save_admins_and_crcs: :environment do
    save_crcs
    save_admins
  end

  desc "Load admins and CRCs"
  task load_admins_and_crcs: :environment do
    load_crcs
    load_admins
  end

  desc "Load saved roster, CRC, and user information"
  task load_users: :environment do
    require "./db/seed-data/crc-restore.rb"
    require "./db/seed-data/roster-restore.rb"
    require "./db/seed-data/user-restore.rb"
  end

  desc "Update apps sections from json"
  task update_apps_sections: :environment do
    require "./db/seed-data/app-sections.rb"
  end

  ADMINISTRATOR_DUMP_FILE = "./administrators.json"

  def save_admins
    administrators = Administrator.all.to_json(dump: true,
    only: [:id, :email, :encrypted_password])
    File.open(ADMINISTRATOR_DUMP_FILE, "wb") {|f| f.write(administrators)}
  end

  def load_admins
    administrators = JSON.parse(File.read(ADMINISTRATOR_DUMP_FILE))
    administrators.each do |a|
      begin
        hash = ActiveSupport::HashWithIndifferentAccess.new(a)
        Administrator.create(hash)
      rescue
        puts "Could not create admin #{hash[:email]}; probably already exists"
      end
    end

  end

  def save_crcs
    crc_users = CrcUser.order(created_at: :asc).to_json(dump: true)
    File.open("./crc-users.json", "wb") {|f| f.write(crc_users)}
  end

  def load_crcs
    require "./db/seed-data/crc-restore.rb"
  end
end
