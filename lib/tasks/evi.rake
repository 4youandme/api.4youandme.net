# evi.rake: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Feb 25 17:11:33 2020.
namespace :evi do
  desc "Like a database migration, but better"
  task move_to_gathered_at: :environment do
    ActiveRecord::Migration.add_column :measurements, :gathered_at, :datetime
    Measurement.all.each {|m| m.gathered_at = m.ts; m.save}
    ActiveRecord::Migration.remove_column :measurements, :tz
    ActiveRecord::Migration.remove_column :measurements, :ts
  end

  desc "Oh, pooply has an ID thing"
  task pooply_update: :environment do
    pooply = OauthProvider.lookup("pooply")
    roster = Roster.new
    ActiveRecord::Migration.add_column(:rosters, :pooply_id, :string) if not roster.respond_to?(:pooply_id)
    AccessToken.where(oauth_provider: pooply).each {|token| token.maybe_update_pooply}
  end

  desc "Add bfox (805-637-8642) to the roster"
  task make_bfox: :environment do
    phone = Phone.canonicalize("8056378642")
    rost = Roster.where(phone: phone).first_or_create(is_test_user: false, site: "MSSM")
  end

  desc "Update this survey question"
  task tweak_survey: :environment do
    question = Content.where(content: "Have you ever taken Methotrexate (oral)?").first.contentable
    option = question.options.includes(:contents).where(contents: { content: "No" }).first
    target = Content.where(content: "Have you ever taken Remicade (infliximab)?").first.contentable
    option.targets.where(target_dest: target).first_or_create
  end

  desc "Update Empatica Missing tile information"
  task tweak_empatica_tile: :environment do
    tiles = Tile.where(name: "empatica-missing-data") + Tile.where(name: "empatica-missing-data-copy")
    tiles.each do |tile|
      data = tile.properties.where(name: "contents").first
      obj = JSON.parse(data.value)
      obj["body"] = "Sync now by opening the Mate App or click to visit the FAQ page for help"
      data.value = obj.to_json
      data.save
    end
  end

  desc "Update RescueTime missing data tile information"
  task tweak_rescuetime_tile: :environment do
    tiles = Tile.where(name: "rescuetime-missing-data") + Tile.where(name: "rescuetime-missing-data-copy")
    tiles.each do |tile|
      data = tile.properties.where(name: "contents").first
      obj = JSON.parse(data.value)
      obj["body"] = "Sync now by opening the RescueTime App or click to visit the FAQ page for help"
      data.value = obj.to_json
      data.save
    end
  end

  desc "Fix the expiration dates of cognitive-tasks"
  task fix_cognitive_task_expires_at: :environment do
    TileSchedule.find_named("reaction-time").each do |ts|
      ts.expires_at = ts.active_at + 2.hours
      ts.save
    end
    TileSchedule.find_named("trail-making").each do |ts|
      ts.expires_at = ts.active_at + 2.hours
      ts.save
    end
  end
  
  desc "update existing survey schedules to be available all day"
  task update_survey_schedules: :environment do
    User.find_in_batches do |batch_of_users|
      batch_of_users.each do |user|
        Survey
          .survey_tile_schedules_for_user(user)
          .each(&:update_survey_schedule)
      end
    end

  end

  desc "update existing cognitive tasks to be available all day"
  task update_cognitive_tasks: :environment do
    TileSchedule.cognitive_tasks.all.each do |qa_ts|
      if !qa_ts.active_at.nil?
        qa_ts.active_at = qa_ts.active_at.in_time_zone(qa_ts.user.timezone).beginning_of_day.utc
        qa_ts.expires_at = qa_ts.active_at.in_time_zone(qa_ts.user.timezone).end_of_day.utc

        qa_ts.save
      end
    end

    TileSchedule.find_named('camcog-ebt').each do |qa_ts|
      if !qa_ts.active_at.nil?
        qa_ts.active_at = qa_ts.active_at.in_time_zone(qa_ts.user.timezone).beginning_of_day.utc
        qa_ts.expires_at = qa_ts.active_at.in_time_zone(qa_ts.user.timezone).end_of_day.utc

        qa_ts.save
      end
    end
  end

  desc "update existing quick activities to be available all day"
  task update_quick_activities: :environment do
    TileSchedule.quick_activity.all.each do |qa_ts|
      if !qa_ts.active_at.nil?
        qa_ts.active_at = qa_ts.active_at.in_time_zone(qa_ts.user.timezone).beginning_of_day.utc
        qa_ts.expires_at = qa_ts.active_at.in_time_zone(qa_ts.user.timezone).end_of_day.utc

        qa_ts.save
      end
    end
  end

  desc "update active tasks to end at midnight"
  task update_active_tasks: :environment do
    TileSchedule.active_tasks.all.each do |active_task|
      if !active_task.active_at.nil?
        active_task.expires_at = active_task.active_at.in_time_zone(active_task.user.timezone).end_of_day.utc

        active_task.save
      end
    end
  end

  desc "Download all of the attachements (signed consent forms)"
  task download_consent_forms: :environment do
    ActiveStorage::Attachment.all.each do |attachment|
      if attachment.blob
        filename = attachment.blob.filename.to_s
        if filename and filename.starts_with?("Consent")
          user_id = filename.gsub("Consent-", "").gsub("-signed.pdf", "")
          data = attachment.blob.download rescue nil
          File.open(Rails.root.join("tmp/#{filename}"), "wb") { |f| f.write(data) } if data
        end
      end
    end
  end

  desc "Fix Mackenzie's time zone."
  task fix_macks_timezone: :environment do
    mack = User.lookup("3038091649")
    mack.study_started_at += 1.hour
    mack.save
    mack.tile_schedules.each do |ts|
      ts.active_at += 1.hour
      ts.expires_at += 1.hour
      ts.reminded_at += 1.hour if ts.reminded_at
      ts.save
    end
  end
  
  desc "Make all congnitive tasks expire 2 hours after they are initially active"
  task update_cognitive_tasks_expiration: :environment do
    TileSchedule.all.includes(:tile, :user).cognitive_tasks.each {|t| t.update(expires_at: (t.active_at + 2.hours))}
  end

  desc "Fix broken roster phone numbers"
  task fix_broken_rosters: :environment do
    User.all.each do |user|
      r = user.roster
      if r and not user.roster_by_number == r
        print("User #{user.phone.number} doesn't match #{r.phone}")
        r.phone = user.phone.number
        r.save
      end
    end
  end

  desc "Update Oxford consent screens"
  task update_oxford_consent: :environment do
    oxfd = Organization.lookup("Oxford")
    oxfd.consent_sections = []
    oxfd.reload
    ConsentSection.from_file("app/views/static/consent-sections-oxford.json", oxfd)
  end

  desc "Update MSSM consent screens"
  task update_mssm_consent: :environment do
    mssm = Organization.lookup("Mount Sinai")
    mssm.consent_sections = []
    mssm.reload
    ConsentSection.from_file("app/views/static/consent-sections-mssm.json", mssm)
  end

  desc "Add reminder button to video diary"
  task add_reminder_button_to_video_diary: :environment do
    task = Task.get("video-diary")
    button = task.details[0].buttons.where(tag: "reminder").first
    task.details[0].buttons.create(tag: "reminder", label: "Remind me again in an hour", position: 1) if not button
  end

  desc "Update walk-test intro, again"
  task update_walk_test_intro: :environment do
    t = Task.get("walk-test")
    i = t.details.where(name: "intro").first
    c = i.contents.first
    c.content = "<p>Step on your Boydport scale to take a measurement. Then click <i>Let's start</i> and you'll be prompted to walk for six minutes. When you're done, step on your scale again.</p><p><b>Important note:</b> Please don't turn off your phone's screen during the walk test. If the screen is turned off the activity may be stopped.</p></p>"
    c.save
  end

  task update_walk_test_outro: :environment do
    t = Task.get("walk-test")
    i = t.details.where(name: "outro").first
    c = i.contents.first
    c.content = "Please step on your Bodyport scale to take a measurement and complete your walk test."
    c.save
  end

  desc "Update sleep survey"
  task update_sleep_disturbance_survey: :environment do
    s = Survey.get("sleep-disturbance")

    c = s.questions[0].contents.first
    c.content = "In the past 7 days:\n\nMy sleep was refreshing"
    c.save

    c = s.questions[1].contents.first
    c.content = "In the past 7 days:\n\nI had trouble staying asleep"
    c.save

    c = s.questions[2].contents.first
    c.content = "In the past 7 days:\n\nI had a problem with my sleep"
    c.save

    c = s.questions[3].contents.first
    c.content = "In the past 7 days:\n\nI had difficulty falling asleep"
    c.save

    c = s.questions[4].contents.first
    c.content = "In the past 7 days:\n\nMy sleep quality was..."
    c.save

    Content.where(content: "poor").update(content: "Poor")
  end

  desc "Update walk-test outro, again"
  task update_walk_test_outro: :environment do
    t = Task.get("walk-test")
    i = t.details.where(name: "outro").first
    c = i.contents.first
    c.content = "Please step on your Bodyport scale to take a measurement and complete your walk test."
    c.save
  end

  desc "Re-Update walk-test intro, again"
  task reupdate_walk_test_intro: :environment do
    t = Task.get("walk-test")
    i = t.details.where(name: "intro").first
    c = i.contents.first
    c.content = "<p><h1>About this activity</h1><p>Every other week, we'll ask you to complete this task in which you'll walk continuously at a regular walking pace for six minutes.</p><p><b>Important note:</b> Please don't turn off your phone's screen during the walk test - if the screen is turned off the activity may be stopped.</p></p>"
    c.save
  end

  desc "Re-Update walk-test outro, again"
  task reupdate_walk_test_outro: :environment do
    t = Task.get("walk-test")
    i = t.details.where(name: "outro").first
    c = i.contents.first
    c.content = "Thanks!  You've earned 30 points."
    c.save
  end

  desc "Update sleep survey"
  task update_sleep_disturbance_survey: :environment do
    s = Survey.get("sleep-disturbance")

    c = s.questions[0].contents.first
    c.content = "In the past 7 days:\n\nMy sleep was refreshing"
    c.save

    c = s.questions[1].contents.first
    c.content = "In the past 7 days:\n\nI had trouble staying asleep"
    c.save

    c = s.questions[2].contents.first
    c.content = "In the past 7 days:\n\nI had a problem with my sleep"
    c.save

    c = s.questions[3].contents.first
    c.content = "In the past 7 days:\n\nI had difficulty falling asleep"
    c.save

    c = s.questions[4].contents.first
    c.content = "In the past 7 days:\n\nMy sleep quality was..."
    c.save

    Content.where(content: "poor").update(content: "Poor")
  end

  desc "Update Oura Warning"
  task update_oura_warning: :environment do
    Content.where(content: "We haven't received any sleep data from your Oura Ring recently").update(content: "We haven't received any data from your Oura recently")
    Property.where("value like '{\"heading\":%Oura Ring%'").each do |p|
      p.value = p.value.gsub(/ (sleep|Ring)/, "")
      p.save
    end
  end

  desc "Enable CamCog"
  task enable_camcog: :environment do
    camcog_tile = Tile.find_by(name: 'camcog-ebt')
    User.where.not(study_started_at: nil).each do |u|
      puts "registering user #{u.name}"
      u.register_camcog
      puts "scheduling tiles for user #{u.name}"
      u.schedule_tile(camcog_tile)
    end
  end
end
