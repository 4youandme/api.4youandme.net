#!/bin/bash
# setup-database-config.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Nov 22 13:10:18 2019.
dir=$(dirname $0)
cd $dir

if [ "$1" == "-f" ]; then rm -f config/database.yml; shift; fi

source defaults.sh
source functions.sh

create-database-config
    
