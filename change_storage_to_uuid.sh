#!/bin/bash
# change_storage_to_uuid.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Mon May 18 17:17:22 2020.
export DISABLE_DATABASE_ENVIRONMENT_CHECK=1
bundle exec rake evi:download_consent_forms
bundle exec rails runner 'User.all.each {|u| u.signed_consent.destroy if u.signed_consent.attachment}'
bundle exec rake db:migrate:redo VERSION=20191219005122
