#!/bin/bash
# entrypoint.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sun Nov 24 10:41:52 2019.
set -e

# Do things in the right rails environment.
export RAILS_ENV=production

# The following line allows the creation of a well known administrator.
export DEVELOPER_CONTAINER=true

# Where Dockerfile said we should install.
dest=/www/sites/api.4youandme.dev/current
cd $dest

# Remove leftovers from previous invocations.
rm -rf tmp/* log/*

# Wait for the DB to be ready to accept connections.
while ! nc -z db 5432; do sleep 2; done

# Make sure that all of our gem dependencies are installed.
bundle install --path=vendor --without="development test" --deployment

# Run any new migrations.
bundle exec rails db:migrate

# Make sure that the roster is in place.
bundle exec rails db:seed

# In production mode, the assets are precompiled.
bundle exec rails assets:precompile

# The command that starts the main process.
bundle exec rails server -b 0.0.0.0 -p 3000
