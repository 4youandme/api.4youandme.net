#!/bin/bash
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

function realpath { echo $(cd $(dirname $1); pwd)/$(basename $1); }
thisdir=$(dirname $(realpath ${BASH_SOURCE[0]}))

set -e
set -x

source ${thisdir}/defaults.sh
source ${thisdir}/functions.sh
export db_username="postgres"
setup-database

