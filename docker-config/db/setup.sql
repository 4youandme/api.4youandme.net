--setup.sql: -*- SQL[ANSI] -*-  DESCRIPTIVE TEXT.
--
-- Author: Brian J. Fox (bfox@opuslogica.com)
-- Birthdate: Thu Nov 28 10:01:04 2019.
CREATE USER evixp_user with encrypted password 'evixp_pass';
CREATE DATABASE evixp;
GRANT ALL PRIVILEGES ON DATABASE evixp TO evixp_user;
\c evixp;
CREATE EXTENSION IF NOT EXISTS "pgcrypto";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";


