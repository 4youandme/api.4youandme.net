require 'irb/ext/save-history'
IRB.conf[:SAVE_HISTORY] = 1000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb-history"
def quiet; ActiveRecord::Base.logger.level = Logger::INFO; end
def noisy; ActiveRecord::Base.logger.level = Logger::DEBUG; end
quiet
@bfox = User.lookup("8056378642")
@luca = User.lookup("8054525857")
@mack = User.lookup("3038091649")
@mackenzie = @mack
@elisabeth = User.lookup("9083034892")
@adrien = User.lookup("4152445727")
puts "Assigned associated users to @bfox, @luca, @adrien, and @elisabeth"
@oura = OauthProvider.lookup("oura") rescue nil
@rescuetime = OauthProvider.lookup("rescuetime") rescue nil
@pooply = OauthProvider.lookup("pooply") rescue nil
@oxford = Organization.lookup("Oxford")
@mssm = Organization.lookup("mssm")
@sinai = @mssm
puts "Assigned associated providers to @oura, @rescuetime, and @pooply"
noisy
