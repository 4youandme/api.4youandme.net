# DOCKER-POSTGRES

Ugh.

postgres_url="postgresql://postgres@localhost:7654/postgres"
function get-database-command() { echo "psql"; }

psql "${postgres_url}" <<-EOF #2>/dev/null
  CREATE USER evixp_user with encrypted password 'evixp_pass';
EOF
# In postgresql, the database has to exist before you can grant access to it.
psql "${postgres_url}" <<-EOF #2>/dev/null
  CREATE DATABASE evixp;
  CREATE DATABASE evixo_dev;
  CREATE DATABASE evixp_test;
EOF

psql "${postgres_url}" <<-EOF #2>/dev/null
  GRANT ALL PRIVILEGES ON DATABASE evixp TO evixp_user;
  GRANT ALL PRIVILEGES ON DATABASE evixp_dev TO evixp_user;
  GRANT ALL PRIVILEGES ON DATABASE evixp_test TO evixp_user;
EOF
psql "${postgres_url}" <<-EOF #2>/dev/null
  \\c evixp
  CREATE EXTENSION IF NOT EXISTS "pgcrypto";
  CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
  ALTER DATABASE evixp OWNER TO evixp_user;
EOF


