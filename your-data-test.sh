# your-data-test.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Mar  6 06:36:16 2020.

URIEncode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER) 
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}

start_date="2020-02-27"
end_date="2020-03-05"
freq=9
stream=$(URIEncode "[weight,steps,mood,energy]")
stream=$(URIEncode "[steps]")
buckets=5
curl -H "api-token: 4a9c123bea1c4fa79b6f19036adec0f1" "https://api.4youandme.net/app/api/v1/your-data?start-date=$start_date&end-date=$end_date&freq=$freq&stream=$stream&buckets=$budkets"
