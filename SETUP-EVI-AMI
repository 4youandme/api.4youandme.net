#!/bin/bash
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Can be mysql, percona, or postgres
database=postgres
ruby_version="2.6"

function usage() {
    local extra_message="$1"
    if [ "$extra_message" != "" ]; then
	echo "$0: $extra_message" 1>&2
    fi
    echo "Usage: $0 [-d <mysql|percona|postgres|pg>] [--database=<mysql|percona|postgres|pg>] --with-rails --with-apache" 1>&2
    exit 1
}

function set_database() {
    local candidate="$1"

    if [ "$candidate" = "pg" ]; then candidate="postgres"; fi
    if [ "$candidate" = "mysql2" ]; then candidate="mysql"; fi
    if [ "$candidate" != "mysql" -a "$candidate" != "percona" -a "$candidate" != "postgres" ]; then
	usage "Invalid database: '$candidate'"
    fi
    database="$candidate";
}

function setup_nodejs() {
    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    apt-get install -y nodejs
}

while getopts ":d-:" optchar; do
    case "${optchar}" in
	-) case "${OPTARG}" in
	       with-rails)
		   install_rails=yes
		   ;;
	       database)
		   set_database "${!OPTIND}"
		   OPTIND=$((OPTIND + 1))
		   ;;
	       database=*)
		   set_database ${OPTARG#*=}
		   ;;
	       *)
		   if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
		       usage "Unknown option --${OPTARG}";
                   fi
		   ;;
	   esac
	   ;;
        d)
            set_database "${OPTARG}"
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
/usr/sbin/adduser --disabled-password --gecos "Brian J. Fox,,76-BAFFLE-76,805.637.8642" --home /home/bfox --shell /bin/bash bfox
/usr/sbin/adduser --disabled-password --gecos "Mackenzie Wildman" --home /home/mwildman --shell /bin/bash mwildman
/usr/sbin/adduser --disabled-password --gecos "The Deployer,,76-BAFFLE-76," --home /home/deployer --shell /bin/bash deployer
pass_bfox="$6$CK7zVMgx$8eV2ED9iC5OKVx6F7OoXHN0Mlq2Oa7U92mJmNWu/UVVvsMOe6VX.uudEBff/zfyawmVY6LGzUKF8b7wPNEKtN/"
pass_mack="$6$Kr9m4Eud$Yyhs59Az5yiaR2lrOnYRsMFwJvMFI2.9x0Nsqrbu3SoGjohT/SOXOhBgqhrRGomWvdOJHp3NF3ttKndiaMb510"
usermod -p "$pass_bfox" bfox
usermod -p "$pass_mack" bfox
usermod -G sudo,www-data bfox
usermod -G sudo,www-data mwildman
usermod -G www-data deployer

# Add swap space
fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo -e '/swapfile\tnone\tswap\tsw\t0 0' >>/etc/fstab

apt-get update
apt-get install -y apt-utils
apt-get install -y software-properties-common
apt-add-repository -y ppa:brightbox/ruby-ng
apt-get update

apt-get install -y git wget sudo curl awscli jq
# apt-get install -y emacs25-nox
apt-get install -y apache2 apache2-dev libapr1-dev libaprutil1-dev

apt-get install -y build-essential autoconf libcurl4-openssl-dev libssl-dev zlib1g-dev
apt-get install -y ruby${ruby_version} ruby${ruby_version}-dev

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
apt-get install -y nodejs

apt-get install -y libsqlite3-dev
apt-get install -y imagemagick

if [ "$database" = "postgres" ]; then
    apt-get update
    apt-get install -y libpq-dev
fi

gem install capistrano capistrano-rails bundler capistrano-bundler capistrano-passenger capistrano-locally
gem install passenger --no-document
gem install bundler --no-document
yes | passenger-install-apache2-module

passenger_version=$(echo $(passenger-config --version) | awk '{ print $3; }')
cat >/etc/apache2/mods-available/passenger.load <<EOF
LoadModule passenger_module /var/lib/gems/${ruby_version}.0/gems/passenger-${passenger_version}/buildout/apache2/mod_passenger.so
EOF
cat >/etc/apache2/mods-available/passenger.conf <<EOF
PassengerRoot /var/lib/gems/${ruby_version}.0/gems/passenger-${passenger_version}
PassengerDefaultRuby /usr/bin/ruby${ruby_version}
EOF

mkdir -p /www/sites
chown root.root /www
chown root.www-data /www/sites
chmod g+rwx /www/sites

a2enmod passenger
a2enmod rewrite

function setup_ssh_with_key() {
    local user="$1"
    local pubkey="$2"
    
    pushd /home/${user}
    mkdir .ssh 2>/dev/null
    chown ${user}:${user} .ssh
    chmod og-rwx,u+rwx .ssh
    cd .ssh
    cat >>authorized_keys <<EOF
${pubkey}
EOF
    chown ${user}:${user} *
    chmod a-rwx,u+rw *.key 2>/dev/null
    chmod go-wx,go+r authorized_keys

    popd
}

bfox_pub="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCz9JTyhButpso+mUafUfgiKzwNty0e68SOLMshlKZCTSqcxsGTgJjq3nqf75oULvGgP1Nche/cLFW0dfxU4bapJZNVdTbYGE+wTAAH1XjpE1bymzKjx+tsr0rqiQ3H2o+ySG1XNRaUyPic02EwYvIaAbyuMqGtEVXBhISOiwbXYNOW5/s2Y0uFA1J07VVj872uMbU6m3ZumOMzqfi0EoHIvXYf07aVwaa/fn3qZuwyOzmicFgRMzsaEM2ZQP8UTn/RDqKU2APLPQ5wGM1fdBJ2Mx9zEc5zZSUznwek2vH9yQOmgr7vf3MaRSKrBQEiJz+VVmAYuaYGdNyDfZrV32+l bfox@Brians-MacBook.local"

setup_ssh_with_key deployer "${bfox_pub}"
setup_ssh_with_key bfox "${bfox_pub}"

cd /home/bfox
wget http://opuslogica.com/downloads/bfox-blob.tar.gz
tar -zxvpf bfox-blob.tar.gz
chown bfox:bfox .bash*  bin/as-user.c
chown bfox:bfox .emacs* .git*
chown root bin/as-user
chmod u+s bin/as-user

# Setup Apache config:
rm -f /etc/apache2/sites-available/*
rm -f /etc/apache2/sites-enabled/*
cat <<EOF >/etc/apache2/sites-available/api.4youandme.dev.conf
  PassengerMinInstances 5
  PassengerLogLevel 1
<VirtualHost *:80>
  ServerName api.4youandme.net
  ServerAlias api.4youandme.dev
  ServerAlias api.4youandme.org
  ServerAlias api.4youandme.com
  ServerAdmin bfox@opuslogica.com
  DocumentRoot "/www/sites/api.4youandme.dev/current/public"
  ErrorLog \$\{APACHE_LOG_DIR\}/api.4youandme.dev.com-error.log
  TransferLog \$\{APACHE_LOG_DIR\}/api.4youandme.dev.com-access.log
  PassEnv RAILS_MASTER_KEY

  <Directory "/www/sites/api.4youandme.dev">
    Require all granted
    Options FollowSymLinks
    AllowOverride None
    Order allow,deny
    Allow from all
  </Directory>
  RewriteEngine On
  RewriteRule ^healthcheck - [L]
  RewriteCond %{HTTP:X-Forwarded-Proto} =http
  RewriteRule .* https://%{HTTP:Host}%{REQUEST_URI} [L,R=temp]
</VirtualHost>
EOF
cd /etc/apache2/sites-enabled
ln -s /etc/apache2/sites-available/api.4youandme.dev.conf .

# Build and install the latest version of ImageMagick
apt-get update
apt-get install -y build-essential
wget https://www.imagemagick.org/download/ImageMagick.tar.gz
tar xvzf ImageMagick.tar.gz
cd ImageMagick-7*/
./configure
make
make install
ldconfig /usr/local/lib
