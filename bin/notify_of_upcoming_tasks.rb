# notify_of_upcoming_tasks.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Mon Feb 10 08:44:35 2020.
TileSchedule.notify_of_upcoming_tasks(10.minutes)
