# update-providers.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Dec 20 07:41:49 2019.
if [ "$1" != "" ]; then
    # Argument is the name of the server, like api.4youandme.dev.
    # Let's use convention to make this work.
    server=$1
    myname=$(basename $0)
    ssh -tt deployer@$server -C "cd /www/sites/$server/current; bash -x ./$myname"
else
    export RAILS_ENV=production
    export DISABLE_DATABASE_ENVIRONMENT_CHECK=1
    export DEVELOPER_CONTAINER=true
    bundle exec rails db:drop_providers
    bundle exec rails db:seed_providers
fi
