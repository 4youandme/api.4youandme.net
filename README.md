# Rails API server for backing generic health studies.

# Start developing:

## First, put your secrets into the environment
1. Create a file `.env.rb` in this directory
```sh
# The name of this app in its long and polite form.
ENV['PUBLIC_APP_NAME'] = "Stress In Crohn's"

# Sending text/sms through TWILIO.
ENV['TWILIO_SID']  = "ef039d230422942224446cb9b543e5d683"
ENV['TWILIO_AUTH'] = "ab1d168c7573de14236b718e2a2eb4a03"
ENV['TWILIO_NUM']  = "+1 805 979-2092"

# Sending Email through Google.
ENV['GMAIL_DOMAIN'] = "4youandme.dev"
ENV['GMAIL_PASS'] = "9ue985ri!"
ENV['GMAIL_USER'] = "notifier@4youandme.dev"

# Sending push notifications through Firebase.
ENV['FCM_SERVER_KEY'] = "AAAA3MfS9Wo:Azyccdefgabcdefg_fKiviQiePLnF_6Q90MTTVl...ay3ot4FNrCnJnxSE6b5Btbrianjfox"

# Storing in AWS.
ENV['AWS_KEY'] = "foo"
ENV['AWS_SECRET'] = "foo"
ENV['AWS_BUCKET'] = "foo"
ENV['AWS_REGION'] = "foo"

# When this server is running in production, the IAM Credentials for the instance should be used.
# To do that, configure the above as follows:
# ENV['AWS_KEY'] = ""
# ENV['AWS_SECRET'] = ""
# ENV['AWS_BUCKET'] = "4youandme-consent"
# ENV['AWS_REGION'] = "us-west-2"

# This next section is about companion apps.

# Rescue Time
ENV['RESCUETIME_CLIENT_ID'] = "aaab117c4988a51a3dc1defea2a99fd2059aa534b53d9ca46821407ff38218dc"
ENV["RESCUETIME_CLIENT_SECRET"] = "9083a7f08ab5cc4a0018eb2ef7f38109f07f2261f81b6cb04175e07cd6bb89d3"

ENV['POOPLY_CLIENT_ID'] = "ksdkfijJDIJXpEknW4Rcr0xCwyvIzYGukKg3rVAnEgcYKur"
ENV["POOPLY_CLIENT_SECRET"] = "lDKfkdfokdofXlPthhm3le4FP9NqaLC4fjodmAYJ5qwAnuSAvHcASmu08bTUVHmBb4jwhc6BRwJnzeURuSCGYOOeQ8dX8BB7BltsM8epAjS2h3IXtzwB"

ENV['OURA_CLIENT_ID'] = "KJSDIFUIJEF"
ENV["OURA_CLIENT_SECRET"] = "LKSJDFIIJEF8KNEF8HENF9IE909J"

```
The full list will appear in time.  Should include anything you need for OAuth, or for accessing
other backend services, like Twilio, Firebase, GMail, etc.

## I like installing everything on my system.
1. Install a postgresql database
2. Run `./start-developing`
3. Application available at `http://localhost:3000`

## Wait! I don't want to install a postgresql database.
1. Install `docker`
2. Run `docker-compose build`
3. Run `docker-compose up`
4. Application available at `http://localhost:3100`

You could do this, and then develop in the container as if you were developing on a remote machine.
```sh
bfox@BriansMacbook$ docker exec -it api4youandmenet_web_1 /bin/bash
root@46009dd01ea4:/www/sites/api.4youandme.dev/current# apt-get install -y emacs-nox vim
```
## I don't want PostgreSQL installed on my system, but I want to edit the rails code here.
1. Install `docker`
2. Run `docker-compose build db`
3. Run `docker-compose up -d db`
4. Run `./setup-database-config.sh -f`
5. Develop the rails app as you normally would

# Deployment
1. Setup a server running Ubuntu 18.04 (now that is "your server")
1. Use the `SETUP-EVI-SERVER` script as the root user on your server
1. Edit `config/deploy/dev.rb` and change the server name to your server
1. Deploy to your server:
```sh
bundle exec cap dev deploy
```

# Philosophy
## Open Source

## Reusability

## Tooling

## SETUP DEV AT HOME
  * install docker
```
  docker-compose up -d db
  psql -U postgres -p 7654 -h localhost
  CREATE USER evixp_user with encrypted password 'evixp_pass';
  CREATE DATABASE evixp;
  CREATE DATABASE evixp_dev;
  CREATE DATABASE evixp_test;
  GRANT ALL PRIVILEGES ON DATABASE evixp TO evixp_user;
  GRANT ALL PRIVILEGES ON DATABASE evixp_dev TO evixp_user;
  GRANT ALL PRIVILEGES ON DATABASE evixp_test TO evixp_user;
  ALTER USER evixp_user WITH SUPERUSER;
  \c evixp
  CREATE EXTENSION IF NOT EXISTS "pgcrypto";
  CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```
## Deploy to another place
Let's say you want to deploy to `api.newserver.org`
1. Prepare the (Ubuntu|Debian) install machine using the `SETUP-EVI-SERVER` script.
1. create `config/deploy/newserver.rb`, with contents like:
```ruby
set :branch, "production"
set :stage, "production"
server "api.newserver.org", user: "deployer", roles: %w{app db web}
```
1. create the postgresql database on the server, or use a docker image (see `docker-compose.yml`
1. deploy with `bundle exec cap deploy`
   Note that you, the deploying person, must be able to `ssh deployer@api.newserver.org`, and that
   `deployer` on `api.newserver.org` must have pull access to the gitlab repo for this code.
1. Ensure that `/www/sites/api.newserver.org/shared/environment.rb` exists, and has the required credentials in it (see above)
1. For first deployment, you will have to seed the database.  You can do this with:
```bash
bundle exec rake db:migrate
bundle exec rake db:seed
```
1. Install the correct apache2 config file.  There is an example in `./apache-config.conf`


## Sending a Notification to a User

To send a notification to the user with phone number `+1 (805) 637-1234` you do the following:

1. login to the API server.
2. Go to the directory where the server is running: e.g., `/www/sites/api.4youandme.net/current`.
3. As the deployer user, start a rails console session in the production environment:
```bash
sudo su deployer -c 'bundle exec rails c production'
```
4. Get the user that you wish to notify into a variable:
```ruby
user = User.lookup("8056371234")
```
5. Send that user a notification:
```ruby
user.send_notification(title: "Important update", message: "The app has important info -- please open it")
```

To send a notification to all participants, do the following:
```ruby
User.all.each { |user| user.send_notification(title: "Alert", message: "some message") }
```

## Tests

There are a few. They take a bit of time to run since the database gets seeded every time the tests are run. To run, do:

```bash
bundle exec rake test
```


