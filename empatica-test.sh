#!/usr/bin/env bash
# empatica-test.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Sun Feb 23 23:01:37 2020.
datafile=${1:-empatica-test.json}
curl --request POST \
     --header "Content-type: application/json" \
     --header "api-token: a-ridiculous-secret" \
     --data @$datafile \
     http://localhost:3000/internal/api/measurement/empatica
