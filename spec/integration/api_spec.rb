# api_spec.rb: -*- Ruby -*-  The OpenAPI specification for Stress in Crohns
# 
#  Copyright (c) 2019 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Mon Sep  9 17:22:57 2019.
require 'swagger_helper'

describe "4 You and Me API" do

  path "/api/v1/consent_answer" do
    post "Record the participant's answer to one of our questions" do
      tags "Consent"
      consumes "application/json", "application/xml"
      parameter name: :consent_answers, in: :body, schema: {
                  type: :array,
                  items: {
                    type: :object,
                    properties: {
                      question_id: { type: :string, example: "8a0801ce-42cf-488c-a691-c7c163440e8b" },
                      answer: { type: :string, example: "Yes" },
                    }
                  }
                }

      response('200', 'Consent answers saved') do
        let(:consent_answers) { [ { question_id: "8a0801ce-42cf-488c-a691-c7c163440e8b", answer: "Yes" },
                                  { question_id: "6162aafc-36cf-4263-a6ac-c6f662325052", answer: "Red" } ] }
        run_test!
      end

      response '404', 'invalid request' do
        let(:consent_answers) { { birthdate: 'bummer' } }
        run_test!
      end
    end
  end

  path "/api/v1/consent_sections" do
    get  "Retrieve all of the consent sections" do
      tags "Consent"
      
      response('200', 'Array of consent sections') do
        run_test!
      end
    end
  end

  path "/api/v1/consent_section/{id}" do
    get "Return the specific consent section" do
      tags "Consent"
      parameter name: :id, in: :path, type: :string, required: :id

      response('200', 'Consent section information as JSON') do
        let(:id) { 1 }
        run_test!
      end

      response '404', 'Record not found' do
        let(:id) { 0 }
        run_test!
      end
    end

    path "/api/v1/person/{id}" do
      get "Return information about the object specified by the ID parameter" do
        tags "Person"
        parameter name: :id, in: :path, type: :string, required: :id

        response('200', 'Person information as JSON') do
          let(:id) { 1 }
          run_test!
        end

        response '404', 'Record not found' do
          let(:id) { 0 }
          run_test!
        end
      end
    end
    
    put "Update information about the object specified by the ID parameter" do
      tags "Person"
      parameter name: :id, in: :path, type: :string, required: :id
      parameter name: :person, in: :body, schema: {
                  type: :object,
                  properties: {
                    fname: { type: :string, example: "Bradley" },
                    minitial: { type: :string, example: "M" },
                    lname: { type: :string, example: "Pitts" },
                    birthdate: { type: :string, example: "1987-12-11" },
                    nationality: { type: :string, example: "USA" },
                    language: { type: :string, example: "en" },
                    email: { type: :string, example: "brad@thepitts.com" },
                    phone: { type: :string, example: "805.555.1212" },

                    phones_attributes: {
                      type: :array,
                      items: {
                        type: :object,
                        properties: {
                          number: {type: :string, example: "1-415-555-8889" },
                          label: { type: :string, example: "Home" }
                        }
                      }
                    },

                    devices_attributes: {
                      type: :array,
                      items: {
                        type: :object,
                        properties: {
                          name: { type: :string, example: "iPhone" },
                          dimensions: { type: :string, example: "1324x750" },
                          resolution: { type: :string, example: "326dpi" },
                          push_tokens_attributes: {
                            type: :array,
                            items: {
                              type: :object,
                              properties: {
                                provider: { type: :string, example: "APN" },
                                token: {
                                  type: :string,
                                  example: "740f4707bebcf74f9b7c25d48e3358945f6aa01da5ddb387462c7eaf61bb78ad"
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }


      response('200', 'Person information as JSON') do
        let(:id) { 1 }
        run_test!
      end

      response '404', 'Record not found' do
        let(:id) { 0 }
        run_test!
      end
    end

    delete "Delete the object specified by the ID parameter" do
      tags "Person"
      parameter name: :id, in: :path, type: :string, required: :id

      response('200', 'JSON object with success: true') do
        let(:id) { 1 }
        run_test!
      end

      response '404', 'Record not found' do
        let(:id) { 0 }
        run_test!
      end
    end
  end


  path "/api/v1/faq_categories/{id}" do
    get "Return information about the object specified by the ID parameter" do
      tags "faq"
      parameter name: :id, in: :path, type: :string, required: :id

      response('200', 'FAQ Category information as JSON') do
        let(:id) { 1 }
        run_test!
      end

      response '404', 'Record not found' do
        let(:id) { 0 }
        run_test!
      end
    end

    put "Update information about the object specified by the ID parameter" do
      tags "faq"
      parameter name: :id, in: :path, type: :string, required: :id

      response('200', 'FAQ Category information as JSON') do
        let(:id) { 1 }
        run_test!
      end

      response '404', 'Record not found' do
        let(:id) { 0 }
        run_test!
      end
    end

    delete "Delete the object specified by the ID parameter" do
      tags "faq"
      parameter name: :id, in: :path, type: :string, required: :id

      response('200', 'JSON object with success: true') do
        let(:id) { 1 }
        run_test!
      end

      response '404', 'Record not found' do
        let(:id) { 0 }
        run_test!
      end
    end
  end

  path "/api/v1/faq_categories" do
    get  "Retrieve all of the categories" do
      tags "faq"
      
      response('200', 'Array of FAQ categories') do
        run_test!
      end
    end
    
    post "Create a new FAQ category" do
      tags "faq"
      consumes "application/json", "application/xml"
      parameter name: :faq_category, in: :body, schema: {
                  type: :object,
                  properties: {
                    name: { type: :string, example: "Mathematical Constructs" },
                    position: { type: :number, example: "1" },
                    language: { type: :string, example: "en" },
                    visible: { type: :string, example: "true" }
                  },

                  required: ['name', :position]
                }

      response('200', 'FAQ category created') do
        let(:faq_category) { { name: 'Mathematical Constructus', language: "en" } }
        run_test!
      end

      response '404', 'invalid request' do
        let(:faq_category) { { birthdate: 'bummer' } }
        run_test!
      end
    end
  end


  path "/api/v1/users/{id}" do
    get "Return information about the object specified by the ID parameter" do
      tags "user"
      parameter name: :id, in: :path, type: :string, required: :id

      response('200', 'User information as JSON') do
        let(:id) { 1 }
        run_test!
      end

      response '404', 'Record not found' do
        let(:id) { 0 }
        run_test!
      end
    end

    put "Update information about the object specified by the ID parameter" do
      tags "user"
      parameter name: :id, in: :path, type: :string, required: :id

      response('200', 'User information as JSON') do
        let(:id) { 1 }
        run_test!
      end

      response '404', 'Record not found' do
        let(:id) { 0 }
        run_test!
      end
    end

    delete "Delete the object specified by the ID parameter" do
      tags "user"
      parameter name: :id, in: :path, type: :string, required: :id

      response('200', 'JSON object with success: true') do
        let(:id) { 1 }
        run_test!
      end

      response '404', 'Record not found' do
        let(:id) { 0 }
        run_test!
      end
    end
  end

  path "/api/v1/users" do
    post "Create a new user object" do
      tags "user"
      consumes "application/json", "application/xml"
      parameter name: :user, in: :body, schema: {
                  type: :object,
                  properties: {
                    name: { type: :string, example: "Bradley M Pitts" },
                    fname: { type: :string, example: "Bradley" },
                    minitial: { type: :string, example: "M" },
                    lname: { type: :string, example: "Pitts" },
                    birthdate: { type: :string, example: "1987-12-11" },
                    nationality: { type: :string, example: "USA" },
                    language: { type: :string, example: "en" },
                    email: { type: :string, example: "brad@thepitts.com" },
                    phone: { type: :string, example: "805.555.1212" },
                    status: { oneOf: [ :active, :dormant, :invited ] }
                  },

                  required: ['name']
                }

      response('200', 'user created') do
        let(:user) { { name: 'Brian J. Fox', nationality: "USA" } }
        run_test!
      end

      response '404', 'invalid request' do
        let(:user) { { birthdate: 'bummer' } }
        run_test!
      end
    end
  end
end
