#!/bin/bash
# reset-database.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2019 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Dec  6 07:53:17 2019.
remake_schema="true"

if [ "$1" = "-f" -o "$1" = "--remake-schema" ]; then remake_schema="true"; shift; fi
if [ "$1" = "-s" -o "$1" = "--use-schema" ]; then remake_schema="false"; shift; fi

if [ "$1" != "" ]; then
    # Argument is the name of the server, like api-rails.4youandme.dev.
    # Let's use convention to make this work.
    server=$1
    myname=$(basename $0)
    ssh -tt deployer@$server -C "cd /www/sites/$server/current; bash -x ./$myname"
else
    export RAILS_ENV=production
    export DISABLE_DATABASE_ENVIRONMENT_CHECK=1
    export DEVELOPER_CONTAINER=true
    if [ "$remake_schema" = "true" ]; then
	bundle exec rails db:drop
	bundle exec rails db:create
	bundle exec rails db:migrate
	bundle exec rails db:seed
    else
	bundle exec rails db:reset
    fi
fi
