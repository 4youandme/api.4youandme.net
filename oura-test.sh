# oura-test.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Feb 25 11:51:56 2020.
curl --request POST \
     --header "Content-type: application/json" \
     --header "api-token: a-ridiculous-secret" \
     --data @oura-internal.json \
     http://localhost:3000/internal/api/measurement/oura
