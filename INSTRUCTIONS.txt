Instructions for testing with a standard "MSSM" user.

1. delete the app from your device
2. install the app to your device
3. use phone number +1 (805) 555-1212
4. use vaidation code 12121

What we expect to happen the first time you do this:  User advances to the video screen.
What actually happens the first time you do this: "You've been logged out."  User is told to login again.
