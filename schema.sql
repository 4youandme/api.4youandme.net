--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.addresses (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    label_id uuid,
    addressable_type character varying,
    addressable_id bigint,
    line1 character varying,
    line2 character varying,
    city character varying,
    state character varying,
    postal character varying,
    country character varying,
    lat double precision,
    lon double precision,
    confirmed boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.addresses OWNER TO bfox;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO bfox;

--
-- Name: consent_answers; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.consent_answers (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid,
    consent_question_id uuid,
    answer character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.consent_answers OWNER TO bfox;

--
-- Name: consent_question_options; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.consent_question_options (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    consent_question_id uuid,
    language character varying,
    "position" integer,
    value character varying,
    content character varying,
    content_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.consent_question_options OWNER TO bfox;

--
-- Name: consent_questions; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.consent_questions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    consent_section_id uuid,
    "position" integer,
    language character varying,
    style_class character varying,
    question_type character varying,
    content character varying,
    content_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.consent_questions OWNER TO bfox;

--
-- Name: consent_section_infos; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.consent_section_infos (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    consent_section_id uuid,
    "position" integer,
    style_class character varying,
    title character varying,
    content text,
    content_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.consent_section_infos OWNER TO bfox;

--
-- Name: consent_section_responses; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.consent_section_responses (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    consent_section_id uuid,
    value character varying,
    title character varying,
    content text,
    content_type character varying,
    style_class character varying,
    next_action character varying,
    "position" integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.consent_section_responses OWNER TO bfox;

--
-- Name: consent_sections; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.consent_sections (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    name character varying,
    postiion integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.consent_sections OWNER TO bfox;

--
-- Name: consents; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.consents (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid,
    share_relative_location boolean,
    share_data_worldwide boolean,
    future_research_contact boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.consents OWNER TO bfox;

--
-- Name: devices; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.devices (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    person_id uuid,
    os character varying,
    name character varying,
    dimensions character varying,
    resolution character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.devices OWNER TO bfox;

--
-- Name: emails; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.emails (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    label_id uuid,
    emailable_type character varying,
    emailable_id uuid,
    address character varying,
    confirmed boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.emails OWNER TO bfox;

--
-- Name: faq_categories; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.faq_categories (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    name character varying,
    language character varying DEFAULT 'en'::character varying,
    "position" integer,
    visible boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.faq_categories OWNER TO bfox;

--
-- Name: faq_questions; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.faq_questions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    faq_category_id uuid,
    language character varying DEFAULT 'en'::character varying,
    "position" integer,
    visible boolean DEFAULT true,
    question character varying,
    answer character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.faq_questions OWNER TO bfox;

--
-- Name: labels; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.labels (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    value character varying,
    lang character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.labels OWNER TO bfox;

--
-- Name: nicknames; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.nicknames (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    nickname character varying,
    nicknameable_type character varying,
    nicknameable_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.nicknames OWNER TO bfox;

--
-- Name: organizations; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.organizations (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    name character varying,
    nationality character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.organizations OWNER TO bfox;

--
-- Name: organizations_users; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.organizations_users (
    organization_id uuid NOT NULL,
    user_id uuid NOT NULL
);


ALTER TABLE public.organizations_users OWNER TO bfox;

--
-- Name: people; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.people (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    prefix character varying(10),
    fname character varying,
    minitial character varying,
    lname character varying,
    suffix character varying(10),
    birthdate character varying,
    nationality character varying,
    language character varying(10),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.people OWNER TO bfox;

--
-- Name: phones; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.phones (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    label_id uuid,
    phoneable_type character varying,
    phoneable_id uuid,
    number character varying,
    confirmed boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.phones OWNER TO bfox;

--
-- Name: push_tokens; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.push_tokens (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    device_id uuid,
    provider character varying,
    token character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.push_tokens OWNER TO bfox;

--
-- Name: rosters; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.rosters (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    phone_id uuid,
    codename character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.rosters OWNER TO bfox;

--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO bfox;

--
-- Name: users; Type: TABLE; Schema: public; Owner: bfox
--

CREATE TABLE public.users (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    person_id uuid,
    status character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.users OWNER TO bfox;

--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: consent_answers consent_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_answers
    ADD CONSTRAINT consent_answers_pkey PRIMARY KEY (id);


--
-- Name: consent_question_options consent_question_options_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_question_options
    ADD CONSTRAINT consent_question_options_pkey PRIMARY KEY (id);


--
-- Name: consent_questions consent_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_questions
    ADD CONSTRAINT consent_questions_pkey PRIMARY KEY (id);


--
-- Name: consent_section_infos consent_section_infos_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_section_infos
    ADD CONSTRAINT consent_section_infos_pkey PRIMARY KEY (id);


--
-- Name: consent_section_responses consent_section_responses_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_section_responses
    ADD CONSTRAINT consent_section_responses_pkey PRIMARY KEY (id);


--
-- Name: consent_sections consent_sections_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_sections
    ADD CONSTRAINT consent_sections_pkey PRIMARY KEY (id);


--
-- Name: consents consents_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consents
    ADD CONSTRAINT consents_pkey PRIMARY KEY (id);


--
-- Name: devices devices_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (id);


--
-- Name: emails emails_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.emails
    ADD CONSTRAINT emails_pkey PRIMARY KEY (id);


--
-- Name: faq_categories faq_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.faq_categories
    ADD CONSTRAINT faq_categories_pkey PRIMARY KEY (id);


--
-- Name: faq_questions faq_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.faq_questions
    ADD CONSTRAINT faq_questions_pkey PRIMARY KEY (id);


--
-- Name: labels labels_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.labels
    ADD CONSTRAINT labels_pkey PRIMARY KEY (id);


--
-- Name: nicknames nicknames_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.nicknames
    ADD CONSTRAINT nicknames_pkey PRIMARY KEY (id);


--
-- Name: organizations organizations_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (id);


--
-- Name: people people_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_pkey PRIMARY KEY (id);


--
-- Name: phones phones_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.phones
    ADD CONSTRAINT phones_pkey PRIMARY KEY (id);


--
-- Name: push_tokens push_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.push_tokens
    ADD CONSTRAINT push_tokens_pkey PRIMARY KEY (id);


--
-- Name: rosters rosters_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.rosters
    ADD CONSTRAINT rosters_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_addresses_on_addressable_type_and_addressable_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_addresses_on_addressable_type_and_addressable_id ON public.addresses USING btree (addressable_type, addressable_id);


--
-- Name: index_addresses_on_label_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_addresses_on_label_id ON public.addresses USING btree (label_id);


--
-- Name: index_consent_answers_on_consent_question_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_consent_answers_on_consent_question_id ON public.consent_answers USING btree (consent_question_id);


--
-- Name: index_consent_answers_on_user_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_consent_answers_on_user_id ON public.consent_answers USING btree (user_id);


--
-- Name: index_consent_question_options_on_consent_question_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_consent_question_options_on_consent_question_id ON public.consent_question_options USING btree (consent_question_id);


--
-- Name: index_consent_questions_on_consent_section_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_consent_questions_on_consent_section_id ON public.consent_questions USING btree (consent_section_id);


--
-- Name: index_consent_section_infos_on_consent_section_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_consent_section_infos_on_consent_section_id ON public.consent_section_infos USING btree (consent_section_id);


--
-- Name: index_consent_section_responses_on_consent_section_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_consent_section_responses_on_consent_section_id ON public.consent_section_responses USING btree (consent_section_id);


--
-- Name: index_consents_on_user_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_consents_on_user_id ON public.consents USING btree (user_id);


--
-- Name: index_devices_on_person_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_devices_on_person_id ON public.devices USING btree (person_id);


--
-- Name: index_emails_on_emailable_type_and_emailable_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_emails_on_emailable_type_and_emailable_id ON public.emails USING btree (emailable_type, emailable_id);


--
-- Name: index_emails_on_label_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_emails_on_label_id ON public.emails USING btree (label_id);


--
-- Name: index_faq_questions_on_faq_category_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_faq_questions_on_faq_category_id ON public.faq_questions USING btree (faq_category_id);


--
-- Name: index_nicknames_on_nicknameable_type_and_nicknameable_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_nicknames_on_nicknameable_type_and_nicknameable_id ON public.nicknames USING btree (nicknameable_type, nicknameable_id);


--
-- Name: index_organizations_users_on_organization_id_and_user_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_organizations_users_on_organization_id_and_user_id ON public.organizations_users USING btree (organization_id, user_id);


--
-- Name: index_organizations_users_on_user_id_and_organization_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_organizations_users_on_user_id_and_organization_id ON public.organizations_users USING btree (user_id, organization_id);


--
-- Name: index_phones_on_label_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_phones_on_label_id ON public.phones USING btree (label_id);


--
-- Name: index_phones_on_phoneable_type_and_phoneable_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_phones_on_phoneable_type_and_phoneable_id ON public.phones USING btree (phoneable_type, phoneable_id);


--
-- Name: index_push_tokens_on_device_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_push_tokens_on_device_id ON public.push_tokens USING btree (device_id);


--
-- Name: index_rosters_on_phone_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_rosters_on_phone_id ON public.rosters USING btree (phone_id);


--
-- Name: index_users_on_person_id; Type: INDEX; Schema: public; Owner: bfox
--

CREATE INDEX index_users_on_person_id ON public.users USING btree (person_id);


--
-- Name: faq_questions fk_rails_0ddf6ac9b7; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.faq_questions
    ADD CONSTRAINT fk_rails_0ddf6ac9b7 FOREIGN KEY (faq_category_id) REFERENCES public.faq_categories(id);


--
-- Name: emails fk_rails_0f4ae85106; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.emails
    ADD CONSTRAINT fk_rails_0f4ae85106 FOREIGN KEY (label_id) REFERENCES public.labels(id);


--
-- Name: consent_answers fk_rails_37e1d2ab3b; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_answers
    ADD CONSTRAINT fk_rails_37e1d2ab3b FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: consent_section_responses fk_rails_37f5779f31; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_section_responses
    ADD CONSTRAINT fk_rails_37f5779f31 FOREIGN KEY (consent_section_id) REFERENCES public.consent_sections(id);


--
-- Name: consent_answers fk_rails_4c31c49f06; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_answers
    ADD CONSTRAINT fk_rails_4c31c49f06 FOREIGN KEY (consent_question_id) REFERENCES public.consent_questions(id);


--
-- Name: rosters fk_rails_573c3ae981; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.rosters
    ADD CONSTRAINT fk_rails_573c3ae981 FOREIGN KEY (phone_id) REFERENCES public.phones(id);


--
-- Name: consent_question_options fk_rails_8e54031d48; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_question_options
    ADD CONSTRAINT fk_rails_8e54031d48 FOREIGN KEY (consent_question_id) REFERENCES public.consent_questions(id);


--
-- Name: addresses fk_rails_a93b32d004; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT fk_rails_a93b32d004 FOREIGN KEY (label_id) REFERENCES public.labels(id);


--
-- Name: push_tokens fk_rails_b7a9b4faab; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.push_tokens
    ADD CONSTRAINT fk_rails_b7a9b4faab FOREIGN KEY (device_id) REFERENCES public.devices(id);


--
-- Name: consent_section_infos fk_rails_b7f64b8741; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_section_infos
    ADD CONSTRAINT fk_rails_b7f64b8741 FOREIGN KEY (consent_section_id) REFERENCES public.consent_sections(id);


--
-- Name: consents fk_rails_be0741b9c1; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consents
    ADD CONSTRAINT fk_rails_be0741b9c1 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: devices fk_rails_c2b185df34; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.devices
    ADD CONSTRAINT fk_rails_c2b185df34 FOREIGN KEY (person_id) REFERENCES public.people(id);


--
-- Name: phones fk_rails_d4160d29cc; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.phones
    ADD CONSTRAINT fk_rails_d4160d29cc FOREIGN KEY (label_id) REFERENCES public.labels(id);


--
-- Name: consent_questions fk_rails_ea908d0732; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.consent_questions
    ADD CONSTRAINT fk_rails_ea908d0732 FOREIGN KEY (consent_section_id) REFERENCES public.consent_sections(id);


--
-- Name: users fk_rails_fa67535741; Type: FK CONSTRAINT; Schema: public; Owner: bfox
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_rails_fa67535741 FOREIGN KEY (person_id) REFERENCES public.people(id);


--
-- PostgreSQL database dump complete
--

