# routes.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Fri Sep  6 09:29:14 2019.
Rails.application.routes.draw do
  devise_for :crc_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)
  
  devise_for :administrators, path: :admin
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Rswag::Ui::Engine => '/docs'
  mount Rswag::Api::Engine => '/docs'

#   RailsAdmin::Engine.routes.draw do
#   end
  
  get   'healthcheck',                                    to: 'application#healthcheck'

  scope "/internal/api" do
    get  "accesstokens(/:provider_name)",                 to: 'internal#get_accesstokens'
    get  "last_measurements(/:datasource)",               to: 'internal#last_measurements'
    post "measurement/:datasource",                       to: 'internal#create_measurement'
    post "failed_tokens/:datasource",                     to: 'internal#failed_tokens'
  end
  
  scope "/app" do
    scope "/api/v1" do
      match   'authenticate'                              =>  'authentication#authenticate',       via: [:get, :post]
      match   'token-validate'                            =>  'authentication#device_authenticate', via: [:get, :post]
      get     'authorize(/:provider_name)',               to: 'oauth#authorize'
      get     'callback(/:provider_name)',                to: 'oauth#callback'
      post    'register',                                 to: 'register#create'
      post    'resend_code',                              to: 'register#resend_code'
      post    'resend-code',                              to: 'register#resend_code'
      post    'validate-code',                            to: 'register#validate'
      get     'validate-code',                            to: 'register#validate'
      get     'apps-and-devices-page',                    to: 'oauth#providers'
      get     'providers',                                to: 'oauth#providers', v2: true
      get     'apps-and-devices',                         to: 'oauth#providers'

      get     'FAQ',                                      to: 'static#faq'
      get     'faq',                                      to: 'static#faq'
      get     'faq-static',                               to: 'static#faq'
      get     'faq-dynamic',                              to: 'faq#all'
      get     'faq/:id',                                  to: 'faq#getone'
      get     'faq-categories',                           to: 'faq#categories'

      get     'device/:device_id',                        to: 'device#get'
      post    'device',                                   to: 'device#create'
      post    'push-token',                               to: 'device#set_push_token'

      get     'contact',                                  to: 'static#contact'
      get     'points',                                   to: 'static#points'
      get     'study-progress',                           to: 'user#study_progress'
      get     'profile-content',                          to: 'static#profile-content'

      get     'consent-sections',                         to: 'consent#consent_sections'
      get     'apps-sections',                            to: 'consent#apps_sections'

      post    'consent-sections',                         to: 'consent#save_answers'
      
      post    'phone-event',                              to: 'phone_events#create'

      post    'signed-consent',                           to: 'consent#create_signed_consent'

      get     'user-status',                              to: 'user#get_status'
      get     'feed',                                     to: 'user#feed'
      get     'tiles',                                    to: 'user#tiles'
      get     'metadata',                                 to: 'user#metadata'
      get     'calendar-feed(/:beg/:fin)',                to: 'user#calendar_feed'
      get     'static/feed',                              to: 'static#feed'
      post    'user-status',                              to: 'user#update_status'
      post    'register-email',                           to: 'user#register_email'

      get     'your-data',                                to: 'user#your_data'

      post   'tasks/consume-video',                       to: 'tasks#consume_video'
      get    'tasks/video-upload-url(/:video_size)',      to: 'tasks#video_upload_url'

      post   'tasks(/:task_type)',                        to: 'tasks#ingest'
      get    'tasks/external-redirect(/:tile_sched_id)',  to: 'tasks#redirect_injest'
      get    'tasks/external-redirect-done',              to: 'tasks#external_redirect_done'
      post   'task-reminder',                             to: 'tasks#set_reminder'

    end
  end

  root to: 'application#docs'
end
