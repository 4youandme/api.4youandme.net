set :application, "api.4youandme.dev"
set :deploy_to, "/www/sites/api.4youandme.dev"
set :stage, "production"
server "localhost", roles: %w{app web}
