set :application, "api.4youandme.dev"
set :deploy_to, "/www/sites/api.4youandme.dev"
set :stage, "production"
server "api.4youandme.dev", user: "deployer", roles: %w{app db web}
