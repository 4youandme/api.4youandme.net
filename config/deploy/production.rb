require 'net/ssh/proxy/command'
ssh_command = "ssh bastion.4youandme.net -W %h:%p"
set :ssh_options, proxy: Net::SSH::Proxy::Command.new(ssh_command)
set :branch, "production"
server "api1.4youandme.net", user: "deployer", roles: %w{app db web}


