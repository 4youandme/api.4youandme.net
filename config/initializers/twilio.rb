# twilio.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
#  Copyright (c) 2019 Brian J. Fox Opus Logica, Inc.
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Thu Nov 21 14:25:34 2019.
TWILIO_SID = ENV['TWILIO_SID']
TWILIO_AUTH= ENV['TWILIO_AUTH']
TWILIO_NUM = ENV['TWILIO_NUM']

Twilio.configure do |config|
  config.account_sid = TWILIO_SID # Rails.application.secrets.twilio_account_sid
  config.auth_token = TWILIO_AUTH # Rails.application.secrets.twilio_auth_token
end
