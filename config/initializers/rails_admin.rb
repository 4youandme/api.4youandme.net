  RailsAdmin.config do |config|

  config.parent_controller = ApplicationController.to_s

  config.main_app_name = ["EVIXP", "Administration"]
  config.authenticate_with do
    warden.authenticate! scope: :administrator
  end
  config.current_user_method(&:current_administrator)

  config.included_models = ["Roster", "User", "Device", "ValidationCode", "OauthProvider", "CrcUser", "Administrator", "ValidationCode", "AccessToken", "CompletedTask", "AuditLog", "FailedToken"]

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
  end

  [Phone, Email, Address].each do |model_class|
    config.model model_class do
      list do
        exclude_fields :label
      end
      edit do
        exclude_fields :label
      end
    end
  end

  Email.class_eval do
    def name
      self.address
    end
  end

  config.model "Roster" do
    object_label_method do
      :show_phone_number
    end

    list do
      field :phone do
        formatted_value do
          Phone.obfuscate(value) rescue nil
        end
      end
      field :site
      field :empatica_id do
        label "Empatica ID"
      end
      field :bodyport_id do
        label "Bodyport ID"
      end
      field :ibdoc_id do
        label "IBDoc ID"
      end
      field :pooply_id do
        label "Pooply ID"
      end
    end

    show do
      field :phone  do
        formatted_value do
          Phone.obfuscate(value) rescue nil
        end
      end
      field :site
      group :opt_ins do
        label "Opt Ins"

        field :opt_in_relative_location_answer do
          label "Relative Location"
        end

        field :opt_in_sharing_options_answer do
          label "Sharing Options"
        end

        field :opt_in_future_research_contact_answer do
          label "Future Research Contact"
        end

      end

      field :empatica_id do
        label "Empatica ID"
      end
      field :bodyport_id do
        label "Bodyport ID"
      end
      field :ibdoc_id do
        label "IBDoc ID"
      end
      field :pooply_id do
        label "Pooply ID"
      end
      field :after_phone_validation
      field :is_test_user
      field :participant
    end
  end
  
  config.model "Person" do
    object_label_method do
      :show_phone_number
    end
  end

  config.model "CrcUser" do
    list do
      field :id
      field :email
      field :superuser
      field :site
      field :reset_password_sent_at
      field :created_at
    end

    edit do
      field :email
      field :password
      field :site, :enum do
        enum do
          Roster.all.pluck(:site).uniq
        end
      end
      field :superuser
    end
  end

  config.model 'User' do
    object_label_method do
      :show_phone_number
    end
    list do
      # field :name
      # field :person
      field :roster
      field :phone do
        formatted_value do
          value.obfuscate rescue nil
        end
      end

#       field :formatted_email do
#         label "Email"
#       end
      field :study_started_at
      field :status
      field :validated
      field :gave_full_consent
    end

    edit do
      field :person
      field :study_started_at
      field :status, :enum do
        enum do
          { registration: 'registration', studyVideo: 'studyVideo', screeningQuestions: 'screeningQuestions', consentSummaryScreens: 'consentSummaryScreens', reviewConsent: 'reviewConsent', optInQuestions: 'optInQuestions', verifyEmail: 'verifyEmail', conclusion: 'conclusion', appsConnection: 'appsConnection', dashboard: 'dashboard' }
        end
      end
      field :validated
      field :gave_full_consent
      field :is_test_user
      field :consent_link do
        visible do
          bindings[:object].consent_link rescue nil
        end
      end
    end
  end

  config.model 'CompletedTask' do
    list do
      sort_by :completed_at
      field :user
      field :task_type
      field :task
      field :started_at
      field :completed_at
    end

    show do
      field :name do
        pretty_value do
          bindings[:view].render(partial: "view_completed_task", locals: { completed_task: bindings[:object] })
        end
      end
    end
  end

  config.model 'AuditLog' do
    list do
      sort_by :when
      field :who
      field :what
      field :when do
        sort_reverse true
      end
      field :which
      field :ip_address do
        label 'Where'
      end
    end
  end
end
