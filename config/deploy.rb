# deploy.rb: -*- Ruby -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
#
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Author: Brian J. Fox (bfox@opuslogica.com)
# Birthdate: Sat Dec 21 10:05:33 2019.
#

lock "~> 3.11.1"
set :application, "api.4youandme.net"
set :repo_url, "https://gitlab.com/4youandme/api.4youandme.net.git"

# Default branch is whatever the current branch is.
set :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/www/sites/api.4youandme.net"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"

# Default value for linked_dirs is []
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

namespace :deploy do
  desc 'Upload .env.rb'
  task :upload do
    on roles(:app) do |host|
      upload!('.env.rb', "foo.env.rb")
    end
  end

  task :setup_database_config do
    on roles(:app) do
      within "#{release_path}" do
        execute "bash #{release_path}/setup-database-config.sh"
      end
    end
  end

  task :maybe_seed_database do
    on roles(:db) do
      within "#{release_path}" do
        fname = "../../delete-me-to-reseed-the-db-on-the-next-deploy"
        if not File.exists?(fname)
          File.write(fname, "")
          system("bundle exec rails db:seed")
        end
      end
    end
  end

  task :link_env do
    on roles(:app) do
      within "#{release_path}" do
        execute "rm -f #{release_path}/.env.rb"
        execute "ln -s #{shared_path}/environment.rb #{release_path}/.env.rb"
      end
    end
  end

end

before "deploy:symlink:shared", "deploy:setup_database_config"
before "deploy:migrate", "deploy:link_env"
# after  "deploy:migrate", "deploy:maybe_seed_database"
